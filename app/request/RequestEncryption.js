import { encryptStringWithAes, decryptStringWithAes } from './SecurityHelper';

export const encryptProperties = obj => {
  const response = {};
  const keys = Object.keys(obj);
  /* eslint-disable no-restricted-syntax */
  // eslint-disable-next-line guard-for-in
  for (const i in keys) {
    const key = keys[i];
    let value = obj[key];
    if (!value) value = '';
    response[key] = encryptStringWithAes(value.toString());
  }

  return response;
};

export const encryptObject = obj => {
  let response = JSON.stringify(obj);
  response = encryptStringWithAes(response);

  return response;
};

export const decryptObject = (obj, isJsonFormat = true) => {
  let response = decryptStringWithAes(obj);
  if (isJsonFormat) {
    response = JSON.parse(response);
  }
  return response;
};
