/* eslint-disable arrow-parens */
import axios from 'axios';
import { getUniqueId } from 'react-native-device-info';

import config from './Config';
import { encryptProperties } from './RequestEncryption';

const API_URL = config.psslai_api_server_host;
const API_MOCK_URL = config.psslai_api_mock_host;
const isMockData = config.mock_data;

export const apiCardDetails = async (token, guid) => {
  let endPoint = `${API_URL}/`;
  if (isMockData) {
    endPoint = API_MOCK_URL;
    params = token;
  }

  const apiResponse = await axios
    .get(
      `${endPoint}members/${guid}/card/details`,
      {},
      {
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
          'user-agent': `mobile, ${getUniqueId()}`,
        },
      },
    )
    .then(response => response)
    .catch(error => error.response);
  return apiResponse;
};

export const apiCardLock = async (token, guid) => {
  let endPoint = `${API_URL}/`;
  if (isMockData) {
    endPoint = API_MOCK_URL;
    params = token;
  }

  const apiResponse = await axios
    .post(
      `${endPoint}members/${guid}/lockCard`,
      {},
      {
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
          'user-agent': `mobile, ${getUniqueId()}`,
        },
      },
    )
    .then(response => response)
    .catch(error => error);
  return apiResponse;
};

export const apiCardUnlock = async (token, guid) => {
  let endPoint = `${API_URL}/`;
  if (isMockData) {
    endPoint = API_MOCK_URL;
    params = token;
  }

  const apiResponse = await axios
    .post(
      `${endPoint}members/${guid}/unlockCard`,
      {},
      {
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
          'user-agent': `mobile, ${getUniqueId()}`,
        },
      },
    )
    .then(response => response)
    .catch(error => error.response.data);
  return apiResponse;
};

export const apiGenerateNewCvv = async (rawParams, token) => {
  let endPoint = `${API_URL}/`;
  let params = encryptProperties(rawParams);
  if (isMockData) {
    endPoint = API_MOCK_URL;
    params = rawParams;
  }

  const apiResponse = await axios
    .post(`${endPoint}/members/cvv/v2/generate`, params, {
      headers: {
        'User-Agent': `mobile, ${getUniqueId()}`,
        Authorization: `Bearer ${token}`,
      },
    })
    .then(response => response)
    .catch(error => error.response);
  return apiResponse;
};

export const apiChangeAtmPin = async (rawParams, token, guid) => {
  const params = encryptProperties(rawParams);
  const apiResponse = await axios
    .post(`${API_URL}/members/${guid}/changePin`, params, {
      headers: {
        'User-Agent': `mobile, ${getUniqueId()}`,
        Authorization: `Bearer ${token}`,
      },
    })
    .then(response => response)
    .catch(error => error.response);
  return apiResponse;
};
