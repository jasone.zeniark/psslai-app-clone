import CryptoJS from 'crypto-js';
import config from './Config';

const secretKey = config.security_encryption_key;

export const decryptStringWithAes = toDecrypt => {
  const encryptedData = toDecrypt.toString(CryptoJS.enc.Utf8);
  const components = encryptedData.split('.');
  const encryptString = components[0];
  const iv = parseInt(components[1]);
  const cipher = CryptoJS.AES.decrypt(
    encryptString,
    CryptoJS.enc.Utf8.parse(secretKey),
    {
      iv: CryptoJS.enc.Utf8.parse(iv),
      mode: CryptoJS.mode.CBC,
    },
  );

  return CryptoJS.enc.Utf8.stringify(cipher).toString();
};

export const encryptStringWithAes = toEncrypt => {
  const now = new Date();
  const iv = now.getTime();
  const cipher = CryptoJS.AES.encrypt(
    toEncrypt,
    CryptoJS.enc.Utf8.parse(secretKey),
    {
      iv: CryptoJS.enc.Utf8.parse(iv),
      mode: CryptoJS.mode.CBC,
    },
  );
  const encryptedString = cipher.toString() + '.' + iv;

  return encryptedString.toString(CryptoJS.enc.Utf8);
};
