/* eslint-disable arrow-parens */
/* eslint-disable prettier/prettier */
import axios from 'axios';
import { getUniqueId } from 'react-native-device-info';

import config from './Config';
import { encryptProperties } from './RequestEncryption';

const API_URL = config.psslai_api_server_host;
const API_MOCK_URL = config.psslai_api_mock_host;
const isMockData = config.mock_data;

export const apiGetTransferDetails = async (token) => {
  let endPoint = `${API_URL}/`;
  if (isMockData) {
    endPoint = API_MOCK_URL;
  }
  const apiResponse = await axios
    .get(`${endPoint}members/eon/transfer/details`, {
      headers: {
        'User-Agent': `mobile, ${getUniqueId()}`,
        Authorization: `Bearer ${token}`,
      },
    })
    .then(response => response)
    .catch(error => error.response);
  return apiResponse;
};

export const apiTransferOwnAccount = async (rawParams, token) => {
  let endPoint = `${API_URL}/`;
  let params = encryptProperties(rawParams);
  if (isMockData) {
    endPoint = API_MOCK_URL;
    params = rawParams;
  }

  const apiResponse = await axios
    .post(`${endPoint}members/eon/transfer/psslai`, params, {
      headers: {
        'User-Agent': `mobile, ${getUniqueId()}`,
        Authorization: `Bearer ${token}`,
      },
    })
    .then(response => response)
    .catch(error => error.response);
  return apiResponse;
};

export const apiTransferOtherVisaAccount = async (rawParams, token) => {
  let endPoint = `${API_URL}/`;
  let params = encryptProperties(rawParams);
  if (isMockData) {
    endPoint = API_MOCK_URL;
    params = rawParams;
  }

  const apiResponse = await axios
    .post(`${endPoint}members/eon/transfer/eon`, params, {
      headers: {
        'User-Agent': `mobile, ${getUniqueId()}`,
        Authorization: `Bearer ${token}`,
      },
    })
    .then(response => response)
    .catch(error => error.response);
  return apiResponse;
};

export const apiTransferOtherBankAccount = async (rawParams, token) => {
  let endPoint = `${API_URL}/`;
  let params = encryptProperties(rawParams);
  if (isMockData) {
    endPoint = API_MOCK_URL;
    params = rawParams;
  }

  const apiResponse = await axios
    .post(`${endPoint}members/eon/transfer/bank`, params, {
      headers: {
        'User-Agent': `mobile, ${getUniqueId()}`,
        Authorization: `Bearer ${token}`,
      },
    })
    .then(response => response)
    .catch(error => error.response);
  return apiResponse;
};
