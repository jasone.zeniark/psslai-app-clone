/* eslint-disable arrow-parens */
import axios from 'axios';
import { getUniqueId } from 'react-native-device-info';

import config from './Config';
import { encryptProperties } from './RequestEncryption';

const API_URL = config.psslai_api_server_host;
const API_MOCK_URL = config.psslai_api_mock_host;
const isMockData = config.mock_data;

export const apiGetNotifications = async (token, guid) => {
  let endPoint = `${API_URL}/`;
  if (isMockData) {
    endPoint = API_MOCK_URL;
  }

  const apiResponse = await axios
    .get(
      `${endPoint}members/${guid}/notifications`,
      {},
      {
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
          'user-agent': `mobile, ${getUniqueId()}`,
        },
      },
    )
    .then(response => response)
    .catch(error => error.response);
  return apiResponse;
};

export const apiReadNotification = async (token, guid) => {
  let endPoint = `${API_URL}/`;
  if (isMockData) {
    endPoint = API_MOCK_URL;
  }

  const apiResponse = await axios
    .post(
      `${endPoint}members/${guid}/notifications/read`,
      {},
      {
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
          'user-agent': `mobile, ${getUniqueId()}`,
        },
      },
    )
    .then(response => response)
    .catch(error => error);
  return apiResponse;
};
