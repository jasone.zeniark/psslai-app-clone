/* eslint-disable arrow-parens */
/* eslint-disable prettier/prettier */
import axios from 'axios';
import { getUniqueId } from 'react-native-device-info';

import config from './Config';
import { encryptProperties } from './RequestEncryption';

const API_URL = config.psslai_api_server_host;
const API_MOCK_URL = config.psslai_api_mock_host;
const isMockData = config.mock_data;

export const apiSignUpUser = async rawParams => {
  let endPoint = `${API_URL}/`;
  let params = encryptProperties(rawParams);
  if (isMockData) {
    endPoint = API_MOCK_URL;
    params = rawParams;
  }

  const apiResponse = await axios
    .post(`${endPoint}signup`, params, {
      headers: { 'User-Agent': `mobile, ${getUniqueId()}` },
    })
    .then(response => response)
    .catch(error => error.response);
  return apiResponse;
};

export const apiLogin = async rawParams => {
  let endPoint = `${API_URL}/`;
  let params = encryptProperties(rawParams);

  if (isMockData) {
    endPoint = API_MOCK_URL;
    params = rawParams;
  }
  // console.log('host: ', `${endPoint}login`);
  const apiResponse = await axios
    .post(`${endPoint}login`, params, {
      headers: {
        'Content-Type': 'application/json',
        'User-Agent': `mobile, ${getUniqueId()}`,
      },
    })
    .then(response => response)
    .catch(error => (error.response !== undefined ? error.response.data : error));
  // console.log('login DATA', apiResponse);

  return apiResponse;
};

export const apiLogout = token => {
  let endPoint = `${API_URL}/`;
  if (isMockData) {
    endPoint = API_MOCK_URL;
  }
  // console.log('token: ', token);
  const apiResponse = axios
    .post(
      `${endPoint}logout`,
      {},
      {
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
      },
    );
  return apiResponse;
};

export const apiProfile = async (token, guid) => {
  let endPoint = `${API_URL}/`;

  if (isMockData) {
    endPoint = API_MOCK_URL;
    params = token;
  }
  // console.log(`${endPoint}members/${guid}`);
  const apiResponse = await axios
    .get(
      `${endPoint}members/${guid}`,
      {},
      {
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
          'user-agent': `mobile, ${getUniqueId()}`,
        },
      },
    )
    .then(response => response)
    .catch(error => error.response.data);
  return apiResponse;
};

export const apiVerifyOtp = async rawParams => {
  let endPoint = `${API_URL}/`;
  let params = encryptProperties(rawParams);

  if (isMockData) {
    endPoint = API_MOCK_URL;
    params = rawParams;
  }

  const apiResponse = await axios
    .post(`${endPoint}otp/verify`, rawParams, {
      headers: {
        'content-type': 'application/json',
        'user-agent': `mobile, ${getUniqueId()}`,
      },
    })
    .then(response => response)
    .catch(error => error.response.data);

  return apiResponse;
};

export const apiResendOtp = async rawParams => {
  let endPoint = `${API_URL}/`;
  let params = encryptProperties(rawParams);

  if (isMockData) {
    endPoint = API_MOCK_URL;
    params = rawParams;
  }
  // console.log('host: ', endPoint);
  // console.log('resend params: ', rawParams);
  const apiResponse = await axios
    .post(`${endPoint}otp/resend`, rawParams, {
      headers: {
        'content-type': 'application/json',
        'user-agent': `mobile, ${getUniqueId()}`,
      },
    })
    .then(response => response)
    .catch(error => error.response.data);
  return apiResponse;
};

export const apiLoans = async (token, guid) => {
  let endPoint = `${API_URL}/`;
  if (isMockData) {
    endPoint = API_MOCK_URL;
  }
  // console.log('host: ', `${endPoint}members/${guid}/loans`);
  const apiResponse = await axios
    .get(
      `${endPoint}members/${guid}/loans`,
      {},
      {
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
          'user-agent': `mobile, ${getUniqueId()}`,
        },
      },
    )
    .then(response => response)
    .catch(error => error.response.data);
  return apiResponse;
};

export const apiTransactions = async (token, guid) => {
  let endPoint = `${API_URL}/`;
  if (isMockData) {
    endPoint = API_MOCK_URL;
  }
  // console.log('host: ', `${endPoint}members/${guid}/card/transactionHistory`);
  const apiResponse = await axios
    .get(
      `${endPoint}members/${guid}/card/transactionHistory`,
      {},
      {
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
          'user-agent': `mobile, ${getUniqueId()}`,
        },
      },
    )
    .then(response => response)
    .catch(error => error);
  return apiResponse;
};

export const apiLoadMore = async (token, guid, getLastRefNo, getFailKey) => {
  let endPoint = `${API_URL}/`;
  if (isMockData) {
    endPoint = API_MOCK_URL;
  }
  const apiResponse = await axios
    .get(
      `${endPoint}members/${guid}/card/transactionHistory`,
      {
        params: { lastRefNo: getLastRefNo, failKey: getFailKey },
      },
      {
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
          'user-agent': `mobile, ${getUniqueId()}`,
        },
      },
    )
    .then(response => response)
    .catch(error => error);
  return apiResponse;
};

export const apiAccountBalances = async (token, guid) => {
  let endPoint = `${API_URL}/`;
  if (isMockData) {
    endPoint = API_MOCK_URL;
  }
  // console.log('host: ', `${endPoint}members/${guid}/balances`);
  const apiResponse = await axios
    .get(
      `${endPoint}members/${guid}/balances`,
      {},
      {
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
          'user-agent': `mobile, ${getUniqueId()}`,
        },
      },
    )
    .then(response => response)
    .catch(error => error.response.data);
  return apiResponse;
};

export const apiRequestOtp = async rawParams => {
  let endPoint = `${API_URL}/`;
  let params = encryptProperties(rawParams);

  if (isMockData) {
    endPoint = API_MOCK_URL;
    params = rawParams;
  }
  const apiResponse = await axios
    .post(`${endPoint}otp/send`, rawParams, {
      headers: {
        'content-type': 'application/json',
        'user-agent': `mobile, ${getUniqueId()}`,
      },
    })
    .then(response => response)
    .catch(error => error.response);
  return apiResponse;
};

export const apiDeleteAccount = async (token) => {
  let endPoint = `${API_URL}/`;
  if (isMockData) {
    endPoint = API_MOCK_URL;
  }
  const apiResponse = await axios
    .post(`${endPoint}members/account/delete`, {}, {
      headers: {
        'User-Agent': `mobile, ${getUniqueId()}`,
        Authorization: `Bearer ${token}`,
      },
    })
    .then(response => response)
    .catch(error => error.response);
  return apiResponse;
};
