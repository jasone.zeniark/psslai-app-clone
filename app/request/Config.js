import React from 'react';

const configuration = {
  psslai_api_server_host: 'https://mobapi-santorini-dev.psslai.com',
  //psslai_api_server_host: 'https://uat-bilismobile.psslai.com/sprint-updates/',
  // psslai_api_server_host: 'https://api-santorini-dev.psslai.com',
  // psslai_api_server_host: 'https://api-bilisonline.psslai.com',
  psslai_api_mock_host: '',
  psslai_contact_us_url: 'https://www.psslai.com/contact-us/',
  psslai_website: 'https://www.psslai.com',
  psslai_privacy_notice: 'https://www.psslai.com/privacy-notice',
  psslai_terms_conditions: 'https://www.psslai.com/privacy-notice',
  psslai_download_form: 'https://www.psslai.com/news/downloadable-forms/',
  psslai_become_member: 'https://www.psslai.com/regular-members/',
  psslai_login_problem: 'https://bilisonline.psslai.com/#/forgot',
  psslai_terms_conditions: 'https://www.psslai.com/faqs/',
  psslai_apply_visa_card: 'https://www.psslai.com/psslai-visa-card/',
  psslai_contact_us: 'https://www.psslai.com/contact-us/',
  security_encryption_key: '7LNVveTdbXYB4Pej',
  security_idle_timer_sec: 300,
  security_idle_grace_period_sec: 30,
  mock_data: false,
};

export default configuration;
