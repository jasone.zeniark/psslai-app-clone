// import 'react-native-get-random-values';
import React, { useState, useEffect } from 'react';
import { SafeAreaView } from 'react-native';
import { WebView } from 'react-native-webview';
import { useDispatch, useSelector } from 'react-redux';

import Header from '../../globals/header/CommonHeader';
import NetworkScreen from '../../globals/header/NetworkError';
import Loader from '../../globals/loader/UserLoader';
import styles from './Styles';

const UserWebView = ({ navigation, route }) => {
  const login = useSelector(state => state.login);
  const dispatch = useDispatch();

  const [loading, setLoading] = useState(false);
  const [isConnected, setConnected] = useState(true);

  useEffect(() => {
    const unsubscribe = navigation.addListener('blur', () => {
      setLoading(false);
    });

    return unsubscribe;
  }, [navigation]);

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      setLoading(true);
    });

    return unsubscribe;
  }, [navigation]);

  useEffect(() => {
    setConnected(login.connected);
  }, [login.connected]);

  return (
    <>
      <Header title={''} web={true} onBackPress={() => navigation.goBack()} />
      <SafeAreaView style={styles.container}>
        {loading && isConnected && <Loader styles={styles.loader} />}
        {!isConnected ? (
          <NetworkScreen />
        ) : (
          <WebView
            style={loading && { marginTop: 50 }}
            onLoadStart={() => setLoading(true)}
            onLoadEnd={() => setLoading(false)}
            source={{ uri: route.params.url }}
            javaScriptEnabled={true}
            // For the Cache
            domStorageEnabled={true}
          />
        )}
      </SafeAreaView>
    </>
  );
};

export default UserWebView;
