import { StyleSheet, Dimensions } from 'react-native';

import { WHITE } from '../../globals/Theme';

const { height } = Dimensions.get('screen');

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: WHITE,
  },
  loader: {
    position: 'absolute',
    alignSelf: 'center',
    top: '12%',
  },
});

export default Styles;
