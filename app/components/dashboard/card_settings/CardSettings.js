/* eslint-disable no-nested-ternary */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Switch,
  ScrollView,
  Alert,
  SectionList,
  RefreshControl,
  LogBox,
  ActivityIndicator,
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import Moment from 'react-moment';
import moment from 'moment';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

import { array } from 'yup';
import { date } from 'yup/lib/locale';
import {
  reloadCardDetails,
  lockCard,
  unlockCard,
  cardLoading,
} from '../../../store/actions/CardActions';
import { loadUserProfile } from '../../../store/actions/LoginActions';
import { cardStatus, cardErrorMessage } from '../../../globals/utils/Constant';
import { apiCardLock, apiCardUnlock } from '../../../request/CardApi';
import { decryptObject } from '../../../request/RequestEncryption';
import { apiLoadMore, apiTransactions } from '../../../request/Auth';

import Header from '../../../globals/header/CommonHeader';
import Routes from '../../../globals/navigations/Routes';
import PsslaiCard from '../tabs/dashboard/Components/PsslaiCard';
import globalStyles from '../../../globals/styles';
import styles from './Styles';
import Loader from './Components/Loader';
import { RabbitLegacy } from 'crypto-js';

import NetworkScreen from '../../../globals/header/NetworkError';

function CardSettings({ navigation }) {
  const login = useSelector(state => state.login);
  const card = useSelector(state => state.card);
  console.log(card);

  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(false);
  const [isLoadMore, setIsLoadMore] = useState(false);
  const [refreshing, setRefreshing] = useState(false);
  const [transactionHistory, setTransactionHistory] = useState([]);
  const [noTransaction, setNoTransaction] = useState('');
  const [loadMore, setLoadMore] = useState('');
  const [switchValue, setSwitchValue] = useState(
    card.result.memberLocked === 1 || card.result.adminLocked === 1,
  );
  const [isConnected, setConnected] = useState(true);

  const handleTransactions = async () => {
    setLoadMore('');
    const apiResponse = await apiTransactions(
      login.result.token,
      login.result.guid,
    );

    if (typeof apiResponse.data !== 'undefined') {
      const { data } = apiResponse;
      if (typeof data.status !== 'undefined') {
        if (data.status === 1) {
          const result = decryptObject(data.result);
          if (result != '') {
            setTransactionHistory(result);
            console.log('TRANSACTION DATA', result);
            setLoadMore('Tap to load more');
          } else {
            setNoTransaction('No Transaction Records Available');
            setLoadMore('');
          }
        } else {
          let errMsg = '';
          for (let ctr = 0; ctr < data.errors.length; ctr += 1) {
            const errorObj = data.errors[ctr];
            errMsg += `${errorObj.msg}\r\n`;
          }
          alert(data.message);
        }
      } else {
        let errMsg = '';
        Object.keys(data).forEach(key => {
          errMsg += `${data[key]}\r\n`;
        });
        alert(data.message);
      }
    } else {
      console.debug(apiResponse.message);
      alert(apiResponse.message);
    }
  };

  const handleLoadMore = async () => {
    setIsLoadMore(true);
    const apiResponse = await apiLoadMore(
      login.result.token,
      login.result.guid,
      transactionHistory[transactionHistory.length - 1].refNo,
      login.result.getFailKey,
    );

    if (typeof apiResponse.data !== 'undefined') {
      const { data } = apiResponse;
      if (typeof data.status !== 'undefined') {
        if (data.status === 1) {
          const result = decryptObject(data.result);
          if (result != 0) {
            setTransactionHistory(transactionHistory.concat(result));
            // console.log('LOAD MORE DATA', result);
            setIsLoadMore(false);
          } else {
            <></>;
            console.log('No More Transaction History');
            setLoadMore('');
            Alert.alert('Attention', 'No more transaction history to display');
            setIsLoadMore(false);
          }
        } else {
          let errMsg = '';
          for (let ctr = 0; ctr < data.errors.length; ctr += 1) {
            const errorObj = data.errors[ctr];
            errMsg += `${errorObj.msg}\r\n`;
          }
          alert(data.message);
          setIsLoadMore(false);
        }
      } else {
        let errMsg = '';
        Object.keys(data).forEach(key => {
          errMsg += `${data[key]}\r\n`;
        });
        alert(data.message);
        setIsLoadMore(false);
      }
    } else {
      console.debug(apiResponse.message);
      alert(apiResponse.message);
      setIsLoadMore(false);
    }
  };

  const handleSwitchLock = async () => {
    <Loader loading={isLoading} />;
    const apiResponse = await apiCardLock(
      login.result.token,
      login.result.guid,
    );
    console.log('lockData', apiResponse);

    if (typeof apiResponse.data !== 'undefined') {
      const { data } = apiResponse;
      if (typeof data.status !== 'undefined') {
        if (data.status === 1) {
          dispatch(lockCard(data));
          dispatch(reloadCardDetails(login, navigation));
          Alert.alert('Success', data.message);
          setIsLoading(false);
        } else {
          let errMsg = '';
          for (let ctr = 0; ctr < data.errors.length; ctr += 1) {
            const errorObj = data.errors[ctr];
            errMsg += `${errorObj.msg}\r\n`;
          }
          Alert.alert('', data.message);
          setIsLoading(false);
        }
      } else {
        let errMsg = '';
        Object.keys(data).forEach(key => {
          errMsg += `${data[key]}\r\n`;
        });
        Alert.alert('', data.message);
        setIsLoading(false);
      }
    } else {
      Alert.alert('', apiResponse.message);
      dispatch(reloadCardDetails(login, navigation));
      setIsLoading(false);
    }
  };

  const handleSwitchUnlock = async () => {
    const apiResponse = await apiCardUnlock(
      login.result.token,
      login.result.guid,
    );
    console.log('unlockData', apiResponse);

    if (typeof apiResponse.data !== 'undefined') {
      const { data } = apiResponse;
      if (typeof data.status !== 'undefined') {
        if (data.status === 1) {
          dispatch(unlockCard(data));
          dispatch(reloadCardDetails(login, navigation));
          Alert.alert('Success', data.message);
          setIsLoading(false);
        } else {
          let errMsg = '';
          for (let ctr = 0; ctr < data.errors.length; ctr += 1) {
            const errorObj = data.errors[ctr];
            errMsg += `${errorObj.msg}\r\n`;
          }
          Alert.alert('', data.message);
          setIsLoading(false);
        }
      } else {
        let errMsg = '';
        Object.keys(data).forEach(key => {
          errMsg += `${data[key]}\r\n`;
        });
        Alert.alert('', data.message);
        setIsLoading(false);
      }
    } else {
      Alert.alert('', apiResponse.message);
      dispatch(reloadCardDetails(login, navigation));
      setIsLoading(false);
    }
  };

  const reloadData = () => {
    dispatch(loadUserProfile(login, navigation));
    dispatch(reloadCardDetails(login, navigation));
    handleTransactions();
  };

  const toggleSwitch = value => {
    if (isConnected) {
      setSwitchValue(value);
      setIsLoading(true);
      if (value) {
        handleSwitchLock();
      } else {
        handleSwitchUnlock();
      }
    } else {
      Alert.alert(
        'Network Connection Error',
        'Please check your mobile network connection',
      );
    }
  };

  const handleChangeCvv = () => {
    if (typeof card.result.cvvDailyLimitReached !== 'undefined') {
      if (card.result.cvvDailyLimitReached === 0) {
        navigation.navigate(Routes.CHANGE_CVV);
      } else {
        Alert.alert(
          'Generate CVV Error',
          'You can only generate a new CVV once a day.',
        );
      }
    }
  };
  const handleChangeAtmPin = () => {
    navigation.navigate(Routes.CHANGE_ATM_PIN);
  };

  let isCardSuspend = false;
  let isCardDeactivated = false;
  let isCardAdminLocked = false;

  if (
    // eslint-disable-next-line operator-linebreak
    cardStatus.STATUS_SUSPENDED === login.result.profile.cardStatus ||
    cardStatus.STATUS_SUSPENDED === login.result.profile.persoCardStatus
  ) {
    isCardSuspend = true;
  }

  if (
    // eslint-disable-next-line operator-linebreak
    cardStatus.STATUS_DEACTIVATED === login.result.profile.persoCardStatus ||
    cardStatus.STATUS_DEACTIVATED === login.result.profile.cardStatus
  ) {
    isCardDeactivated = true;
  }

  if (card.result.adminLocked === cardStatus.STATUS_ACTIVE) {
    isCardAdminLocked = true;
  }

  useEffect(() => {
    if (login.connected) {
      reloadData();
    }
    LogBox.ignoreLogs(['VirtualizedLists should never be nested']); // hide the sectionList and scrollView issue temporarily.
  }, [login.connected]);

  useEffect(() => {
    if (card.result.memberLocked === 1 || card.result.adminLocked === 1) {
      setSwitchValue(true);
    }
  }, [card.isLoading]);

  const mData = Object.values(
    transactionHistory.reduce((mDate, item) => {
      // eslint-disable-next-line curly
      if (!mDate[moment(item.tranDate).format('MMMM DD, YYYY')])
        // eslint-disable-next-line nonblock-statement-body-position
        mDate[moment(item.tranDate).format('MMMM DD, YYYY')] = {
          title: moment(item.tranDate).format('MMMM DD, YYYY'),
          data: [],
        };
      mDate[moment(item.tranDate).format('MMMM DD, YYYY')].data.push(item);
      return mDate;
    }, {}),
  );

  const renderItems = ({ item }) => (
    <View>
      <View style={styles.history}>
        <View style={styles.listWrapper}>
          <Text style={styles.textDesc}>{item.description}</Text>
          <Text style={styles.textMerchant}>{item.merchant}</Text>
        </View>
        <View style={styles.listWrapper}>
          <Text style={styles.textBalance}>
            - PHP {item.amount.substring(1)}
          </Text>
          <Text style={styles.textRefNo}>REF: {item.refNo}</Text>
        </View>
      </View>
    </View>
  );

  const renderHeader = ({ section }) => (
    <Moment format="MMMM DD, YYYY" element={Text} style={styles.itemWrapper}>
      {section.title}
    </Moment>
  );

  const onRefresh = React.useCallback(() => {
    if (login.connected) {
      setRefreshing(true);
      handleTransactions();
      dispatch(loadUserProfile(login, navigation));
      dispatch(reloadCardDetails(login, navigation));
    }
  }, []);

  useEffect(() => {
    if (card.isLoading === false && login.isLoading === false) {
      setRefreshing(false);
    }
  }, [card.isLoading, login.isLoading]);

  useEffect(() => {
    setConnected(login.connected);
    if (login.connected) {
      onRefresh();
    }
  }, [login.connected]);

  const renderFooter = () => (
    <View style={{ marginTop: 30 }}>
      <ActivityIndicator size="large" />
    </View>
  );

  const handleNetworkLoadMore = () => {
    if (login.connected) {
      handleLoadMore();
    } else {
      Alert.alert(
        'Network Connection Error',
        'Please check your mobile network connection',
      );
    }
  };

  return (
    <>
      <Header title="Card Settings" onBackPress={() => navigation.goBack()} />
      <ScrollView
        refreshControl={
          <RefreshControl
            refreshing={isConnected ? refreshing : false}
            onRefresh={isConnected ? onRefresh : null}
          />
        }
        nestedScrollEnabled={true}
        showsVerticalScrollIndicator={false}
        keyboardShouldPersistTaps="handled"
        style={styles.scrollView}>
        {isConnected ? (
          <>
            {(card.isLoading || login.isLoading || isLoading) && isConnected ? (
              <>
                <View style={styles.subTextWrapper}>
                  <SkeletonPlaceholder backgroundColor="#e2e2e2">
                    <View style={styles.skeletonItemCard} />
                  </SkeletonPlaceholder>
                  <Text style={styles.headerTextLoading}>Card Settings</Text>
                  <SkeletonPlaceholder backgroundColor="#e2e2e2">
                    <View style={styles.skeletonItem1} />
                    <View style={styles.skeletonItem2} />
                    <View style={styles.skeletonItem1} />
                    <View style={styles.skeletonItem2} />
                    <View style={styles.skeletonItem1} />
                    <View style={styles.skeletonItem2} />
                    <View style={styles.skeletonItem1} />
                    <View style={styles.skeletonItem2} />
                  </SkeletonPlaceholder>
                </View>
              </>
            ) : (
              <View pointerEvents={isCardSuspend ? 'none' : 'auto'}>
                <PsslaiCard
                  isActive={card.isActive}
                  data={card.result}
                  info={login.result.profile}
                />
                <View style={styles.mainWrapper}>
                  <Text style={styles.headerText}>Card Settings</Text>
                  {!isCardSuspend ? (
                    isCardDeactivated ? (
                      <Text style={styles.cardSuspendedText}>
                        {cardErrorMessage.CARD_DEACTIVATED}
                      </Text>
                    ) : isCardAdminLocked ? (
                      <Text style={styles.cardSuspendedText}>
                        {cardErrorMessage.CARD_ADMIN_LOCKED}
                      </Text>
                    ) : null
                  ) : (
                    <Text style={styles.cardSuspendedText}>
                      {cardErrorMessage.CARD_SUSPENDED}
                    </Text>
                  )}
                </View>
                <View
                  opacity={
                    isCardSuspend || isCardAdminLocked || isCardDeactivated
                      ? 0.35
                      : 1
                  }>
                  <View style={styles.lockCardWrapper}>
                    <View>
                      <Text style={globalStyles.textBold}>Lock Card</Text>
                    </View>
                  </View>

                  <Switch
                    style={styles.switch}
                    disabled={
                      isCardAdminLocked || isCardSuspend || isCardDeactivated
                    }
                    onValueChange={toggleSwitch}
                    value={switchValue}
                  />

                  <View style={styles.mainWrapper}>
                    <Text style={styles.lockCardText}>
                      Locking your card will lock the ff. features: POS
                      transactions, and ATM withdrawal
                    </Text>
                  </View>
                </View>

                {switchValue ? null : (
                  <View
                    opacity={
                      isCardSuspend || isCardAdminLocked || isCardDeactivated
                        ? 0.35
                        : 1
                    }>
                    <View style={styles.subWrapper}>
                      <Text style={styles.subHeader}>ATM PIN</Text>
                      <TouchableOpacity
                        style={styles.ATMTouchableOpacity}
                        onPress={handleChangeAtmPin}
                        disabled={
                          isCardAdminLocked ||
                          isCardSuspend ||
                          isCardDeactivated
                        }>
                        <Text style={styles.editBtn}> Edit </Text>
                      </TouchableOpacity>
                    </View>
                    <View style={styles.subTextWrapper}>
                      <Text style={styles.subText}>
                        {card.result.datePinChanged !== '' ? (
                          <>
                            Updated last{' '}
                            <Moment format="MMMM DD, YYYY" element={Text}>
                              {card.result.datePinChanged}
                            </Moment>
                          </>
                        ) : (
                          <>No record available</>
                        )}
                      </Text>
                    </View>

                    <View style={styles.subWrapper}>
                      <Text style={styles.subHeader}>Card's CVV</Text>
                      <TouchableOpacity
                        style={styles.CCVTouchableOpacity}
                        onPress={handleChangeCvv}
                        disabled={
                          isCardAdminLocked ||
                          isCardSuspend ||
                          isCardDeactivated
                        }>
                        <Text style={styles.editBtn}> Edit </Text>
                      </TouchableOpacity>
                    </View>
                    <View style={styles.subTextWrapper}>
                      <Text style={styles.subText}>
                        {card.result.dateCvvChanged !== ''
                          ? card.result.cvv
                          : 'N/A'}
                        {card.result.dateCvvChanged !== '' ? (
                          <>
                            {' '}
                            * Updated last{' '}
                            <Moment format="MMMM DD, YYYY" element={Text}>
                              {card.result.dateCvvChanged}
                            </Moment>
                          </>
                        ) : (
                          <></>
                        )}
                      </Text>
                    </View>

                    <View>
                      <View style={styles.subWrapper}>
                        <Text style={styles.subHeader}>Transaction Limit</Text>
                        {/* <TouchableOpacity
                      style={styles.TransactionLimitTouchableOpacity}
                      disabled={
                        isCardAdminLocked || isCardSuspend || isCardDeactivated
                      }>
                      <Text style={styles.editBtn}> Edit </Text>
                    </TouchableOpacity> */}
                      </View>
                      <View style={styles.subTextWrapper}>
                        <Text style={styles.subText}>
                          PHP 10,000.00 per day
                        </Text>
                      </View>
                    </View>
                  </View>
                )}
              </View>
            )}
            {switchValue ? null : (
              <>
                <View style={styles.separator50}>
                  <Text style={styles.headerText}>Transaction History</Text>
                </View>
                <View style={styles.separator20} />

                {card.isLoading && login.isLoading && isConnected ? (
                  <View>
                    <SkeletonPlaceholder backgroundColor="#e2e2e2">
                      <View style={styles.subTextWrapper}>
                        <View style={styles.skeletonItem3} />
                        <View style={styles.skeletonItem4} />
                        <View style={styles.skeletonItem5} />
                        <View style={styles.skeletonItem6} />
                      </View>
                    </SkeletonPlaceholder>
                  </View>
                ) : (
                  <View>
                    <View>
                      <SectionList
                        style={{
                          backgroundColor: 'transparent',
                        }}
                        sections={mData}
                        keyExtractor={(item, index) => item + index}
                        renderItem={renderItems}
                        renderSectionHeader={renderHeader}
                        ListFooterComponent={
                          isLoadMore ? (
                            <View style={styles.footer}>
                              <ActivityIndicator
                                size="large"
                                animating={isLoadMore}
                                style={styles.indicator}
                                color={styles.editBtn.color}
                              />
                            </View>
                          ) : (
                            <View style={styles.footer}>
                              <Text style={styles.noTransactionText}>
                                {noTransaction}
                              </Text>
                              <TouchableOpacity
                                style={styles.loadMoreBtnWrapper}
                                onPress={() => handleNetworkLoadMore()}>
                                <Text style={styles.loadMoreText}>
                                  {loadMore}
                                </Text>
                              </TouchableOpacity>
                            </View>
                          )
                        }
                      />
                    </View>
                  </View>
                )}
              </>
            )}
          </>
        ) : (
          <NetworkScreen />
        )}
      </ScrollView>
    </>
  );
}

export default CardSettings;
