import React from 'react';
import { View, Modal, ActivityIndicator } from 'react-native';
import { normalize } from 'react-native-elements';
import styles from './Styles';
import colors from '../../../../globals/colors';

const Loader = props => {
  const { loading } = props;
  return (
    <Modal transparent={true} animationType={'none'} visible={loading}>
      <View style={styles.modalBackground}>
        <View style={styles.activityIndicatorWrapper}>
          <ActivityIndicator
            animating={loading}
            size="small"
            color={colors.primary}
          />
        </View>
      </View>
    </Modal>
  );
};

export default Loader;
