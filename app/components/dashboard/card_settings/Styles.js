import { StyleSheet, Dimensions } from 'react-native';
import colors from '../../../globals/colors';

const { white, light, primary, red } = colors;
const { width, height } = Dimensions.get('screen');

export default StyleSheet.create({
  topWrapper: {
    flexDirection: 'row',
    marginTop: 15,
  },
  touchableOpacity: {
    flexDirection: 'row',
    width: width / 11,
    marginStart: 20,
  },
  arrowImage: {
    width: 30,
    height: 18,
    justifyContent: 'center',
    alignSelf: 'center',
  },
  topLabel: {
    fontSize: 15,
    fontFamily: 'Montserrat-Bold',
    marginStart: 5,
  },
  scrollView: {
    height,
    backgroundColor: 'rgba(0,0,0,0)',
  },
  mainWrapper: {
    marginTop: 30,
  },
  headerText: {
    marginHorizontal: 20,
    fontSize: 25,
    fontFamily: 'Montserrat-Bold',
  },
  cardSuspendedText: {
    marginHorizontal: 20,
    marginTop: 10,
    fontSize: 16,
    color: red,
    fontFamily: 'Montserrat-Bold',
  },
  headerTextLoading: {
    marginTop: 2,
    marginBottom: 20,
    fontSize: 25,
    fontFamily: 'Montserrat-Bold',
  },
  cardTitle: {
    color: white,
    fontFamily: 'Montserrat-Bold',
    marginHorizontal: 25,
    marginTop: 30,
    fontSize: 20,
  },

  cardView: {
    resizeMode: 'cover',
    height: height / 3.2,
    width: width * 0.9,
    marginTop: -255,
    marginStart: 19,
  },

  cardNumber: {
    color: white,
    fontFamily: 'Montserrat-Bold',
    marginHorizontal: 25,
    marginTop: 5,
  },
  cardBalanceText: {
    color: white,
    fontWeight: '300',
    marginHorizontal: 25,
    marginTop: 40,
    fontSize: 15,
  },
  cardBalanceWrapper: {
    flexDirection: 'row',
    marginStart: 25,
    marginTop: 5,
  },
  cardPesoSign: {
    fontSize: 25,
    fontFamily: 'Montserrat-Bold',
    color: white,
  },
  cardAmount: {
    marginStart: 5,
    fontSize: 25,
    fontFamily: 'Montserrat-Bold',
    color: white,
  },
  lockCardWrapper: {
    marginHorizontal: 20,
    marginTop: 25,
    flexDirection: 'row',
    alignItems: 'center',
  },
  switch: {
    transform: [{ scaleX: 0.9 }, { scaleY: 0.8 }],
    marginStart: '82%',
    marginTop: -15,
  },
  lockCardText: {
    marginTop: -25,
    marginHorizontal: 20,
    fontSize: 16,
    color: light,
    fontFamily: 'Montserrat-Bold',
  },

  subWrapper: {
    marginTop: 25,
    marginHorizontal: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  subHeader: {
    fontFamily: 'Montserrat-Bold',
    fontSize: 15,
  },

  subHeader2: {
    fontFamily: 'Montserrat-Bold',
    fontSize: 15,
    marginStart: 20,
  },

  subText: {
    color: light,
    fontSize: 15,
    fontFamily: 'Montserrat-Bold',
  },
  subTextWrapper: {
    marginTop: 15,
    marginHorizontal: 20,
  },
  ATMTouchableOpacity: {
    // marginStart: '73%',
  },
  CCVTouchableOpacity: {
    // marginStart: '69%',
  },

  TransactionLimitTouchableOpacity: {
    // marginStart: '56.5%',
  },

  editBtn: {
    fontFamily: 'Montserrat-Bold',
    fontSize: 15,
    color: primary,
  },

  safeview: {
    flex: 1,
  },

  footer: {
    height: '100%',
    padding: 10,
    backgroundColor: '#rgba(0,0,0,0)',
  },

  indicator: {
    marginTop: 10,
    paddingBottom: 15,
  },

  separator50: {
    marginTop: 50,
  },

  separator30: {
    marginTop: 30,
  },

  separator20: {
    marginTop: 20,
  },

  history: {
    width: '88%',
    borderWidth: 0.2,
    alignSelf: 'center',
    borderColor: colors.light,
    flexDirection: 'row',
    elevation: 100,
    backgroundColor: colors.white,
  },

  textMerchant: {
    marginStart: 10,
    marginTop: 5,
  },

  textDesc: {
    marginStart: 10,
    fontFamily: 'Montserrat-Bold',
    fontSize: 12,
  },

  textBalance: {
    fontSize: 12,
    alignSelf: 'flex-end',
    marginEnd: 10,
    fontFamily: 'Montserrat-Bold',
  },

  textRefNo: {
    fontSize: 12,
    alignSelf: 'flex-end',
    marginEnd: 10,
    marginTop: 10,
    color: colors.medium,
    fontFamily: 'Montserrat-Bold',
  },

  listWrapper: {
    width: '50%',
    paddingVertical: 15,
  },

  skeletonItem1: {
    width: '100%',
    height: 25,
    borderRadius: 4,
    marginBottom: 15,
  },
  skeletonItem2: {
    width: '100%',
    height: 40,
    borderRadius: 4,
    marginBottom: 30,
  },
  skeletonItemCard: {
    width: '100%',
    height: 222,
    borderRadius: 4,
    marginTop: 20,
    marginBottom: 30,
  },

  skeletonItem3: {
    height: 20,
    width: '50%',
  },

  skeletonItem4: {
    marginTop: 20,
    height: 150,
    width: width * 0.9,
  },

  skeletonItem5: {
    marginTop: 20,
    height: 20,
    width: '50%',
  },

  skeletonItem6: {
    marginTop: 20,
    height: 250,
    width: width * 0.9,
  },

  dateText: {
    marginStart: 20,
    fontSize: 13,
    fontFamily: 'Montserrat-Bold',
  },

  itemWrapper: {
    marginTop: 20,
    marginBottom: 10,
    marginStart: 20,
    fontSize: 15,
    fontFamily: 'Montserrat-Bold',
  },

  transactionWrapper: {
    justifyContent: 'center',
    marginTop: 30,
    alignItems: 'center',
  },

  loadMoreBtnWrapper: {
    alignItems: 'center',
    paddingBottom: 15,
    justifyContent: 'center',
  },

  loadMoreText: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 13,
  },

  noTransactionText: {
    marginLeft: 15,
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 13,
  },
});
