import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  mainWrapper: {
    marginTop: 20,
  },
  contentWrapper: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    width: 'auto',
    paddingHorizontal: 15,
  },
  logo: {
    width: 90,
    height: 25,
  },
  touchableAlign: {
    alignSelf: 'flex-end',
  },
  notificationIcon: {
    width: 25,
    height: 25,
    marginStart: 235,
  },

  safeview: {
    flex: 1,
  },
});
