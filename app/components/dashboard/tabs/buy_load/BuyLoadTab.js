import React, { useState, useEffect, useRef } from 'react';
import {
  SafeAreaView,
  View,
  Image,
  ScrollView,
  TouchableOpacity,
  PanResponder,
} from 'react-native';
import globalStyles from '../../../../globals/styles';
import styles from './Styles';

function BuyLoadTab({ navigation }) {
  return (
    <SafeAreaView>
      <ScrollView style={globalStyles.scrollView}>
        <View style={globalStyles.headerWrapper}>
          <View style={styles.contentWrapper}>
            <Image
              style={styles.logo}
              source={require('../../../../images/assets/psslai_logo.png')}
            />
            <TouchableOpacity style={styles.touchableAlign}>
              <Image
                style={styles.notificationIcon}
                source={require('../../../../images/assets/notification_icon.png')}
              />
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

export default BuyLoadTab;
