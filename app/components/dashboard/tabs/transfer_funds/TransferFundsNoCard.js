import React, { useState, useEffect, useRef } from 'react';
import {
  Text,
  SafeAreaView,
  View,
  Image,
  Modal,
  ScrollView,
  TouchableOpacity,
  PanResponder,
} from 'react-native';

import globalStyles from '../../../../globals/styles';

import { cardStatus } from '../../../../globals/utils/Constant';
import styles from './Styles';
import { ModalPicker } from '../../../../globals/modal/ModalPicker';
import config from '../../../../request/Config';
import { LinkingData, NavigateWeb } from '../../../../globals/Helpers';

const { psslai_apply_visa_card } = config;

// eslint-disable-next-line import/no-named-as-default-member
const { STATUS_PENDING, STATUS_FOR_ACTIVATION } = cardStatus;

function TransferFundsNoCard({ navigation, details }) {
  const handleCardStatusChecker = () => {
    if (details.persoCardStatus === STATUS_PENDING) {
      return 'Personalized card is being processed.';
    }

    if (details.cardStatus === STATUS_PENDING) {
      return 'VISA card is being processed.';
    }

    if (details.cardStatus === STATUS_FOR_ACTIVATION) {
      return 'VISA card is for activation.';
    }
  };

  return (
    <>
      <View style={styles.iconWrapper}>
        <Image source={require('../../../../images/assets/send_money.png')} />
      </View>

      <View style={styles.textWrapper}>
        <Text style={styles.mainText}>
          Send money to banks, wallets, and remittance centers nationwide.
        </Text>
      </View>
      {details.persoCardStatus === STATUS_PENDING ||
      details.cardStatus === STATUS_PENDING ||
      details.cardStatus === STATUS_FOR_ACTIVATION ? (
        <Text style={styles.cardStatusText}>{handleCardStatusChecker()}</Text>
      ) : (
        <TouchableOpacity
          onPress={() => NavigateWeb(navigation, psslai_apply_visa_card)}
          style={styles.mainBtn}>
          <Text style={styles.mainBtnText}>Apply for a VISA Card</Text>
        </TouchableOpacity>
      )}
    </>
  );
}

export default TransferFundsNoCard;
