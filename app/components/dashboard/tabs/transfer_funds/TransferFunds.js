/* eslint-disable indent */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable object-curly-newline */
/* eslint-disable arrow-parens */
/* eslint-disable no-alert */
import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  SafeAreaView,
  Text,
  View,
  Image,
  ScrollView,
  RefreshControl,
  Alert,
} from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import RadioButtonRN from 'radio-buttons-react-native';
import { useFocusEffect } from '@react-navigation/native';

import { Formik, Field } from 'formik';

import * as yup from 'yup';
import {
  apiGetTransferDetails,
  apiTransferOwnAccount,
  apiTransferOtherVisaAccount,
  apiTransferOtherBankAccount,
} from '../../../../request/TransferFund';
import { resetOTP, resendOTP } from '../../../../store/actions/OTPActions';
import { apiRequestOtp } from '../../../../request/Auth';
import { logoutUser } from '../../../../store/actions/LoginActions';
import { decryptObject } from '../../../../request/RequestEncryption';
import FormButton from '../../../../globals/form/button/FormButton';
import { getCardTypeLabel } from '../../../../globals/utils/Common';
import {
  cardStatus,
  cardErrorMessage,
} from '../../../../globals/utils/Constant';
import FormikCurrency from '../../../../globals/form/input/FormikCurrency';
import FormDropdown from '../../../../globals/form/dropdown/FormDropdown';
import FormikInput from '../../../../globals/form/input/FormikInput';
import FormikInputMoney from '../../../../globals/form/input/FormikInputMoney';
import Routes from '../../../../globals/navigations/Routes';
import regExp from '../../../../globals/utils/RegExp';
import { RadioSelectedIcon } from '../../../../images/icons/UserIcons';
import BulletItems from '../../../../globals/bullet/bulletItems';
import globalStyles from '../../../../globals/styles';
import styles from './Styles';
import TransferFundsNoCard from './TransferFundsNoCard';

import NetworkBanner from '../../../../globals/header/NetworkBanner';
import NetworkScreen from '../../../../globals/header/NetworkError';

const TransferFundsTab = ({ navigation }) => {
  let formRef;
  let dropDownSourceRef = React.useRef();
  let dropDownDestinationRef = React.useRef();
  const dispatch = useDispatch();
  const login = useSelector(state => state.login);
  const otp = useSelector(state => state.otp);
  const { token, profile } = login.result;
  const { OTP } = Routes;

  // let allowReloadComponent = true;
  const [isLoading, setIsLoading] = useState(true);
  const [refreshing, setRefreshing] = useState(false);
  const [isCardActive, setIsCardActive] = useState(false);
  const [isCancelling, setIsCancelling] = useState(false);
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [destinationType, setDestinationType] = useState(1);
  const [valueMoney, setValueMoney] = React.useState('');
  const [maxMoneyLength, setMaxMoneyLength] = React.useState(16);

  // original values
  const [sourceAccountList, setSourceAccountList] = useState([]);
  const [bankAccountList, setBankAccountList] = useState([]);
  const [eonAccount, setEonAccount] = useState('');
  const [otherDetails, setOtherDetails] = useState({
    transferPSSLAIToVisaSpiel: '',
    transferToOtherBankCutOff: '',
    transferToOtherBankDailyLimit: '',
    transferToOtherBankFee: '',
    transferToOtherBankMonthlyLimit: '',
  });

  // formated values for dropdowns
  const [sourceAccountOptions, setSourceAccountOptions] = useState([]);
  const [bankAccountOptions, setBankAccountOptions] = useState([]);
  const [eonAccountOptions, setEonAccountOptions] = useState([]);

  const [isConnected, setConnected] = useState(true);

  const destinationTypeList = [
    { value: 1, label: 'Own Account', disabled: true },
    { value: 2, label: 'Other Bank', deactivate: true },
    { value: 3, label: 'PSSLAI VISA Card/EON' },
  ];

  const initialValues = {
    type: destinationType,
    source_account: '',
    destination_account: '',
    transferAmount: '',
    destination_bank: '',
    destination_account_number: '',
    destination_account_name: '',
  };

  const ReminderOwnAccount = [
    {
      bullet: '  •',
      content: (
        <>
          <Text style={styles.reminderText}>
            {otherDetails.transferPSSLAIToVisaSpiel}
          </Text>
        </>
      ),
    },
    {
      bullet: '  •',
      content: (
        <>
          <Text style={styles.reminderText}>
            Fund transfer is temporarily inaccessible during holidays, weekends, and after office hours.
          </Text>
        </>
      ),
    },
  ];
  const ReminderOtherBank = [
    {
      bullet: '  •',
      content: (
        <>
          <Text style={styles.reminderText}>
          Fund Transfer is thru Pesonet with transaction fee of {otherDetails.transferToOtherBankFee}
          </Text>
        </>
      ),
    },
    {
      bullet: '  •',
      content: (
        <>
          <Text style={styles.reminderText}>
            Fund Transfer limit is {otherDetails.transferToOtherBankDailyLimit}{' '}
            per day or total of {otherDetails.transferToOtherBankMonthlyLimit}{' '}
            per month
          </Text>
        </>
      ),
    },
    {
      bullet: '  •',
      content: (
        <>
          <Text style={styles.reminderText}>
            Cutoff time is {otherDetails.transferToOtherBankCutOff}{' '}
            (Transaction after the cut-off time will be processed on the next
            banking day)
          </Text>
        </>
      ),
    },
    {
      bullet: '  •',
      content: ( 
        <>
          <Text style={styles.reminderText}>
            Fund transfer is temporarily inaccessible during holidays, weekends, and after office hours.
          </Text>
        </>
      ),
    },
  ];

  const signUpValidationSchema = yup.object().shape({
    transferAmount: yup
      .string()
      .matches(regExp.money, 'Invalid Amount')
      // eslint-disable-next-line prettier/prettier
      .test('Is positive?', 'Zero and negative amount is not allowed', value =>
        value ? value.replace(/,/g, '') > 0 : value > 0,
      ),
    // source_account: yup.string().required(destinationType === 1 && 'Required'),
    source_account: yup.string().when('type', {
      is: 1, // own account
      then: yup.string().required(),
    }),
    destination_account: yup.string().when('type', {
      is: 3, // PSSLAI VISA Card/EON
      then: yup
        .string()
        .required('Destination account is required')
        .min(12, () => 'Should be 12 digits only')
        .max(12, () => 'Should be 12 digits only'),
    }),
    destination_bank: yup.string().when('type', {
      is: 2, // Other Bank
      then: yup.string().required('Destination bank is required'),
    }),
    destination_account_number: yup.string().when('type', {
      is: 2, // Other Bank
      then: yup.string().required('Destination account number is required'),
    }),
    destination_account_name: yup.string().when('type', {
      is: 2, // Other Bank
      then: yup.string().required('Destination account name is required'),
    }),
  });

  // create case here to rebuild the accountlist
  const getTransferDetails = async () => {
    setIsLoading(true);
    dispatch(resetOTP());
    const apiResponse = await apiGetTransferDetails(token);

    if (apiResponse.status === 200) {
      const data = decryptObject(apiResponse.data.result);
      console.log('card', data);
      // Assemblying sourceAccounts Data
      const rawSourceAccounts = data.sourceAccounts;
      let sourceAccountArray = [];
      Object.values(rawSourceAccounts).forEach(val => {
        sourceAccountArray = [
          ...sourceAccountArray,
          `${getCardTypeLabel(val.type)} • ${val.accountNumber}`,
        ];
      });
      setSourceAccountOptions(sourceAccountArray);
      setSourceAccountList(rawSourceAccounts);

      // Assemblying EON account Data
      const rawEonAccount = data.eonAccount;
      setEonAccountOptions([`EON • ${rawEonAccount}`]);
      setEonAccount(rawEonAccount);

      // Assemblying Bank account Data
      const rawBankAccounts = data.bankList;
      let bankAccountArray = [];
      Object.values(rawBankAccounts).forEach(val => {
        bankAccountArray = [...bankAccountArray, `${val.bankName}`];
      });
      setBankAccountOptions(bankAccountArray);
      setBankAccountList(rawBankAccounts);
      setIsLoading(false);
      setRefreshing(false);

      // Other details
      const rawOtherDetails = data.otherDetails;
      setOtherDetails(rawOtherDetails);
    } else {
      // handle errors here
      const { data } = apiResponse;
      if (typeof data.errors !== 'undefined') {
        let errMsg = '';
        for (let ctr = 0; ctr < data.errors.length; ctr += 1) {
          const errorObj = data.errors[ctr];
          errMsg += `${errorObj.msg}\r\n`;
        }
        Alert.alert('Transfer Details Error', errMsg);
        navigation.goBack();
        setRefreshing(false);
      } else {
        // eslint-disable-next-line no-lonely-if
        if (apiResponse.status === 401) {
          Alert.alert('Authentication Error', data.message);
          dispatch(logoutUser({ message: data.message, status: 0 }));
          navigation.navigate(Routes.LOGIN);
        } else {
          Alert.alert('Error', data.message);
        }
        setRefreshing(false);
      }
    }
  };

  const handleResetForm = (isSuccess = false) => {
    const sourceRef = dropDownSourceRef;

    if (eonAccount !== '' || isSuccess) {
      // Destination Bank Reset
      if (destinationType === 2) {
        const destinationRef = dropDownDestinationRef;
        destinationRef.select(-1);
      }
      sourceRef.select(-1);
      formRef.resetForm();
      setValueMoney('0');
    }
  };

  const handleCancel = () => {
    navigation.goBack();
    setIsCancelling(true);
  };

  // Handler for resending OTP code.
  const handleOTP = async () => {
    dispatch(resetOTP());
    const params = {
      memberGuid: login.result.guid,
    };
    const apiResponse = await apiRequestOtp(params);
    console.log('OTP request response: ', apiResponse);

    if (apiResponse.status === 200) {
      const { data } = apiResponse;
      dispatch(resendOTP(data));
      navigation.navigate(OTP, {
        prevRoute: Routes.TRANSFER_FUNDS,
        prevData: login,
        requestOTP: true,
      });
      setIsSubmitting(false);
    } else {
      // handle errors here
      const { data } = apiResponse;
      if (typeof data.errors !== 'undefined') {
        let errMsg = '';
        for (let ctr = 0; ctr < data.errors.length; ctr += 1) {
          const errorObj = data.errors[ctr];
          errMsg += `${errorObj.msg}\r\n`;
        }
        setIsSubmitting(false);
        Alert.alert('OTP error', errMsg);
      } else {
        setIsSubmitting(false);
        Alert.alert('OTP error', data.message);
        if (apiResponse.status === 401) {
          dispatch(logoutUser({ message: data.message, status: 0 }));
          navigation.navigate(Routes.LOGIN);
        }
      }
    }
  };

  const handleFormSubmit = async (formData, { resetForm, setFieldValue }) => {
    const sourceRef = dropDownSourceRef;
    const destinationRef = dropDownDestinationRef;
    setIsSubmitting(true);

    const resetData = () => {
      // Destination Bank Reset
      if (destinationType === 2) {
        destinationRef.select(-1);
      }
      sourceRef.select(-1);
      setValueMoney('');
      resetForm({});
      setIsSubmitting(false);
      setFieldValue('type', destinationType);
    };

    const handleApiResponse = apiResponse => {
      if (apiResponse.status === 200) {
        const { data } = apiResponse;
        Alert.alert('Success', data.message);
        resetData();
        // allowReloadComponent = true;
      } else {
        // handle errors here
        const { data } = apiResponse;
        if (typeof data.errors !== 'undefined') {
          let errMsg = '';
          for (let ctr = 0; ctr < data.errors.length; ctr += 1) {
            const errorObj = data.errors[ctr];
            errMsg += `${errorObj.msg}\r\n`;
          }
          Alert.alert('Transfer fund error', errMsg);
          setIsSubmitting(false);
        } else {
          setIsSubmitting(false);
          Alert.alert('Transfer fund error', data.message);
          if (apiResponse.status === 401) {
            dispatch(logoutUser({ message: data.message, status: 0 }));
            navigation.navigate(Routes.LOGIN);
          }
        }
        // allowReloadComponent = true;
      }
    };
    if (!isConnected) {
      Alert.alert(
        'Network Connection Error',
        'Please check your mobile network connection',
      );

      setIsSubmitting(false);
    } else if (otp.isSuccess === true) {
      dispatch(resetOTP());
      if (destinationType === 1) {
        // Transfer Own account
        const apiParams = {
          source_account: sourceAccountList[formData.source_account].type,
          amount: parseFloat(formData.transferAmount.replace(/,/g, '')),
        };
        console.log('Sending FT Own account Params: ', apiParams);
        const apiResponse = await apiTransferOwnAccount(apiParams, token);
        console.log('apiResponse', apiResponse);
        handleApiResponse(apiResponse);
      } else if (destinationType === 3) {
        // Transfer PSSLAI VISA Card/EON
        const apiParams = {
          destination_account: formData.destination_account.replace(/-/g, ''),
          amount: parseFloat(formData.transferAmount.replace(/,/g, '')),
        };
        console.log('Sending FT Other VISA Params: ', apiParams);
        const apiResponse = await apiTransferOtherVisaAccount(apiParams, token);
        console.log('apiResponse', apiResponse);
        handleApiResponse(apiResponse);
      } else if (destinationType === 2) {
        // Transfer PSSLAI VISA Card/EON
        const apiParams = {
          destination_bank: bankAccountList[formData.destination_bank].guid,
          destination_account_number: formData.destination_account_number,
          destination_account_name: formData.destination_account_name,
          amount: parseFloat(formData.transferAmount.replace(/,/g, '')),
        };
        console.log('Sending FT Other Bank Params: ', apiParams);
        const apiResponse = await apiTransferOtherBankAccount(apiParams, token);
        console.log('apiResponse', apiResponse);
        handleApiResponse(apiResponse);
      }
    } else {
      // console.log('request OTP');
      // allowReloadComponent = false;
      handleOTP();
    }
  };

  const handleDesticationType = (e, resetForm, setFieldValue) => {
    resetForm();
    setValueMoney('');
    setDestinationType(e.value);
    setFieldValue('type', e.value);
  };

  const handleCardStatusChecker = () => {
    setIsCardActive(false);
    if (
      [cardStatus.STATUS_ACTIVE, cardStatus.STATUS_SUSPENDED].includes(
        login.result.profile.cardStatus,
      ) ||
      [cardStatus.STATUS_ACTIVE, cardStatus.STATUS_SUSPENDED].includes(
        login.result.profile.persoCardStatus,
      )
    ) {
      setIsCardActive(true);
    }
  };

  useFocusEffect(
    React.useCallback(() => {
      setIsCancelling(false);
      // setIsLoading(true);
      if (
        // eslint-disable-next-line operator-linebreak
        cardStatus.STATUS_SUSPENDED === login.result.profile.cardStatus ||
        cardStatus.STATUS_SUSPENDED === login.result.profile.persoCardStatus
      ) {
        Alert.alert('Card Suspended', 'Fund Transfer is not available');
        navigation.goBack();
      }
    }, []),
  );
  useEffect(() => {
    if (login.connected) {
      getTransferDetails();
      handleCardStatusChecker();
    }
  }, []);

  useEffect(() => {
    if (!isLoading && isConnected && isCardActive) {
      setValueMoney('');
      if (isCardActive) {
        handleResetForm();
      }
    }
  }, [isLoading]);

  useEffect(() => {
    if (!isLoading && isConnected) {
      if (otp.isSuccess === true) {
        formRef.handleSubmit();
        // console.log('success OTP!');
      }
    }
  }, [otp.isSuccess]);

  const onRefresh = React.useCallback(() => {
    if (login.connected) {
      setRefreshing(true);
      getTransferDetails();
      handleCardStatusChecker();
      setDestinationType(1); // Default State
    }
  }, []);

  useEffect(() => {
    setConnected(login.connected);
    if (login.connected) {
      onRefresh();
    }
  }, [login.connected]);

  return (
    // isSubmitting
    <SafeAreaView style={[styles.safeview]}>
      <NetworkBanner connected={isConnected} />
      <ScrollView
        style={globalStyles.scrollView}
        refreshControl={
          <RefreshControl
            refreshing={isConnected ? refreshing : false}
            // eslint-disable-next-line no-nested-ternary
            onRefresh={isSubmitting ? '' : isConnected ? onRefresh : null}
          />
        }>
        {isConnected ? (
          <>
            <View style={globalStyles.headerWrapper}>
              <View style={styles.contentWrapper}>
                <Image
                  style={styles.logo}
                  // eslint-disable-next-line global-require
                  source={require('../../../../images/assets/psslai_logo.png')}
                />
              </View>
            </View>
            {!isCardActive ? (
              <>
                <TransferFundsNoCard
                  navigation={navigation}
                  details={profile}
                />
              </>
            ) : (
              <>
                <View style={styles.formWrapper}>
                  {isLoading && isConnected ? (
                    <>
                      <SkeletonPlaceholder backgroundColor="#e2e2e2">
                        <View style={styles.skeletonItem5} />
                        <View style={styles.skeletonItem1} />
                        <View style={styles.skeletonItem1} />
                        <View style={styles.skeletonItem1} />
                        <View style={styles.skeletonItem4} />
                        <View style={styles.skeletonItem2} />
                        <View style={styles.skeletonItem3} />
                        <View style={styles.skeletonItem2} />
                        <View style={styles.skeletonItem3} />
                        <View style={styles.skeletonItem2} />
                        <View style={styles.skeletonItem6} />
                        <View style={styles.skeletonItem2} />
                      </SkeletonPlaceholder>
                    </>
                  ) : (
                    <>
                      {eonAccount === '' ? (
                        <>
                          <TransferFundsNoCard navigation={navigation} />
                        </>
                      ) : (
                        <>
                          <Formik
                            // eslint-disable-next-line no-return-assign
                            innerRef={p => (formRef = p)}
                            validationSchema={signUpValidationSchema}
                            initialValues={initialValues}
                            onSubmit={handleFormSubmit}>
                            {({
                              handleSubmit,
                              values,
                              setFieldValue,
                              isValid,
                              resetForm,
                            }) => (
                              <>
                                <View
                                  style={styles.radioWrapper}
                                  pointerEvents={`${isSubmitting ? 'none' : 'auto'
                                    }`}>
                                  <Text style={styles.formLabel}>
                                    Destination
                                  </Text>
                                  <RadioButtonRN
                                    data={destinationTypeList}
                                    // eslint-disable-next-line max-len
                                    // eslint-disable-next-line prettier/prettier
                                    selectedBtn={e =>
                                      handleDesticationType(
                                        e,
                                        resetForm,
                                        setFieldValue,
                                      )
                                    }
                                    deactiveColor="#cacaca"
                                    box={false}
                                    duration={0}
                                    circleSize={8}
                                    icon={<RadioSelectedIcon />}
                                    initial={destinationType}
                                    textStyle={styles.radioText}
                                    boxStyle={styles.radioBox}
                                  />
                                </View>

                                {destinationType === 1 ? (
                                  <>
                                    <FormDropdown
                                      label="Source Account"
                                      options={sourceAccountOptions}
                                      defaultValue="Select source account..."
                                      // eslint-disable-next-line no-return-assign
                                      ref={ref => (dropDownSourceRef = ref)}
                                      name="source_account"
                                      onSelect={e =>
                                        setFieldValue('source_account', e)
                                      }
                                      disabled={isSubmitting}
                                    />

                                    <FormDropdown
                                      label="Destination Account"
                                      options={eonAccountOptions}
                                      defaultValue={eonAccountOptions[0]}
                                      disabled={true}
                                      name="destination_account"
                                      // eslint-disable-next-line prettier/prettier
                                      onSelect={e =>
                                        setFieldValue('destination_account', e)
                                      }
                                    />
                                    <Field
                                      component={FormikInputMoney}
                                      name="transferAmount"
                                      placeholderText="0.00"
                                      labelText="Amount"
                                      maxLength={maxMoneyLength}
                                      keyboardType={'numeric'}
                                      editable={!isSubmitting}
                                      setFieldValue={setFieldValue}
                                      setMaxMoneyLength={setMaxMoneyLength}
                                    />
                                    <Text style={styles.reminderHead}>
                                      Reminder
                                    </Text>
                                    <BulletItems
                                      list={ReminderOwnAccount}
                                      bulletWidth={20}
                                    />
                                  </>
                                ) : (
                                  <></>
                                )}

                                {destinationType === 3 ? (
                                  <>
                                    <FormDropdown
                                      label="Source Account"
                                      options={eonAccountOptions}
                                      defaultValue={eonAccountOptions[0]}
                                      // eslint-disable-next-line no-return-assign
                                      ref={ref => (dropDownSourceRef = ref)}
                                      disabled={true}
                                      name="source_account"
                                      onSelect={e =>
                                        setFieldValue('source_account', e)
                                      }
                                    />
                                    <Field
                                      component={FormikInput}
                                      name="destination_account"
                                      placeholderText="Enter 12-digit EON Account"
                                      labelText="Destination Account"
                                      maxLength={12}
                                      keyboardType={'numeric'}
                                      editable={!isSubmitting}
                                    />
                                    <Field
                                      component={FormikInputMoney}
                                      name="transferAmount"
                                      placeholderText="0.00"
                                      labelText="Amount"
                                      maxLength={maxMoneyLength}
                                      keyboardType={'numeric'}
                                      editable={!isSubmitting}
                                      setFieldValue={setFieldValue}
                                      setMaxMoneyLength={setMaxMoneyLength}
                                    />
                                  </>
                                ) : (
                                  <></>
                                )}

                                {destinationType === 2 ? (
                                  <>
                                    <FormDropdown
                                      label="Source Account"
                                      options={eonAccountOptions}
                                      defaultValue={eonAccountOptions[0]}
                                      // eslint-disable-next-line no-return-assign
                                      ref={ref => (dropDownSourceRef = ref)}
                                      disabled={true}
                                      name="source_account"
                                      onSelect={e =>
                                        setFieldValue('source_account', e)
                                      }
                                    />
                                    <FormDropdown
                                      label="Destination Bank"
                                      options={bankAccountOptions}
                                      defaultValue="Select a bank..."
                                      // eslint-disable-next-line no-return-assign
                                      ref={ref =>
                                        (dropDownDestinationRef = ref)
                                      }
                                      name="destination_bank"
                                      onSelect={e =>
                                        setFieldValue('destination_bank', e)
                                      }
                                      disabled={isSubmitting}
                                    />
                                    <Field
                                      component={FormikInput}
                                      name="destination_account_number"
                                      placeholderText="Account Number"
                                      labelText="Destination Account Number"
                                      keyboardType={'numeric'}
                                      editable={!isSubmitting}
                                    />
                                    <Field
                                      component={FormikInput}
                                      name="destination_account_name"
                                      placeholderText="Account Name"
                                      labelText="Destination Account Name"
                                      editable={!isSubmitting}
                                    />
                                    <Field
                                      component={FormikInputMoney}
                                      name="transferAmount"
                                      placeholderText="0.00"
                                      labelText="Amount"
                                      maxLength={maxMoneyLength}
                                      keyboardType={'numeric'}
                                      editable={!isSubmitting}
                                      setFieldValue={setFieldValue}
                                      setMaxMoneyLength={setMaxMoneyLength}
                                    />
                                    <Text style={styles.reminderHead}>
                                      Reminders
                                    </Text>
                                    <BulletItems
                                      list={ReminderOtherBank}
                                      bulletWidth={20}
                                    />
                                  </>
                                ) : (
                                  <></>
                                )}
                                <FormButton
                                  title={`Transfer PHP ${values.transferAmount
                                    ? values.transferAmount
                                    : '0.00'
                                    }`}
                                  titleStyle={styles.submitText}
                                  buttonStyle={styles.submitButton}
                                  onPress={handleSubmit}
                                  disabled={
                                    !isValid || values.transferAmount === 0
                                  }
                                  // disabled={!isValid}
                                  loading={isSubmitting}
                                  disabledStyle={styles.submitBtnDisabled}
                                  disabledTitleStyle={
                                    styles.submitBtnTextDisabled
                                  }
                                />
                                <View style={styles.cancelBtnWrapper}>
                                  <FormButton
                                    title={'Cancel'}
                                    titleStyle={styles.cancelText}
                                    buttonStyle={styles.cancelButton}
                                    disabled={isSubmitting || isCancelling}
                                    // onPress={Tester}
                                    onPress={handleCancel}
                                    disabledStyle={styles.cancelBtnDisabled}
                                  />
                                </View>
                              </>
                            )}
                          </Formik>
                        </>
                      )}
                    </>
                  )}
                </View>
              </>
            )}
          </>
        ) : (
          <NetworkScreen />
        )}
      </ScrollView>
    </SafeAreaView>
  );
};

export default TransferFundsTab;
