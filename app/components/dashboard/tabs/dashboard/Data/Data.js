export const dummyData = [
  {
    url: 'https://img.icons8.com/ios/72/atm.png',
    description: 'Withdraw cash from any Unionbank/Bancnet/Visa ATM',
    id: 1,
  },
  {
    url: 'https://img.icons8.com/ios/72/atm.png',
    description: 'View your balance and transactions in real-time',
    id: 2,
  },
  {
    url: 'https://img.icons8.com/ios/72/atm.png',
    description: 'Pay your bills online ',
    id: 3,
  },
  {
    url: 'https://img.icons8.com/ios/72/atm.png',
    description: 'Transfer funds to a UBP/Local/eMoney Wallet Accounts',
    id: 4,
  },
];
