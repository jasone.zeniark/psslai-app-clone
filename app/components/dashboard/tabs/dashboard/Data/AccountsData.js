export const accountDummyData = [
  {
    type: 'Savings',
    currency: '₱',
    accounts: '01900000',
    outstanding: '32,800.00',
    withdrawable: '22,800.00',
    id: 1,
  },
  {
    type: 'Capcon',
    currency: '₱',
    accounts: '0199630',
    outstanding: '200.50',
    withdrawable: '100.00',
    id: 2,
  },
  {
    type: 'CASA',
    currency: '₱',
    accounts: '1199620',
    outstanding: '0.00',
    withdrawable: '0.00',
    id: 3,
  },
];

export const accountDummyData2 = [
  {
    type: 'CA',
    accountNumber: '0100928184',
    outstandingBalance: '2,765.25',
    withdrawableBalance: '1,765.25',
    name: 'Capital Contribution',
  },
  {
    type: 'CS',
    accountNumber: '0500151336',
    outstandingBalance: '20,048.35',
    withdrawableBalance: '20,047.35',
    name: 'Cash Advanced Storage Account',
  },
];
