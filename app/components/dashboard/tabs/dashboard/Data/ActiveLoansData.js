export const activeLoansDummyData = [
  {
    url: 'https://img.icons8.com/material/64/000000/car--v1.png',
    type: 'Auto',
    currency: '₱',
    lrrpNumber: '0910008',
    outstanding: '120,800.00',
    id: 1,
  },

  {
    url: 'https://img.icons8.com/ios/64/000000/wallet--v1.png',
    type: 'Salary',
    currency: '₱',
    lrrpNumber: '0910009',
    outstanding: '21,700.00',
    id: 2,
  },
  {
    url: 'https://img.icons8.com/ios/50/000000/home--v1.png',
    type: 'Real Estate',
    currency: '₱',
    lrrpNumber: '0910010',
    outstanding: '1,290,875.00',
    id: 3,
  },
];

export const ActiveLoansDummyData2 = [
  {
    type: 'FD',
    loanId: '022019-0004431AL',
    renewalBalance: '74,395.06',
    renewable: false,
    realRenewalBalance: '79,900.06',
    prin_out_balance: '74,395.06',
    monthlyAmort: '3,527.78',
    accumulatedUnpaidAmount: '0.00',
    name: 'AffordaLoan',
  },
];
