export const loanImageData = [
  {
    type: 'AL',
    image: require('../../../../../images/assets/Loan_AL.png'),
  },
  {
    type: 'AP',
    image: require('../../../../../images/assets/Loan_AP.png'),
  },
  {
    type: 'AS',
    image: require('../../../../../images/assets/Loan_AS.png'),
  },
  {
    type: 'BL',
    image: require('../../../../../images/assets/Loan_BL.png'),
  },
  {
    type: 'BL-1',
    image: require('../../../../../images/assets/Loan_BL-1.png'),
  },
  {
    type: 'BL-2',
    image: require('../../../../../images/assets/Loan_BL-2.png'),
  },
  {
    type: 'BL-3',
    image: require('../../../../../images/assets/Loan_BL-3.png'),
  },
  {
    type: 'BL-4',
    image: require('../../../../../images/assets/Loan_BL-4.png'),
  },
  {
    type: 'BL-5',
    image: require('../../../../../images/assets/Loan_BL-5.png'),
  },
  {
    type: 'BL-CRL1',
    image: require('../../../../../images/assets/Loan_BL-CRL1.png'),
  },
  {
    type: 'BL-CRL2',
    image: require('../../../../../images/assets/Loan_BL-CRL2.png'),
  },
  {
    type: 'BL-CRL3',
    image: require('../../../../../images/assets/Loan_BL-CRL3.png'),
  },
  {
    type: 'BL-CRL4',
    image: require('../../../../../images/assets/Loan_BL-CRL4.png'),
  },
  {
    type: 'BL-PVL',
    image: require('../../../../../images/assets/Loan_BL-PVL.png'),
  },
  {
    type: 'BL-VL',
    image: require('../../../../../images/assets/Loan_BL-VL.png'),
  },
  {
    type: 'CL',
    image: require('../../../../../images/assets/Loan_CL.png'),
  },
  {
    type: 'EM',
    image: require('../../../../../images/assets/Loan_EM.png'),
  },
  {
    type: 'MT',
    image: require('../../../../../images/assets/Loan_MT.png'),
  },
  {
    type: 'OP',
    image: require('../../../../../images/assets/Loan_OP.png'),
  },
  {
    type: 'PL',
    image: require('../../../../../images/assets/Loan_PL.png'),
  },
  {
    type: 'SL',
    image: require('../../../../../images/assets/Loan_SL.png'),
  },
];
