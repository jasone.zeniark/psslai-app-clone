/* eslint-disable operator-linebreak */
/* eslint-disable no-nested-ternary */
/* eslint-disable arrow-parens */
/* eslint-disable no-alert */
/* eslint-disable no-unused-vars */
/* eslint-disable camelcase */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from 'react';
import {
  Text,
  SafeAreaView,
  View,
  Image,
  Alert,
  RefreshControl,
  TouchableWithoutFeedback,
  ScrollView,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

import PageView from './Components/PageView';
import AccountsView from './Components/AccountBalanceView';
import ActiveLoansView from './Components/ActiveLoansView';
import PsslaiCard from './Components/PsslaiCard';
import { dummyData } from './Data/Data';
import { ActiveLoansDummyData2 } from './Data/ActiveLoansData';

import config from '../../../../request/Config';
import { apiCardDetails } from '../../../../request/CardApi';
import { decryptObject } from '../../../../request/RequestEncryption';

import { cardStatus } from '../../../../globals/utils/Constant';
import CustomAlert from '../../../../globals/alert/CustomAlert';
import Loader from '../../../../globals/loader/UserLoader';
import FormButton from '../../../../globals/form/button/FormButton';
import globalStyles from '../../../../globals/styles';
import routes from '../../../../globals/navigations/Routes';
import {
  MembershipIcon,
  InfoOutlineIcon,
  LocationIcon,
  CloseIcon,
} from '../../../../images/icons/UserIcons';
import { NavigateWeb, LinkingData } from '../../../../globals/Helpers';

import {
  cardLoaded,
  resetCardDetails,
  fetchCardDetails,
  cardLoading,
} from '../../../../store/actions/CardActions';

import { logoutUser } from '../../../../store/actions/LoginActions';

import NetworkBanner from '../../../../globals/header/NetworkBanner';
import NetworkScreen from '../../../../globals/header/NetworkError';

import styles from './Styles';
import { apiLoans, apiAccountBalances } from '../../../../request/Auth';

const { psslai_apply_visa_card } = config;

const {
  STATUS_NO_LINKED_CARD,
  STATUS_ACTIVE,
  STATUS_PENDING,
  STATUS_FOR_ACTIVATION,
  STATUS_DEACTIVATED,
  STATUS_SUSPENDED,
  STATUS_PERSO_CARD_ACTIVATION_RETRY,
} = cardStatus;

function DashboardTab({ navigation }) {
  // eslint-disable-next-line arrow-parens
  const login = useSelector(state => state.login);
  const dispatch = useDispatch();
  const [loans, setLoans] = useState([]);
  const [accountBalances, setAccountBalances] = useState([]);
  const [cardState, setCardState] = useState('');

  const [details, setDetails] = useState({});
  const [refreshing, setRefreshing] = useState(false);
  const [isLoading, setLoading] = useState(false);
  const [isBalanceLoading, setBalanceLoading] = useState(false);
  const [isLoanLoading, setLoanLoading] = useState(false);
  const [isActiveCard, setActiveCard] = useState(false);
  const [isReplaceVisible, setReplaceVisible] = useState(false);
  const [isActivateVisible, setActivateVisible] = useState(false);
  const [isUpgradeVisible, setUpgradeVisible] = useState(false);
  const [isConnected, setConnected] = useState(true);

  const toggleReplace = () => {
    setReplaceVisible(!isReplaceVisible);
  };

  const toggleActivate = () => {
    setActivateVisible(!isActivateVisible);
  };

  const toggleUpgrade = () => {
    setUpgradeVisible(!isUpgradeVisible);
  };

  const navigateBilisOnline = () =>
    LinkingData('https://bilisonline.psslai.com');

  const handleLoans = async () => {
    setLoanLoading(true);
    const apiResponse = await apiLoans(login.result.token, login.result.guid);
    if (typeof apiResponse.data !== 'undefined') {
      const { data } = apiResponse;
      if (typeof data.status !== 'undefined') {
        if (data.status === 1) {
          const result = decryptObject(data.results);
          setLoans(result);
          setLoanLoading(false);
        } else {
          let errMsg = '';
          for (let ctr = 0; ctr < data.errors.length; ctr += 1) {
            const errorObj = data.errors[ctr];
            errMsg += `${errorObj.msg}\r\n`;
          }
          setLoanLoading(false);
          alert(data.message);
        }
      } else {
        let errMsg = '';
        Object.keys(data).forEach(key => {
          errMsg += `${data[key]}\r\n`;
        });
        setLoanLoading(false);
        alert(data.message);
      }
    } else {
      setLoanLoading(false);
      console.log(apiResponse.message);
    }
  };

  const handleAccountBalances = async () => {
    setBalanceLoading(true);
    const apiResponse = await apiAccountBalances(
      login.result.token,
      login.result.guid,
    );
    if (typeof apiResponse.data !== 'undefined') {
      const { data } = apiResponse;
      if (typeof data.status !== 'undefined') {
        if (data.status === 1) {
          const result = decryptObject(data.results);
          setAccountBalances(result);
          setBalanceLoading(false);
        } else {
          let errMsg = '';
          for (let ctr = 0; ctr < data.errors.length; ctr += 1) {
            const errorObj = data.errors[ctr];
            errMsg += `${errorObj.msg}\r\n`;
          }
          setBalanceLoading(false);
          alert(data.message);
        }
      } else {
        let errMsg = '';
        Object.keys(data).forEach(key => {
          errMsg += `${data[key]}\r\n`;
        });
        setBalanceLoading(false);
        alert(data.message);
      }
    } else {
      setBalanceLoading(false);
      console.log(apiResponse.message);
    }
  };

  // Handler for fetching card details.
  const handleFetchCard = async () => {
    setLoading(true);
    dispatch(resetCardDetails());
    dispatch(cardLoading());

    const apiResponse = await apiCardDetails(
      login.result.token,
      login.result.guid,
    );

    // console.log('Card details response:', apiResponse);

    if (apiResponse.status === 200) {
      const { data } = apiResponse;
      const result = decryptObject(data.result);
      // console.log('Card details result:', result);
      dispatch(fetchCardDetails(result));
      dispatch(cardLoaded());
      setDetails(result);
      setActiveCard(true);
      setLoading(false);
    } else {
      // handle errors here
      const { data } = apiResponse;
      if (typeof data.errors !== 'undefined') {
        let errMsg = '';
        for (let ctr = 0; ctr < data.errors.length; ctr += 1) {
          const errorObj = data.errors[ctr];
          errMsg += `${errorObj.msg}\r\n`;
        }
        setActiveCard(true);
        setLoading(false);
        Alert.alert('Card Details Error', errMsg);
      } else {
        setActiveCard(false);
        setLoading(false);
        if (data.message !== 'No active Visa Card') {
          if (apiResponse.status === 401) {
            Alert.alert('Authentication Error', data.message);
            dispatch(logoutUser({ message: data.message, status: 0 }));
            navigation.navigate(routes.LOGIN);
          } else {
            Alert.alert('Error', data.message);
          }
        }
      }
    }
  };

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      // The screen is focused
      if (isConnected) {
        handleLoans();
        handleAccountBalances();
        handleFetchCard();
      }
    });

    // Return the function to unsubscribe from the event so it gets removed on unmount
    return unsubscribe;
  }, []);

  // Handler for checking perso or visa card status.
  const handleCardStatusChecker = () => {
    if (login.result.profile.persoCardStatus === STATUS_PENDING) {
      return 'Personalized card is being processed.';
    }

    if (login.result.profile.persoCardStatus === STATUS_FOR_ACTIVATION) {
      return 'Activate your Personalized Card';
    }

    if (login.result.profile.cardStatus === STATUS_ACTIVE) {
      return 'Upgrade to a personalized card';
    }

    if (login.result.profile.cardStatus === STATUS_PENDING) {
      return 'VISA card is being processed.';
    }

    if (login.result.profile.cardStatus === STATUS_FOR_ACTIVATION) {
      return 'Activate your VISA Card';
    }
  };

  const onRefresh = React.useCallback(() => {
    if (login.connected) {
      setRefreshing(true);
      handleLoans();
      handleAccountBalances();
      handleFetchCard();
    }
  }, []);

  useEffect(() => {
    if (
      isLoanLoading === false &&
      isBalanceLoading === false &&
      isLoading === false
    ) {
      setRefreshing(false);
    }
  }, [isLoanLoading, isBalanceLoading, isLoading]);

  useEffect(() => {
    setConnected(login.connected);
    if (login.connected) {
      onRefresh();
    }
  }, [login.connected]);

  return (
    <SafeAreaView style={[styles.safeview]}>
      <NetworkBanner connected={isConnected} />
      <ScrollView
        nestedScrollEnabled={true}
        style={globalStyles.scrollView}
        refreshControl={
          <RefreshControl
            refreshing={isConnected ? refreshing : false}
            onRefresh={isConnected ? onRefresh : null}
          />
        }>
        {isConnected ? (
          <View>
            <View style={globalStyles.headerWrapper}>
              <View style={styles.contentWrapper}>
                <Image
                  style={styles.logo}
                  // eslint-disable-next-line global-require
                  source={require('../../../../images/assets/psslai_logo.png')}
                />
              </View>
            </View>

            {/* PSSLAI Card Component. */}
            {isLoading && isConnected ? (
              <>
                <SkeletonPlaceholder backgroundColor="#e2e2e2">
                  <View style={styles.skeletonItem2} />
                </SkeletonPlaceholder>

                <SkeletonPlaceholder backgroundColor="#e2e2e2">
                  <View style={styles.skeletonItem4} />
                  <View style={styles.skeletonItem4} />
                </SkeletonPlaceholder>
              </>
            ) : (
              <>
                {!login.result.profile.accountNo ? (
                  <View style={styles.spacerTop40}>
                    <Text style={styles.getYoursNow}>Get yours now!</Text>
                  </View>
                ) : null}
                <PsslaiCard
                  isActive={isActiveCard}
                  data={details}
                  info={login.result.profile}
                />
                <View style={styles.spacerTop40}>
                  {isActiveCard ||
                  login.result.profile.persoCardStatus > 0 ||
                  login.result.profile.cardStatus > 0 ? (
                    <View style={styles.clearButtonContainer}>
                      <FormButton
                        icon={<InfoOutlineIcon size={styles.iconSize.height} />}
                        title="Lost/Replace card?"
                        type="clear"
                        buttonStyle={styles.clearButton}
                        titleStyle={styles.clearButtonTitle}
                        onPress={toggleReplace}
                      />
                      {login.result.profile.persoCardStatus ===
                        STATUS_PENDING ||
                      login.result.profile.cardStatus === STATUS_PENDING ? (
                        <Text style={styles.cardStatusText}>
                          {handleCardStatusChecker()}
                        </Text>
                      ) : login.result.profile.persoCardStatus ===
                          STATUS_FOR_ACTIVATION ||
                        login.result.profile.cardStatus ===
                          STATUS_FOR_ACTIVATION ||
                        login.result.profile.cardStatus === STATUS_ACTIVE ? (
                        <FormButton
                          icon={
                            <MembershipIcon size={styles.iconSize.height} />
                          }
                          title={handleCardStatusChecker()}
                          type="clear"
                          buttonStyle={styles.clearButton}
                          titleStyle={styles.clearButtonTitle}
                          onPress={
                            login.result.profile.cardStatus === STATUS_ACTIVE
                              ? login.result.profile.persoCardStatus ===
                                  STATUS_FOR_ACTIVATION ||
                                login.result.profile.cardStatus ===
                                  STATUS_FOR_ACTIVATION
                                ? toggleActivate
                                : toggleUpgrade
                              : toggleActivate
                          }
                        />
                      ) : null}
                    </View>
                  ) : (
                    <>
                      <PageView data={dummyData} />
                      <FormButton
                        title="Apply for a VISA Card"
                        type="outline"
                        // eslint-disable-next-line prettier/prettier
                        onPress={() =>
                          NavigateWeb(navigation, psslai_apply_visa_card)
                        }
                        buttonStyle={styles.applyVisa}
                        titleStyle={styles.applyVisaTitle}
                      />
                    </>
                  )}
                </View>
              </>
            )}

            {/* Active Balances */}
            {isBalanceLoading && isConnected ? (
              <>
                <SkeletonPlaceholder backgroundColor="#e2e2e2">
                  <View style={styles.skeletonItem3} />
                  <View style={styles.skeletonItem5} />
                </SkeletonPlaceholder>
              </>
            ) : (
              <>
                <Text style={styles.subHeader}>
                  {accountBalances.length > 0
                    ? `Account Balances (${accountBalances.length})`
                    : ''}
                </Text>
                <View style={[styles.spacerTop30]}>
                  <AccountsView data={accountBalances} />
                </View>
              </>
            )}
            {/* Active loans */}
            {isLoanLoading && isConnected ? (
              <>
                <SkeletonPlaceholder backgroundColor="#e2e2e2">
                  <View style={styles.skeletonItem3} />
                  <View style={styles.skeletonItem5} />
                </SkeletonPlaceholder>
              </>
            ) : (
              <>
                <Text style={styles.subHeader}>
                  {loans.length > 0 ? `Active Loans (${loans.length})` : ''}
                </Text>
                <View style={[styles.spacerTop30]}>
                  <ActiveLoansView data={loans} />
                </View>
              </>
            )}
            <View style={globalStyles.spacer} />
          </View>
        ) : (
          <NetworkScreen />
        )}
      </ScrollView>
      {/* Lost/Replace Card Modal */}
      <CustomAlert
        visible={isReplaceVisible}
        toggle={toggleReplace}
        styles={{ borderRadius: 5 }}>
        <CloseIcon
          size={29}
          onPress={toggleReplace}
          color={'black'}
          style={styles.modalClose}
        />
        <View style={styles.modalContent}>
          <View style={styles.modalSubContent}>
            <Text style={styles.modalReplaceTitle}>Lost/Replace Card?</Text>
            <Text style={styles.modalSubTitle}>Visit any PSSLAI Office</Text>
            <View style={styles.modalItemsContent}>
              <LocationIcon size={29} />
              <View style={styles.modalList}>
                <Text style={styles.modalItemList}>1. PSSLAI Head Office</Text>
                <Text style={styles.modalItemList}>2. Camp Crame Branch</Text>
                <Text style={styles.modalItemList}>3. Satellite Offices</Text>
              </View>
            </View>
          </View>
        </View>
      </CustomAlert>

      {/* Activate Card Modal */}
      <CustomAlert
        visible={isActivateVisible}
        toggle={toggleActivate}
        styles={{ borderRadius: 5 }}>
        <CloseIcon
          size={29}
          onPress={toggleActivate}
          color={'black'}
          style={styles.modalClose}
        />
        <View style={styles.modalContent}>
          <View style={styles.modalSubContent}>
            <Text style={styles.modalTitle}>Pickup and activate your card</Text>
            <Text style={styles.modalSubTitle}>
              Pickup your card from any PSSLAI Office
            </Text>
            <View style={styles.modalItemsContent}>
              <LocationIcon size={29} />
              <View style={styles.modalList}>
                <Text style={styles.modalItemList}>1. PSSLAI Head Office</Text>
                <Text style={styles.modalItemList}>2. Camp Crame Branch</Text>
                <Text style={styles.modalItemList}>3. Satellite Offices</Text>
              </View>
            </View>
            <Text style={[styles.modalSubTitle, { textAlign: 'center' }]}>
              Activate your card thru PSSLAI Bilis Online (
              <Text style={styles.modalLink} onPress={navigateBilisOnline}>
                https://bilisonline.psslai.com
              </Text>
              )
            </Text>
          </View>
        </View>
      </CustomAlert>

      {/* Upgrade VISA Card Modal */}
      <CustomAlert
        visible={isUpgradeVisible}
        toggle={toggleUpgrade}
        styles={{ borderRadius: 5 }}>
        <CloseIcon
          size={29}
          onPress={toggleUpgrade}
          color={'black'}
          style={styles.modalClose}
        />
        <View style={styles.modalContent}>
          <View style={styles.modalSubContent}>
            <Text style={styles.modalTitle}>Upgrade Visa Card</Text>
            <Text style={styles.modalDeductText}>
              Your upgraded card will be available in 7 to 10 business days. Php
              150 card fee will be deducted from your PSSLAI Account.
            </Text>
            <Text style={[styles.modalSubTitle, { textAlign: 'center' }]}>
              Upgrade your card thru PSSLAI Bilis Online (
              <Text style={styles.modalLink} onPress={navigateBilisOnline}>
                https://bilisonline.psslai.com
              </Text>
              )
            </Text>
          </View>
        </View>
      </CustomAlert>
    </SafeAreaView>
  );
}

export default DashboardTab;
