import React, { useState, useEffect, useRef } from 'react';
import {
  SafeAreaView,
  View,
  Image,
  TouchableOpacity,
  Text,
  Modal,
} from 'react-native';
import { ModalPicker } from '../../../../globals/modal/ModalPicker';
import globalStyles from '../../../../globals/styles';
import styles from './Styles';
import PageView from '../dashboard/Components/PageView';
import AccountsView from '../dashboard/Components/AccountBalanceView';
import ActiveLoansView from '../dashboard/Components/ActiveLoansView';
import { dummyData } from '../dashboard/Data/Data';
import { accountDummyData } from '../dashboard/Data/AccountsData';
import { activeLoansDummyData } from '../dashboard/Data/ActiveLoansData';
import { ScrollView } from 'react-native-gesture-handler';

function DashboardNoCard({ navigation }) {
  const [isModalVisible, setisModalVisible] = useState(false);
  const changeModalVisibility = bool => {
    setisModalVisible(bool);
  };
  return (
    <SafeAreaView style={styles.safeview}>
      <ScrollView>
        <View style={globalStyles.headerWrapper}>
          <View style={styles.contentWrapper}>
            <Image
              style={styles.logo}
              source={require('../../../../images/assets/psslai_logo.png')}
            />

            <TouchableOpacity style={styles.touchableAlign}>
              <Image
                style={[styles.notificationIcon]}
                source={require('../../../../images/assets/notification_icon.png')}
              />
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.spacerTop40}>
          <Text style={styles.getYoursNow}>Get yours now!</Text>
        </View>

        <View style={styles.spacerTop30}>
          <TouchableOpacity 
          style={globalStyles.cardView}
          onPress ={() => alert()}
          >
            <Text style={styles.cardTitle}>PSSLAI Visa Card</Text>
            <Text style={styles.cardNumber}>xxxx xxxx xxxx</Text>
          </TouchableOpacity>
        </View>
ß
        <Modal
          transparent={true}
          animationType="fade"
          visible={isModalVisible}
          nRequestClose={() => changeModalVisibility(false)}>
          <ModalPicker changeModalVisibility2={changeModalVisibility} />
        </Modal>

        <View style={[styles.spacerTop30]}>
          <PageView data={dummyData} />
        </View>

        <TouchableOpacity
          onPress={() => changeModalVisibility(true)}
          style={styles.applyForVisa}>
          <Text>Apply for Visa Card</Text>
        </TouchableOpacity>

        <Text style={styles.subHeader}>Account Balances</Text>

        <View style={[styles.spacerTop30]}>
          <AccountsView data={accountDummyData} />
        </View>

        <Text style={styles.subHeader}>Active Loans (3)</Text>

        <View style={[styles.spacerTop30]}>
          <ActiveLoansView data={activeLoansDummyData} />
        </View>

        <View style={styles.spacerTop40} />
      </ScrollView>
    </SafeAreaView>
  );
}

export default DashboardNoCard;
