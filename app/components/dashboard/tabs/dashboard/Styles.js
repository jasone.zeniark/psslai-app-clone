// eslint-disable-next-line prettier/prettier
import { StyleSheet, Dimensions, Platform } from 'react-native';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import COLORS from '../../../../globals/colors';
import colors from '../../../../globals/colors';

import {
  DARK_BLUE,
  GRAY,
  SEMI_LIGHT,
  WHITE,
  FONT,
  BLACK,
  BLUE,
} from '../../../../globals/Theme';

const { white, light, primary, semi_light } = colors;
const { width, height } = Dimensions.get('screen');
const os = Platform.OS === 'ios';

export default StyleSheet.create({
  spacerTop10: {
    marginTop: 10,
  },
  spacerTop30: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 30,
  },
  spacerTop40: {
    marginTop: 40,
  },
  contentWrapper: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    width: 'auto',
    paddingHorizontal: 15,
  },
  logo: {
    width: 90,
    height: 25,
    marginLeft: width * 0.02,
  },
  loader: {
    justifyContent: 'center',
    alignSelf: 'center',
  },
  hotlineView: {
    flexDirection: 'row',
    alignContent: 'center',
    height: 30,
    marginHorizontal: 35,
    marginTop: 20,
    alignItems: 'center',
    marginEnd: 25,
  },

  hotlineText: {
    alignItems: 'center',
    fontSize: 13,
    marginStart: 12,
  },

  headerLostReplace: {
    marginHorizontal: 30,
    fontSize: 20,
    fontWeight: '500',
  },
  touchableAlign: {
    alignSelf: 'flex-end',
  },
  notificationIcon: {
    width: 25,
    height: 25,
    marginStart: 235,
  },
  getYoursNow: {
    marginHorizontal: 20,
    fontSize: 30,
    fontFamily: 'Montserrat-Regular',
    color: primary,
  },
  cardTitle: {
    color: white,
    fontFamily: 'Montserrat-Bold',
    marginHorizontal: 25,
    marginTop: 30,
    fontSize: height * 0.025,
  },
  cardAccountNumberText: {
    color: white,
    fontFamily: 'Montserrat-SemiBold',
    marginHorizontal: 25,
    marginTop: 5,
    fontSize: height * 0.018,
  },
  cardNumber: {
    color: white,
    fontFamily: 'Montserrat-Bold',
    marginHorizontal: 25,
    marginTop: 5,
    fontSize: height * 0.019,
  },
  cardBalanceText: {
    color: white,
    fontFamily: 'Montserrat-SemiBold',
    marginHorizontal: 25,
    marginTop: height * 0.039,
    fontSize: height * 0.018,
  },
  cardBalanceWrapper: {
    flexDirection: 'row',
    marginStart: 25,
    marginTop: height * 0.007,
  },
  cardPesoSign: {
    fontSize: height * 0.039,
    fontFamily: 'Montserrat-Bold',
    color: white,
  },
  cardAmount: {
    marginStart: 5,
    fontSize: height * 0.039,
    fontFamily: 'Montserrat-Bold',
    color: white,
  },
  outlineBtn: {
    flexDirection: 'row',
    borderRadius: 25,
    borderWidth: 1,
    borderColor: semi_light,
    height: 50,
    width: width / 1.2,
    alignSelf: 'center',
    alignItems: 'center',
  },
  outlineBtnIcon: {
    height: 20,
    width: 20,
    marginStart: 18,
  },

  outlineBtnicon: {
    height: 20,
    width: 20,
  },

  outlineBtnText: {
    paddingStart: 5,
    color: primary,
    fontWeight: '300',
  },
  subHeader: {
    color: primary,
    fontWeight: '200',
    fontSize: 18,
    marginHorizontal: 25,
    marginTop: 30,
    fontWeight: 'bold',
  },
  accountBalanceWrapper: {
    width: width / 1.2,
    height: height / 5.2,
    marginHorizontal: 20,
    borderColor: semi_light,
    borderWidth: 1,
    borderRadius: 5,
    marginTop: 10,

    alignSelf: 'center',
  },
  accountBalanceItem: {
    flexDirection: 'row',
    marginHorizontal: 20,
    marginTop: 20,
  },
  itemPesoSign: {
    fontSize: 25,
    fontWeight: 'bold',
  },
  savingsNumber: {
    marginStart: 5,
  },
  itemAmount: {
    marginStart: 5,
    fontSize: 25,
    fontWeight: 'bold',
  },
  outstandingBalanceItem: {
    marginStart: 20,
    marginTop: 5,
  },
  outstandingBalanceText: {
    color: light,
  },
  applyVisa: {
    alignSelf: 'center',
    height: height * 0.055,
    borderWidth: 2,
    marginTop: height * 0.05,
    borderColor: DARK_BLUE,
  },
  applyVisaTitle: {
    ...FONT,
    color: DARK_BLUE,
    fontSize: height * 0.0185,
    paddingHorizontal: 12,
  },

  applyForVisa: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    width: width / 2,
    borderWidth: 1,
    borderRadius: 5,
    marginTop: 40,
    alignSelf: 'center',
  },

  lockCardOverlay: {
    position: 'absolute',
    alignSelf: 'center',
    alignItems: 'center',
    opacity: 0.23,
    height: '100%',
    width: '100%',
    borderRadius: 10,
    backgroundColor: BLACK,
  },

  lockCircle1: {
    position: 'absolute',
    alignSelf: 'center',
    top: '6%',
    alignItems: 'center',
    opacity: 0.05,
    borderRadius: 1000,
    height: height / 3.9,
    width: height / 3.9,
    backgroundColor: BLACK,
  },
  lockCircle2: {
    position: 'absolute',
    alignSelf: 'center',
    justifyContent: 'center',
    top: '14%',
    opacity: 0.08,
    borderRadius: 1000,
    height: height / 4.7,
    width: height / 4.7,
    backgroundColor: BLACK,
  },

  lockCircle3: {
    position: 'absolute',
    alignSelf: 'center',
    justifyContent: 'center',
    top: '22%',
    opacity: 0.09,
    borderRadius: 1000,
    height: height / 6,
    width: height / 6,
    backgroundColor: BLACK,
  },

  lockCircle4: {
    position: 'absolute',
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    top: '22%',
    borderRadius: 1000,
    height: height / 6,
    width: height / 6,
  },

  lockText: {
    ...FONT,
    color: WHITE,
    fontSize: height * 0.0135,
  },

  lockSize: {
    height: height * 0.035,
  },

  // page view style
  cardContainer: {
    flex: 1,
    flexDirection: 'column',
  },
  bgContainer: {
    flex: 1,
    justifyContent: 'center',
    resizeMode: 'cover',
  },
  dotView: {
    flexDirection: 'row',
    justifyContent: 'center',
  },

  image: {
    width: 20,
    height: 20,
    marginTop: 22,
  },

  atm: {
    height: height * 0.053,
  },

  cardViewContainer: {
    marginLeft: -width * 0.055,
    marginBottom: width * 0.05,
  },

  cardView: {
    width: width * 0.78,
    flexDirection: 'row',
    marginHorizontal: width * 0.109999,
    alignSelf: 'flex-start',
  },

  clearButtonContainer: {
    paddingHorizontal: 20,
  },

  clearButton: {
    borderRadius: 26,
    shadowColor: os ? GRAY : SEMI_LIGHT,
    backgroundColor: WHITE,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.15,
    shadowRadius: 15.19,
    elevation: os ? 2 : 0,
    minHeight: height * 0.05,
    justifyContent: 'center',
    paddingVertical: 10,
    marginVertical: height * 0.013,
    flexWrap: 'wrap',
  },

  clearButtonTitle: {
    ...FONT,
    color: DARK_BLUE,
    marginLeft: 10,
    marginRight: 'auto',
    textAlign: 'left',
    width: '87%',
    fontSize: height * 0.0165,
  },

  cardStatusText: {
    fontFamily: 'Montserrat-Bold',
    color: DARK_BLUE,
    fontSize: height * 0.0181,
    marginLeft: 5,
    marginVertical: height * 0.013,
    flexWrap: 'wrap',
  },

  accountView: {
    borderRadius: 10,
    elevation: os ? 20 : 2,
    backgroundColor: COLORS.white,
    height: 210,
    width: width - 32,
    marginHorizontal: 16,
    marginBottom: 10,
  },

  textView: {
    justifyContent: 'center',
    marginStart: 20,
  },

  carouselText: {
    fontSize: height * 0.02,
    fontFamily: 'Montserrat-Bold',
    color: DARK_BLUE,
  },

  safeview: {
    flex: 1,
  },

  viewAccount: {
    width: '100%',
    marginTop: 16,
    justifyContent: 'center',
  },

  textAccount: {
    marginHorizontal: 18,
    fontSize: 16,
    fontFamily: 'Montserrat-Bold',
    color: COLORS.primary,
  },

  viewOutstanding: {
    width: '100%',
    marginTop: 10,
  },

  textOutstanding: {
    marginStart: 16,
    fontSize: 32,
    fontFamily: 'Montserrat-Regular',
    color: COLORS.primary,
  },

  outstandingText: {
    marginStart: 20,
    fontFamily: 'Montserrat-Bold',
    color: COLORS.light,
    fontSize: 12,
    marginTop: 5,
  },

  viewWithdrawable: {
    width: '100%',
    marginTop: 10,
    marginTop: 16,
  },

  textWithdrawable: {
    marginHorizontal: 20,
    fontSize: 16,
    fontFamily: 'Montserrat-Bold',
    color: COLORS.primary,
  },

  withdrawableText: {
    marginStart: 20,
    fontFamily: 'Montserrat-Bold',
    color: COLORS.light,
    fontSize: 12,
    marginTop: 7,
  },

  activeLoansView: {
    borderRadius: 10,
    elevation: os ? 20 : 0,
    backgroundColor: COLORS.white,
    width: width - 32,
    marginHorizontal: 16,
  },

  viewActiveLoans: {
    flexDirection: 'row',
    alignItems: 'center',
    margin: 24,
  },

  imageActiveLoans: {
    width: 35,
    height: 35,
  },

  activeLoanText: {
    marginStart: 15,
    fontSize: 32,
    color: COLORS.primary,
    fontFamily: 'Montserrat-Regular',
  },

  separator: {
    height: 1,
    marginHorizontal: 25,
    backgroundColor: COLORS.light,
  },

  activeLoansHeaderView: {
    marginHorizontal: 24,
    marginTop: 24,
  },

  activeLoansSubView: {
    marginHorizontal: 24,
    marginTop: 8.86,
  },

  activeLoansHeaderText: {
    fontSize: 16,
    fontFamily: 'Montserrat-Bold',
  },
  activeLoansSubText: {
    fontSize: 20,
    fontFamily: 'Montserrat-Bold',
  },
  bottomMessage: {
    fontSize: 14,
    fontFamily: 'Montserrat-Regular',
    color: COLORS.light,
  },
  activeLoansMonthlyAmort: {
    marginBottom: 24,
    marginHorizontal: 24,
  },
  iconSize: {
    height: height * 0.023,
  },
  skeletonItem1: {
    marginVertical: 5,
    width: 150,
    height: 15,
    borderRadius: 4,
    marginLeft: width * 0.05,
  },
  skeletonItem2: {
    marginTop: 30,
    alignSelf: 'center',
    height: height / 3.5,
    width: width * 0.9,
    borderRadius: 4,
  },
  skeletonItem3: {
    marginVertical: 15,
    width: 150,
    height: 15,
    borderRadius: 4,
    marginLeft: width * 0.05,
  },
  skeletonItem4: {
    marginTop: 30,
    marginVertical: 5,
    marginLeft: width * 0.05,
    width: width * 0.9,
    height: height * 0.07,
    borderRadius: 4,
  },
  skeletonItem5: {
    marginVertical: 5,
    alignSelf: 'center',
    height: height / 3.5,
    width: width * 0.9,
    borderRadius: 4,
  },

  modalClose: {
    alignSelf: 'flex-end',
  },

  modalContent: {
    width: width * 0.8,
    justifyContent: 'center',
    paddingBottom: 30,
  },

  modalSubContent: {
    alignSelf: 'center',
    marginHorizontal: width * 0.061,
  },

  modalItemsContent: {
    flexDirection: 'row',
  },

  modalTitle: {
    alignSelf: 'center',
    textAlign: 'center',
    fontFamily: 'Montserrat-Bold',
    color: DARK_BLUE,
    fontSize: height * 0.027,
    paddingVertical: 8,
  },

  modalReplaceTitle: {
    alignSelf: 'center',
    fontFamily: 'Montserrat-Bold',
    color: DARK_BLUE,
    fontSize: height * 0.027,
    paddingVertical: 8,
  },

  modalSubTitle: {
    fontFamily: 'Montserrat-Bold',
    color: BLACK,
    fontSize: height * 0.0181,
    paddingVertical: 15,
    marginLeft: 2,
  },

  modalLink: {
    textDecorationLine: 'underline',
    color: BLUE,
  },

  modalList: {
    marginLeft: 7,
  },

  modalItemList: {
    fontFamily: 'Montserrat-Medium',
    color: BLACK,
    fontSize: height * 0.0172,
  },

  modalDeductText: {
    textAlign: 'center',
    alignSelf: 'center',
    fontFamily: 'Montserrat-Regular',
    marginTop: 10,
    marginBottom: 20,
    width: width * 0.7,
    color: BLACK,
    fontSize: height * 0.016,
  },
});
