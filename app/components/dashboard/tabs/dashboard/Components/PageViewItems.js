import React from 'react';
import { View, Image, Text, Dimensions } from 'react-native';

import { AtmIcon } from '../../../../../images/icons/UserIcons';
import Styles from '../Styles';

const { width, height } = Dimensions.get('screen');

const PageViewItems = ({ item }) => (
  <View style={Styles.cardView}>
    <AtmIcon size={Styles.atm.height} />
    <View style={[Styles.textView]}>
      <Text style={Styles.carouselText}>{item.description}</Text>
    </View>
  </View>
);

export default PageViewItems;
