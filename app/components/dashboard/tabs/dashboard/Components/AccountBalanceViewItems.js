import React from 'react';
import { View, Text, ImageBackground } from 'react-native';
import Styles from '../Styles';

const AccountBalanceViewItems = ({ item }) => {
  return (
    <View style={Styles.accountView}>
      <ImageBackground
        source={require('../../../../../images/assets/bg_container_white.png')}
        style={[Styles.bgContainer]}>
        <View style={[Styles.viewAccount]}>
          <Text
            numberOfLines={2}
            ellipsizeMode={'tail'}
            style={[Styles.textAccount]}>
            {item.name} • {item.accountNumber}
          </Text>
        </View>

        <View style={[Styles.viewOutstanding]}>
          <Text
            numberOfLines={1}
            ellipsizeMode={'tail'}
            style={[Styles.textOutstanding]}>
            ₱{item.currency} {item.outstandingBalance}
          </Text>
        </View>
        <Text style={[Styles.outstandingText]}>Outstanding Balance</Text>

        <View style={[Styles.viewWithdrawable]}>
          <Text style={[Styles.textWithdrawable]}>
            ₱{item.currency} {item.withdrawableBalance}
          </Text>
        </View>
        <Text style={[Styles.withdrawableText]}>Withdrawable Balance</Text>
      </ImageBackground>
    </View>
  );
};

export default AccountBalanceViewItems;
