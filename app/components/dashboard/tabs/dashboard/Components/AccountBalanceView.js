import React, { useState, useEffect } from 'react';
import {
  ImageBackground,
  View,
  Dimensions,
  FlatList,
  Text,
  Animated,
} from 'react-native';

import AccountBalanceViewItems from './AccountBalanceViewItems';
import Styles from '../Styles';
import COLORS from '../../../../../globals/colors';

const { width, height } = Dimensions.get('screen');

const AccountBalanceView = ({ data }) => {
  const scrollX = new Animated.Value(0);
  const position = Animated.divide(scrollX, width);
  const [dataList, setDataList] = useState(data);

  useEffect(() => {
    setDataList(data);
  });

  return (
    <>
      <FlatList
        data={data}
        ref={flatList => {
          this.flatList = flatList;
        }}
        keyExtractor={(item, index) => 'key' + index}
        horizontal
        pagingEnabled
        scrollEnabled
        snapToAlignment="center"
        scrollEventThrottle={30}
        decelerationRate={'normal'}
        showsHorizontalScrollIndicator={false}
        ListEmptyComponent={<View></View>}
        renderItem={({ item }) => {
          return <AccountBalanceViewItems item={item} />;
        }}
        onScroll={Animated.event(
          [{ nativeEvent: { contentOffset: { x: scrollX } } }],
          { useNativeDriver: false },
        )}
      />
      <View style={Styles.dotView}>
        {data.map((_, i) => {
          let opacity = position.interpolate({
            inputRange: [i - 1, i, i + 1],
            outputRange: [0.3, 1, 0.3],
            extrapolate: 'clamp',
          });
          return (
            <Animated.View
              key={i}
              style={[
                {
                  opacity,
                  height: 7,
                  width: 7,
                  backgroundColor: COLORS.primary,
                  margin: 8,
                  borderRadius: 5,
                },
              ]}
            />
          );
        })}
      </View>
    </>
  );
  // }
};

export default AccountBalanceView;
