import React, { useEffect, useState } from 'react';
import { View, Text, Image, ImageBackground } from 'react-native';
import COLORS from '../../../../../globals/colors';
import Styles from '../Styles';
import { loanImageData } from '../Data/LoansImageData';

const ActiveLoansViewItems = ({ item }) => {
  const [imageDir, setImageDir] = useState();
  const getLoanImage = item => {
    let obj = loanImageData.find(o => o.type === item.type);
    if (obj != null) {
      setImageDir(obj.image);
    } else {
      // Default Image Universal Loan
      setImageDir(loanImageData.find(o => o.type === 'AS').image);
    }
  };

  useEffect(() => {
    getLoanImage(item);
  }, []);
  return (
    <View style={Styles.activeLoansView}>
      <ImageBackground
        source={require('../../../../../images/assets/bg_container_white.png')}
        style={[Styles.bgContainer]}>
        <View style={[Styles.viewActiveLoans]}>
          <Image style={Styles.imageActiveLoans} source={imageDir} />
          <Text style={[Styles.activeLoanText]}>{item.name}</Text>
        </View>
        <View style={[Styles.separator]} />
        {/* Loan ID */}
        <View style={[Styles.activeLoansHeaderView]}>
          <Text style={[Styles.activeLoansHeaderText]}>LPRO Number</Text>
        </View>
        <View style={[Styles.activeLoansSubView]}>
          <Text style={[Styles.activeLoansSubText]}>{item.loanId}</Text>
        </View>
        {/* Unpaid Balances */}
        <View style={[Styles.activeLoansHeaderView]}>
          <Text style={[Styles.activeLoansHeaderText]}>Summary Due Amount*</Text>
        </View>
        <View style={[Styles.activeLoansSubView]}>
          <Text style={[Styles.activeLoansSubText]}>
            ₱{item.currency} {item.accumulatedUnpaidAmount}
          </Text>
        </View>
        {/* Outstanding Balance */}
        <View style={[Styles.activeLoansHeaderView]}>
          <Text style={[Styles.activeLoansHeaderText]}>
            Outstanding balance
          </Text>
        </View>

        <View style={[Styles.activeLoansSubView]}>
          <Text style={[Styles.activeLoansSubText]}>
            ₱{item.currency} {item.prin_out_balance}
          </Text>
        </View>
        {/* Monthly amortization */}
        <View style={[Styles.activeLoansHeaderView]}>
          <Text style={[Styles.activeLoansHeaderText]}>
            Monthly amortization
          </Text>
        </View>
        <View style={[Styles.activeLoansMonthlyAmort]}>
          <Text style={[Styles.activeLoansSubText]}>
            ₱{item.currency} {item.monthlyAmort}
          </Text>
        </View>
        <View style={[Styles.activeLoansMonthlyAmort]}>
          <Text style={[Styles.bottomMessage]}>
          * Due principal balance plus unpaid interest and penalties
          </Text>
        </View>
      </ImageBackground>
    </View>
  );
};

export default ActiveLoansViewItems;
