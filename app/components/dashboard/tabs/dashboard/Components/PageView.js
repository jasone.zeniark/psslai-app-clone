import React, { useState, useEffect } from 'react';
import { View, Dimensions, FlatList, Animated } from 'react-native';
import PageViewItems from './PageViewItems';
import Styles from '../Styles';
import COLORS from '../../../../../globals/colors';

const { width, height } = Dimensions.get('screen');

const PageView = ({ data }) => {
  const scrollX = new Animated.Value(0);
  let position = Animated.divide(scrollX, width);
  const [dataList, setDataList] = useState(data);

  useEffect(() => {
    setDataList(data);
    // infiniteScroll(dataList); => this is for auto scroll
  });

  if (data && data.length) {
    return (
      <View>
        <FlatList
          data={data}
          ref={flatList => {
            this.flatList = flatList;
          }}
          contentContainerStyle={Styles.cardViewContainer}
          keyExtractor={(item, index) => 'key' + index}
          horizontal
          pagingEnabled
          scrollEnabled
          snapToAlignment="center"
          scrollEventThrottle={30}
          decelerationRate={'normal'}
          showsHorizontalScrollIndicator={false}
          renderItem={({ item }) => {
            return <PageViewItems item={item} />;
          }}
          onScroll={Animated.event(
            [{ nativeEvent: { contentOffset: { x: scrollX } } }],
            { useNativeDriver: false },
          )}
        />

        <View style={Styles.dotView}>
          {data.map((_, i) => {
            let opacity = position.interpolate({
              inputRange: [i - 1, i, i + 1],
              outputRange: [0.3, 1, 0.3],
              extrapolate: 'clamp',
            });
            return (
              <Animated.View
                key={i}
                style={[
                  {
                    opacity,
                    height: 7,
                    width: 7,
                    backgroundColor: COLORS.primary,
                    margin: 8,
                    borderRadius: 5,
                  },
                ]}
              />
            );
          })}
        </View>
      </View>
    );
  }

  console.log('Please provide Images');
  return null;
};

/*function infiniteScroll(dataList) {
  const numberOfData = dataList.length;
  let scrollValue = 0,
    scrolled = 0;

  setInterval(function () {
    scrolled++;
    if (scrolled < numberOfData) scrollValue = scrollValue + width;
    else {
      scrollValue = 0;
      scrolled = 0;
    }

    this.flatList.scrollToOffset({animated: true, offset: scrollValue});
  }, 3000);
}*/

export default PageView;
