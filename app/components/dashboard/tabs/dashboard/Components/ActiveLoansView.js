import React, { useState, useEffect } from 'react';
import { View, Dimensions, FlatList, Animated } from 'react-native';
import ActiveLoansViewItems from './ActiveLoansViewItems';
import Styles from '../Styles';
import COLORS from '../../../../../globals/colors';

const { width, height } = Dimensions.get('screen');

const ActiveLoansView = ({ data }) => {
  const scrollX = new Animated.Value(0);
  let position = Animated.divide(scrollX, width);
  const [dataList, setDataList] = useState(data);

  useEffect(() => {
    setDataList(data);
  });

  return (
    <View>
      <FlatList
        data={data}
        ref={flatList => {
          this.flatList = flatList;
        }}
        keyExtractor={(item, index) => 'key' + index}
        horizontal
        pagingEnabled
        scrollEnabled
        ListEmptyComponent={<View></View>}
        snapToAlignment="center"
        scrollEventThrottle={30}
        decelerationRate={'normal'}
        showsHorizontalScrollIndicator={false}
        renderItem={({ item }) => {
          console.log('Loan item', item);
          return <ActiveLoansViewItems item={item} />;
        }}
        onScroll={Animated.event(
          [{ nativeEvent: { contentOffset: { x: scrollX } } }],
          { useNativeDriver: false },
        )}
      />

      <View style={Styles.dotView}>
        {data.map((_, i) => {
          let opacity = position.interpolate({
            inputRange: [i - 1, i, i + 1],
            outputRange: [0.3, 1, 0.3],
            extrapolate: 'clamp',
          });
          return (
            <Animated.View
              key={i}
              style={[
                {
                  opacity,
                  height: 7,
                  width: 7,
                  backgroundColor: COLORS.primary,
                  margin: 8,
                  borderRadius: 5,
                },
              ]}
            />
          );
        })}
      </View>
    </View>
  );
};

export default ActiveLoansView;
