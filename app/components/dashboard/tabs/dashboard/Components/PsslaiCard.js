import React, { useState } from 'react';
import { View, ImageBackground, Text } from 'react-native';

import { useNavigation } from '@react-navigation/native';

import { CardNumber, NumbersAndCommas } from '../../../../../globals/Helpers';
import { LockIcon } from '../../../../../images/icons/UserIcons';

import { BLACK } from '../../../../../globals/Theme';

import * as Constant from '../../../../../globals/utils/Constant';

import styles from '../Styles';
import globalStyles from '../../../../../globals/styles';
import { TouchableOpacity } from 'react-native-gesture-handler';
import routes from '../../../../../globals/navigations/Routes';

const PsslaiCard = ({ isActive, data, info }) => {
  const blueCard = '../../../../../images/assets/visa-card-blue-bg.png';
  const goldCard = '../../../../../images/assets/visa-card-gold-bg.png';
  const platinumCard = '../../../../../images/assets/visa-card-platinum-bg.png';

  const {
    STATUS_NO_LINKED_CARD,
    STATUS_ACTIVE,
    STATUS_PENDING,
    STATUS_FOR_ACTIVATION,
    STATUS_DEACTIVATED,
    STATUS_SUSPENDED,
    STATUS_PERSO_CARD_ACTIVATION_RETRY,
  } = Constant.cardStatus;

  const { CARD_TYPE_CODE_BLUE, CARD_TYPE_CODE_GOLD, CARD_TYPE_CODE_PLATINUM } =
    Constant.cardCode;

  const { cardStatus, persoCardStatus } = info;

  const noLinkedCard =
    persoCardStatus === STATUS_NO_LINKED_CARD &&
    cardStatus === STATUS_NO_LINKED_CARD;

  const suspendedCard =
    persoCardStatus === STATUS_SUSPENDED || cardStatus === STATUS_SUSPENDED;

  const deactivatedCard =
    persoCardStatus === STATUS_DEACTIVATED || cardStatus === STATUS_DEACTIVATED;

  const platinumFont =
    info.cardType === CARD_TYPE_CODE_PLATINUM
      ? {
          // eslint-disable-next-line indent
          color: BLACK,
        }
      : {};
  const navigation = useNavigation();
  const [showBalance, setShowBalance] = useState(
    suspendedCard ||
      deactivatedCard ||
      data.memberLocked === STATUS_ACTIVE ||
      data.adminLocked === STATUS_ACTIVE,
  );

  const handleLockStatusChecker = () => {
    if (deactivatedCard) {
      return 'Card is deactivated';
    }

    if (suspendedCard) {
      return 'Card is suspended';
    }

    if (data.adminLocked === STATUS_ACTIVE) {
      return 'Card is admin-locked';
    }

    if (data.memberLocked === STATUS_ACTIVE) {
      return 'Card is user-locked';
    }
  };

  return (
    <View style={styles.spacerTop30}>
      <TouchableOpacity
        touchSoundDisabled={true}
        activeOpacity={1}
        disabled={
          deactivatedCard ||
          suspendedCard ||
          data.adminLocked === STATUS_ACTIVE ||
          noLinkedCard
        }
        onPress={() => navigation.navigate(routes.CARD_SETTINGS)}>
        <ImageBackground
          source={
            typeof info.cardType !== 'undefined'
              ? info.cardType === CARD_TYPE_CODE_BLUE
                ? require(blueCard)
                : info.cardType === CARD_TYPE_CODE_GOLD
                ? require(goldCard)
                : info.cardType === CARD_TYPE_CODE_PLATINUM
                ? require(platinumCard)
                : require(blueCard)
              : require(blueCard)
          }
          style={globalStyles.cardView}
          imageStyle={{ borderRadius: 10 }}>
          <Text style={[styles.cardTitle, platinumFont]}>PSSLAI Visa Card</Text>
          {isActive ? (
            <>
              <Text style={[styles.cardAccountNumberText, platinumFont]}>
                Account Number
              </Text>
              <Text style={[styles.cardNumber, platinumFont]}>
                {CardNumber(data.accountNo)}
              </Text>
              {showBalance ? (
                <Text style={[styles.cardBalanceText, platinumFont]}></Text>
              ) : (
                <View>
                  <Text style={[styles.cardBalanceText, platinumFont]}>
                    Balance
                  </Text>
                  <View style={[styles.cardBalanceWrapper, platinumFont]}>
                    <Text style={[styles.cardPesoSign, platinumFont]}>₱</Text>
                    <Text style={[styles.cardAmount, platinumFont]}>
                      {NumbersAndCommas(data.balance)}
                    </Text>
                  </View>
                </View>
              )}
            </>
          ) : (
            <>
              <Text style={styles.cardNumber}>XXXX XXXX XXXX</Text>
            </>
          )}

          {(deactivatedCard ||
            suspendedCard ||
            data.memberLocked === STATUS_ACTIVE ||
            data.adminLocked === STATUS_ACTIVE) && (
            <>
              <View style={styles.lockCardOverlay} />
              <View style={styles.lockCircle1} />
              <View style={styles.lockCircle2} />
              <View style={styles.lockCircle3} />
              <View style={styles.lockCircle4}>
                <LockIcon size={styles.lockSize.height} />
                <Text style={styles.lockText}>{handleLockStatusChecker()}</Text>
              </View>
            </>
          )}
        </ImageBackground>
      </TouchableOpacity>
    </View>
  );
};

export default PsslaiCard;
