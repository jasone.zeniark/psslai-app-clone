import {StyleSheet, Dimensions} from 'react-native';
import Colors from '../../../globals/colors';

const {width} = Dimensions.get('screen');

const Styles = StyleSheet.create({
  scrollView: {
    height: '100%',
  },

  topMargin: {
    marginTop: 20,
  },

  viewMargin: {
    marginHorizontal: 20,
    marginTop: 40,
  },

  viewContainer: {
    flexDirection: 'row',
    marginHorizontal: 20,
  },

  logo: {
    width: 90,
    height: 25,
  },

  notifIcon: {
    alignSelf: 'flex-end',
  },

  icon: {
    width: 25,
    height: 25,
    marginStart: 235,
  },
  textHeader: {
    color: Colors.primary_hard,
    fontSize: 40,
    fontWeight: 'bold',
  },
});

export default Styles;
