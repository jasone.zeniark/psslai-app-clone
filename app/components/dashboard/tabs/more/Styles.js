import { StyleSheet, Dimensions, Platform } from 'react-native';
import colors from '../../../../globals/colors';

import { BLACK, DARK_BLUE, BLUE } from '../../../../globals/Theme';

const { primary } = colors;
const { width, height } = Dimensions.get('screen');

const isIOS = Platform.OS === 'ios';

export default StyleSheet.create({
  mainWrapper: {
    marginTop: 20,
  },
  contentWrapper: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    width: 'auto',
    paddingHorizontal: 15,
  },
  logo: {
    width: 90,
    height: 25,
    marginLeft: width * 0.02,
  },
  touchableAlign: {
    alignSelf: 'flex-end',
  },
  notificationIcon: {
    width: 25,
    height: 25,
    marginStart: 235,
  },
  btnWrapper: {
    flexDirection: 'row',
    marginTop: 30,
    marginHorizontal: 20,
  },

  logoutWrapper: {
    flexDirection: 'row',
    marginVertical: 30,
    marginHorizontal: 20,
  },
  btnIcon: {
    width: 30,
    height: 30,
  },
  btnText: {
    fontFamily: 'Montserrat-Medium',
    marginStart: 10,
    fontSize: 25,
    color: primary,
    paddingRight: 12,
  },
  subBtnContainer: {
    marginTop: 7,
    marginLeft: width * 0.2,
  },

  subBtnText: {
    fontFamily: 'Montserrat-Medium',
    marginTop: 15,
    fontSize: 18,
    color: primary,
  },

  accountSettingsCollapsible: {
    marginLeft: 'auto',
    justifyContent: 'center',
  },

  accountSettingsBtn: {
    flexDirection: 'row',
    width: '92%',
  },

  safeview: {
    flex: 1,
  },

  textView: {
    padding: 18,
    marginTop: 25,
    borderBottomColor: colors.light,
    borderBottomWidth: 0.5,
    borderTopColor: colors.light,
    borderTopWidth: 0.5,
  },

  name: {
    fontSize: 18,
    fontFamily: 'Montserrat-Bold',
  },
  mobile: {
    color: colors.light,
    fontFamily: 'Montserrat-Regular',
    marginTop: 5,
  },

  lastLogin: {
    marginTop: 15,
    width: '90%',
    fontFamily: 'Montserrat-Regular',
  },

  iconSize: {
    height: 29,
  },

  iconStyle: {
    marginTop: isIOS ? '0.5%' : '1%',
  },
  historyIconStyle: {
    marginLeft: -3,
  },
  mobileCarret: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  modalClose: {
    alignSelf: 'flex-end',
  },

  modalContent: {
    width: width * 0.8,
    justifyContent: 'center',
    paddingBottom: 30,
  },

  modalSubContent: {
    alignSelf: 'center',
    marginHorizontal: width * 0.061,
  },

  modalItemsContent: {
    flexDirection: 'row',
  },

  modalTitle: {
    alignSelf: 'center',
    textAlign: 'center',
    fontFamily: 'Montserrat-Bold',
    color: DARK_BLUE,
    fontSize: height * 0.027,
    paddingVertical: 8,
  },

  modalReplaceTitle: {
    alignSelf: 'center',
    fontFamily: 'Montserrat-Bold',
    color: DARK_BLUE,
    fontSize: height * 0.027,
    paddingVertical: 8,
  },

  modalSubTitle: {
    fontFamily: 'Montserrat-Bold',
    color: BLACK,
    fontSize: height * 0.0181,
    paddingVertical: 15,
    marginLeft: 2,
  },

  modalLink: {
    textDecorationLine: 'underline',
    color: BLUE,
  },

  modalList: {
    marginLeft: 7,
  },

  modalItemList: {
    fontFamily: 'Montserrat-Medium',
    color: BLACK,
    fontSize: height * 0.0172,
  },

  modalDeductText: {
    textAlign: 'center',
    alignSelf: 'center',
    fontFamily: 'Montserrat-Regular',
    marginTop: 10,
    marginBottom: 0,
    width: width * 0.7,
    color: BLACK,
    fontSize: height * 0.016,
  },

  submitButton: {
    paddingVertical: 15,
    marginTop: 20,
    marginHorizontal: 0,
    backgroundColor: '#201751',
    fontSize: 36,
    fontFamily: 'Montserrat-Regular',
    borderRadius: 4,
  },
  cancelButton: {
    paddingVertical: 13,
    backgroundColor: 'transparent',
    fontSize: 36,
    fontFamily: 'Montserrat-Regular',
    borderRadius: 4,
    borderWidth: 2,
    borderColor: '#D1D5DD',
  },
  submitText: {
    fontFamily: 'Montserrat-Bold',
  },
  cancelText: {
    fontFamily: 'Montserrat-Bold',
    color: '#000',
  },
  cancelBtnWrapper: {
    marginBottom: 20,
    marginTop: 10,
  },
  submitBtnDisabled: {
    backgroundColor: '#7d7898',
  },
  cancelBtnDisabled: {
    backgroundColor: 'transparent',
  },
  submitBtnTextDisabled: {
    color: '#fff',
    fontFamily: 'Montserrat-Bold',
  },
  formLabel: {
    fontSize: 14,
    fontWeight: '100',
    color: '#2B2D33',
    fontFamily: 'Montserrat-Regular',
  },
});
