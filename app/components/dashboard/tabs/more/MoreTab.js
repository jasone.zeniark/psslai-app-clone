/* eslint-disable no-alert */
import React, { useState, useEffect, useRef } from 'react';
import {
  Text,
  SafeAreaView,
  View,
  Image,
  ScrollView,
  TouchableOpacity,
  PanResponder,
  localStorage,
  Alert,
} from 'react-native';
import { useFocusEffect } from '@react-navigation/native';
import { useDispatch, useSelector } from 'react-redux';

import { cos } from 'react-native-reanimated';
import { Formik, Field } from 'formik';
import * as yup from 'yup';
import FormikInput from '../../../../globals/form/input/FormikInput';
import FormButton from '../../../../globals/form/button/FormButton';
import routes from '../../../../globals/navigations/Routes';
import globalStyles from '../../../../globals/styles';
import { NavigateWeb } from '../../../../globals/Helpers';

import config from '../../../../request/Config';
import styles from './Styles';
import COLORS from '../../../../globals/colors';
import { apiLogout, apiProfile, apiDeleteAccount } from '../../../../request/Auth';

import { logoutUser } from '../../../../store/actions/LoginActions';
import { decryptObject } from '../../../../request/RequestEncryption';
import * as Constant from '../../../../globals/utils/Constant';
import NetworkBanner from '../../../../globals/header/NetworkBanner';


import CustomAlert from '../../../../globals/alert/CustomAlert';

import {
  CardSettingsIcon,
  ChatIcon,
  LogoutIcon,
  AccountSettingsIcon,
  CarretDownIcon,
  CarretRightIcon,
  CloseIcon,
} from '../../../../images/icons/UserIcons';

const { psslai_contact_us } = config;

function MoreTab({ navigation }) {
  const login = useSelector(state => state.login);
  const dispatch = useDispatch();

  const [alertVisible, setAlertVisible] = useState(false);
  const [alertMessage, setAlertMessage] = useState('');
  const [isConnected, setConnected] = useState(true);
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [showAccountSettings, setShowAccountSettings] = useState(false);


  const [isDeleteVisible, setUpgradeVisible] = useState(false);
  const toggleDeleteAccount = () => {
    if (!isSubmitting) {
      setUpgradeVisible(!isDeleteVisible);
    }
  };

  const toggleAccountSettings = () =>
    setShowAccountSettings(!showAccountSettings);

  const {
    STATUS_NO_LINKED_CARD,
    STATUS_PENDING,
    STATUS_PERSO_CARD_ACTIVATION_RETRY,
    STATUS_FOR_ACTIVATION,
    STATUS_SUSPENDED,
    STATUS_ACTIVE,
  } = Constant.cardStatus;

  // eslint-disable-next-line object-curly-newline
  const {
    firstName,
    lastName,
    username,
    mobileNumber,
    persoCardStatus,
    cardStatus,
  } = login.result.profile;


  const handleLogout = () => {
    apiLogout(login.result.token)
      .then(response => {
        const { data } = response;
        console.log('logOUTdata', response);
        if (response.status === 200) {
          dispatch(logoutUser(data));
          // Alert.alert('', response.data.message);
        } else {
          if (typeof data.errors !== 'undefined') {
            let errMsg = '';
            for (let ctr = 0; ctr < data.errors.length; ctr += 1) {
              const errorObj = data.errors[ctr];
              errMsg += `${errorObj.msg}\r\n`;
            }
            alert(errMsg);
          }
        }
      })
      .catch(error => {
        dispatch(logoutUser({ message: error.response.message, status: 0 }));
        setAlertVisible(true);
        Alert.alert('', error.response.message);
      });
  };
  const collapsibleIcon = (value, handler) => (
    <>
      {value ? (
        <CarretDownIcon size={20} onPress={handler} />
      ) : (
        <CarretRightIcon size={20} onPress={handler} />
      )}
    </>
  );

  const navigateCardSettings = () => {
    let hasActiveCard = true;
    if (
      persoCardStatus === STATUS_PERSO_CARD_ACTIVATION_RETRY ||
      persoCardStatus === STATUS_FOR_ACTIVATION
    ) {
      if (cardStatus !== STATUS_ACTIVE && cardStatus !== STATUS_SUSPENDED) {
        hasActiveCard = false;
      }
    } else if (cardStatus === STATUS_FOR_ACTIVATION) {
      hasActiveCard = false;
    } else if (
      cardStatus === STATUS_PENDING &&
      persoCardStatus === STATUS_NO_LINKED_CARD
    ) {
      hasActiveCard = false;
    } else if (
      cardStatus === STATUS_NO_LINKED_CARD &&
      persoCardStatus === STATUS_PENDING
    ) {
      hasActiveCard = false;
    }

    if (hasActiveCard) {
      if (
        persoCardStatus === STATUS_NO_LINKED_CARD &&
        cardStatus === STATUS_NO_LINKED_CARD
      ) {
        Alert.alert(
          'No Linked Card',
          'Card Settings & History are not available',
        );
      } else {
        navigation.navigate(routes.CARD_SETTINGS);
      }
    } else {
      Alert.alert(
        'No Active Card',
        'Card Settings & History are not available',
      );
    }
  };

  /* Account deletion */
  const initialValues = {
    delete_field: '',
  };
  const deleteAccValidationSchema = yup.object().shape({
    delete_field: yup
      .string().lowercase()
      .required('This is required')

  });





  const handleFormSubmit = async (formData, { resetForm }) => {
    setIsSubmitting(true);
    // const apiParams = {
    //   delete_field: 'val here',
    // };

    if (isConnected) {
      // console.log('apiParams: ', apiParams);
      const apiResponse = await apiDeleteAccount(
        // apiParams,
        login.result.token,
      );
      console.log('apiResponse', apiResponse);
      if (apiResponse.status === 200) {
        const { data } = apiResponse;
        const decryptedMsg = decryptObject(data.message, false).replace(
          /["]+/g,
          '',
        );
        Alert.alert('Success', 'Successfully deleted');
        resetForm({});
        setUpgradeVisible(!isDeleteVisible);
        // dispatch(reloadCardDetails(login, navigation));
        // navigation.goBack();
        // handleLogout();
        setIsSubmitting(false);
      } else {
        // handle errors here
        const { data } = apiResponse;
        if (typeof data.errors !== 'undefined') {
          let errMsg = '';
          for (let ctr = 0; ctr < data.errors.length; ctr += 1) {
            const errorObj = data.errors[ctr];
            errMsg += `${errorObj.msg}\r\n`;
          }
          Alert.alert('Delete Account Error', errMsg);
          setIsSubmitting(false);
          setUpgradeVisible(!isDeleteVisible);
        } else {
          Alert.alert('Delete Account Error', data.message);
          setUpgradeVisible(!isDeleteVisible);
          // navigation.goBack();
          // handleLogout();
          setIsSubmitting(false);

        }
      }
    } else {
      Alert.alert(
        'Network Connection Error',
        'Please check your mobile network connection',
      );

    }
  };

  const handleCancel = () => {
    setUpgradeVisible(false);
  };

  /* END Account deletion */


  useFocusEffect(
    React.useCallback(() => {
      setShowAccountSettings(false);
    }, []),
  );




  useEffect(() => {
    setConnected(login.connected);
  }, [login.connected]);

  const handleNetworkLogout = () => {
    if (login.connected) {
      handleLogout();
    } else {
      dispatch(logoutUser({ message: 'Offline logout', status: 0 }));
      Alert.alert('', 'You have been successfully logged out');
    }
  };



  const handleAccountDeletion = () => {
    console.log('Handle Account Deletion');
  };


  return (
    <SafeAreaView>
      <NetworkBanner connected={isConnected} />
      <ScrollView style={globalStyles.scrollView}>
        <View style={globalStyles.headerWrapper}>
          <View style={styles.contentWrapper}>
            <Image
              style={styles.logo}
              // eslint-disable-next-line global-require
              source={require('../../../../images/assets/psslai_logo.png')}
            />

            {/* <TouchableOpacity style={styles.touchableAlign}>
              <Image
                style={{ width: 25, height: 25, marginStart: 235 }}
                source={require('../../../../images/assets/notification_icon.png')}
              />
            </TouchableOpacity> */}
          </View>
        </View>

        <View style={[styles.underline]} />

        {login.result.profile.mobileNumber !== 'undefined' ? (
          <View style={[styles.textView]}>
            <Text style={[styles.name]}>
              {firstName} {lastName}
            </Text>
            <Text style={styles.mobile}>
              {username} • +63******
              {login.result.profile !== 'undefined'
                ? (mobileNumber || '****').slice(
                  (mobileNumber || '****').length - 4,
                )
                : ''}
            </Text>
          </View>
        ) : (
          <></>
        )}

        <View style={[styles.underline]} />
        {/* <View style={styles.btnWrapper}>
          <Image
            style={styles.btnIcon}
            source={require('../../../../images/assets/Loans.png')}
          />
          <TouchableOpacity>
            <Text style={styles.btnText}>Apply for Loans</Text>
          </TouchableOpacity>
        </View> */}

        <View style={styles.btnWrapper}>
          {/* <Image
            style={styles.btnIcon}
            // eslint-disable-next-line global-require
            source={require('../../../../images/assets/card.png')}
          /> */}
          <CardSettingsIcon size={29} style={styles.iconStyle} />
          <TouchableOpacity onPress={navigateCardSettings}>
            <Text style={styles.btnText}>Card Settings & History</Text>
          </TouchableOpacity>
        </View>

        {/* <View style={styles.btnWrapper}>
          <Image
            style={styles.btnIcon}
            source={require('../../../../images/assets/setting.png')}
          />
          <TouchableOpacity>
            <Text style={styles.btnText}>Account Settings</Text>
          </TouchableOpacity>
        </View> */}


        <View style={styles.btnWrapper}>
          <AccountSettingsIcon size={29} style={styles.iconStyle} />
          <TouchableOpacity
            style={styles.accountSettingsBtn}
            onPress={toggleAccountSettings}>
            <Text style={styles.btnText}>Account Settings</Text>
            <View style={styles.accountSettingsCollapsible}>
              {collapsibleIcon(showAccountSettings, null)}
            </View>
          </TouchableOpacity>
        </View>
        {showAccountSettings && (
          <View style={styles.subBtnContainer}>
            <TouchableOpacity onPress={toggleDeleteAccount}>
              <Text style={styles.subBtnText}>Delete Account</Text>
            </TouchableOpacity>

          </View>
        )}


        <View style={styles.btnWrapper}>
          <ChatIcon size={29} style={styles.iconStyle} />
          <TouchableOpacity
            onPress={() => NavigateWeb(navigation, psslai_contact_us)}>
            <Text style={styles.btnText}>Contact Us</Text>
          </TouchableOpacity>
        </View>

        <View style={styles.logoutWrapper}>
          <LogoutIcon size={29} style={styles.iconStyle} />
          <TouchableOpacity
            onPress={() =>
              Alert.alert('Are you sure you want to log out?', '', [
                { text: 'YES', onPress: () => handleNetworkLogout() },
                { text: 'CANCEL', onPress: () => console.log('Press Cancel') },
              ])
            }>
            <Text style={styles.btnText}>Log Out</Text>
          </TouchableOpacity>
        </View>
        {/* {showAlertMessage()} */}


        {/* Delete Account Modal */}
        <CustomAlert
          visible={isDeleteVisible}
          toggle={toggleDeleteAccount}
          styles={{ borderRadius: 5 }}>
          <CloseIcon
            size={29}
            onPress={toggleDeleteAccount}
            color={'black'}
            style={styles.modalClose}
          />
          <View style={styles.modalContent}>
            <View style={styles.modalSubContent}>
              <Text style={styles.modalTitle}>Delete Account</Text>
              <Text style={styles.modalDeductText}>
                Are you sure you want to delete/suspend your account?
              </Text>
              <View style={styles.formWrapper}>
                <Formik
                  // validationSchema={deleteAccValidationSchema}
                  initialValues={initialValues}
                  onSubmit={handleFormSubmit}>
                  {({ handleSubmit, isValid, values }) => (
                    <>
                      <FormButton
                        title="Yes"
                        titleStyle={styles.submitText}
                        buttonStyle={styles.submitButton}
                        onPress={handleSubmit}
                        disabled={isSubmitting}
                        loading={isSubmitting}
                        disabledStyle={styles.submitBtnDisabled}
                        disabledTitleStyle={styles.submitBtnTextDisabled}
                      />
                      <View style={styles.cancelBtnWrapper}>
                        <FormButton
                          title={'Cancel'}
                          titleStyle={styles.cancelText}
                          buttonStyle={styles.cancelButton}
                          disabled={isSubmitting}
                          // onPress={Tester}
                          onPress={handleCancel}
                          disabledStyle={styles.cancelBtnDisabled}
                        />
                      </View>
                    </>
                  )}
                </Formik>
              </View>
            </View>
          </View>
        </CustomAlert>
      </ScrollView>
    </SafeAreaView>
  );
}

export default MoreTab;
