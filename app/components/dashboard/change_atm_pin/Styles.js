/* eslint-disable camelcase */
import { StyleSheet, Dimensions } from 'react-native';
import colors from '../../../globals/colors';

const { white, primary, semi_light, medium_gray, semi_gray, blue } = colors;
const { height, width } = Dimensions.get('screen');

export default StyleSheet.create({
  mainWrapper: {
    marginTop: 20,
  },
  contentWrapper: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    width: 'auto',
    paddingHorizontal: 15,
  },
  header: {
    opacity: 0,
  },
  logo: {
    width: 90,
    height: 25,
    marginLeft: width * 0.02,
  },
  touchableAlign: {
    alignSelf: 'flex-end',
  },
  notificationIcon: {
    width: 25,
    height: 25,
    marginStart: 235,
  },
  iconWrapper: {
    alignSelf: 'center',
    marginTop: 70,
  },
  textWrapper: {
    marginTop: 20,
    marginHorizontal: 40,
  },
  mainText: {
    fontSize: 30,
    fontWeight: '400',
    alignSelf: 'center',
    color: primary,
    textAlign: 'center',
  },
  mainBtn: {
    marginTop: 50,
    borderWidth: 1,
    borderRadius: 25,
    backgroundColor: primary,
    width: width / 1.2,
    height: 50,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  mainBtnText: {
    color: white,
    fontWeight: 'bold',
  },

  safeview: {
    flex: 1,
  },
  formWrapper: {
    // marginTop: 30,
    paddingHorizontal: 15,
  },
  radioText: {
    fontFamily: 'Montserrat-Regular',
    // fontWeight: '800',
  },
  radioBox: {
    width: 210,
    marginHorizontal: -5,
  },
  radioWrapper: {
    marginBottom: 17,
  },
  submitButton: {
    paddingVertical: 15,
    marginTop: 20,
    marginHorizontal: 0,
    backgroundColor: '#201751',
    fontSize: 36,
    fontFamily: 'Montserrat-Regular',
    borderRadius: 4,
  },
  cancelButton: {
    paddingVertical: 13,
    backgroundColor: 'transparent',
    fontSize: 36,
    fontFamily: 'Montserrat-Regular',
    borderRadius: 4,
    borderWidth: 2,
    borderColor: '#D1D5DD',
  },
  submitText: {
    fontFamily: 'Montserrat-Bold',
  },
  cancelText: {
    fontFamily: 'Montserrat-Bold',
    color: '#000',
  },
  cancelBtnWrapper: {
    marginBottom: 20,
    marginTop: 10,
  },
  submitBtnDisabled: {
    backgroundColor: '#7d7898',
  },
  cancelBtnDisabled: {
    backgroundColor: 'transparent',
  },
  submitBtnTextDisabled: {
    color: '#fff',
    fontFamily: 'Montserrat-Bold',
  },
  formLabel: {
    fontSize: 14,
    fontWeight: '100',
    color: '#2B2D33',
    fontFamily: 'Montserrat-Regular',
  },
  reminderHead: {
    fontSize: 14,
    color: '#2B2D33',
    fontFamily: 'Montserrat-Bold',
    marginBottom: 10,
  },
  reminderText: {
    fontSize: 12,
    color: '#2B2D33',
    fontFamily: 'Montserrat-Regular',
    marginBottom: 10,
  },
  skeletonItem1: {
    marginVertical: 5,
    width: 200,
    height: 20,
    borderRadius: 4,
  },
  skeletonItem2: {
    marginVertical: 5,
    width: '100%',
    height: 50,
    borderRadius: 4,
  },
  skeletonItem3: {
    marginVertical: 5,
    width: 150,
    height: 15,
    borderRadius: 4,
  },
  skeletonItem4: {
    marginVertical: 2.5,
    marginTop: 30,
    width: 150,
    height: 15,
    borderRadius: 4,
  },
  skeletonItem6: {
    marginTop: 30,
    marginVertical: 5,
    width: '100%',
    height: 50,
    borderRadius: 4,
  },
  skeletonItem5: {
    marginVertical: 2.5,
    width: 150,
    height: 15,
    borderRadius: 4,
  },

  titleText: {
    fontFamily: 'Montserrat-Bold',
    fontSize: 20,
    marginVertical: 20,
    textAlign: 'center',
  },
  subText: {
    fontFamily: 'Montserrat-Regular',
    // fontSize: 20,
    // marginVertical: 20,
    color: '#b7b5b5',
    textAlign: 'center',
    paddingHorizontal: 10,
  },
  showToggleWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 8,
    marginBottom: 15,
  },
  showToggleText: {
    fontFamily: 'Montserrat-Bold',
    color: medium_gray,
    fontSize: 16,
    marginLeft: 4,
  },
  iconToggle: {
    color: medium_gray,
    fontSize: 24,
  },
  wrapper: {
    paddingHorizontal: 10,
  },
  otpInput: {
    marginTop: '12%',
    alignItems: 'center',
    width: '80%',
    // color: 'red',
    height: height * 0.1,
  },
  underlineStyleBase: {
    width: height * 0.05,
    height: height * 0.075,
    borderRadius: 5,
    borderWidth: 1,
    fontSize: height * 0.034,
    color: medium_gray,
    // color: 'red',
    borderColor: semi_gray,
    backgroundColor: semi_gray,
  },
  underlineStyleHighLighted: {
    borderWidth: 2,
    backgroundColor: white,
    borderColor: blue,
    // color: 'red',
  },
  otpInputWrapper: {
    alignItems: 'center',
  },
});
