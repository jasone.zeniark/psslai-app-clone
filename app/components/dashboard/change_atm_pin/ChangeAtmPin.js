/* eslint-disable prettier/prettier */
/* eslint-disable object-curly-newline */
/* eslint-disable arrow-parens */
import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  SafeAreaView,
  Text,
  View,
  ScrollView,
  Alert,
  Platform,
  TouchableOpacity,
} from 'react-native';
import { useFocusEffect } from '@react-navigation/native';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import { Formik } from 'formik';
import * as yup from 'yup';

import { apiChangeAtmPin } from '../../../request/CardApi';
import { reloadCardDetails } from '../../../store/actions/CardActions';
import {
  logoutUser,
  loadUserProfile,
} from '../../../store/actions/LoginActions';
import Routes from '../../../globals/navigations/Routes';
import FormButton from '../../../globals/form/button/FormButton';
import Header from '../../../globals/header/CommonHeader';
import { cardErrorMessage } from '../../../globals/utils/Constant';
import globalStyles from '../../../globals/styles';
import { EyeIcon, EyeOffIcon } from '../../../images/icons/UserIcons';
import styles from './Styles';

const ChangeAtmPin = ({ navigation }) => {
  const dispatch = useDispatch();
  const login = useSelector(state => state.login);
  // console.log(login);
  const { token } = login.result;
  const { guid } = login.result;
  const [isCancelling, setIsCancelling] = useState(false);
  const [isSubmitting, setIsSubmitting] = useState(false);
  // const [currentPin, setCurrentPin] = useState('');
  const [showPin, setShowPin] = useState(false);

  const [isConnected, setConnected] = useState(true);

  const initialValues = {
    is_current_pin_set: false,
    current_pin: '',
    new_pin: '',
  };
  const signUpValidationSchema = yup.object().shape({
    current_pin: yup
      .string()
      .required('Current PIN is required')
      .min(6, () => 'Should be 6 digits')
      .max(6, () => 'Should be 6 digits'),
    new_pin: yup.string().when('is_current_pin_set', {
      is: true, // PSSLAI VISA Card/EON
      then: yup
        .string()
        .required('New PIN is required')
        .min(6, () => 'Should be 6 digits')
        .max(6, () => 'Should be 6 digits'),
    }),
    // new_pin: yup.string().required('New PIN is required'),
  });
  const handleCancel = () => {
    navigation.goBack();
    setIsCancelling(true);
  };

  const handleFormSubmit = async (formData, { resetForm }) => {
    setIsSubmitting(true);
    const apiParams = {
      current_pin: formData.current_pin,
      new_pin: formData.new_pin,
    };
    if (isConnected) {
      const apiResponse = await apiChangeAtmPin(apiParams, token, guid);
      console.log('apiResponse', apiResponse);
      if (apiResponse.status === 200) {
        const { data } = apiResponse;
        Alert.alert('Success', data.message);
        resetForm({});
        setIsSubmitting(false);
        dispatch(reloadCardDetails(login, navigation));
        navigation.goBack();
      } else {
        // handle errors here
        const { data } = apiResponse;
        if (typeof data.errors !== 'undefined') {
          let errMsg = '';
          for (let ctr = 0; ctr < data.errors.length; ctr += 1) {
            const errorObj = data.errors[ctr];
            errMsg += `${errorObj.msg}\r\n`;
          }
          Alert.alert('Set New ATM PIN Error', errMsg);
          setIsSubmitting(false);
          resetForm({});
        } else {
          dispatch(reloadCardDetails(login, navigation));
          setIsSubmitting(false);
          resetForm({});
          if (data.message === 'Visa Card is suspended') {
            dispatch(loadUserProfile(login, navigation));
            Alert.alert('Card Suspended', cardErrorMessage.CARD_SUSPENDED);
            navigation.goBack();
          } else {
            Alert.alert('Set New ATM PIN Error', data.message);
          }
          if (apiResponse.status === 401) {
            dispatch(logoutUser({ message: data.message, status: 0 }));
            navigation.navigate(Routes.LOGIN);
          }
        }
      }
    } else {
      Alert.alert(
        'Network Connection Error',
        'Please check your mobile network connection',
      );

      setIsSubmitting(false);
    }
  };

  const handleShowPinToggle = () => {
    if (showPin) {
      setShowPin(false);
    } else {
      setShowPin(true);
    }
  };

  useFocusEffect(
    React.useCallback(() => {
      setIsCancelling(false);
    }, []),
  );

  useEffect(() => {
    setConnected(login.connected);
  }, [login.connected]);

  return (
    <SafeAreaView>
      <Header
        title="Back"
        onBackPress={() => (!isSubmitting
          ? navigation.goBack()
          : Alert.alert(
            'Sorry',
            'You can not go back while the system is currently processing your request.',
          ))
        }
      />
      <ScrollView style={globalStyles.scrollView}>
        <View style={styles.wrapper}>
          <View style={styles.formWrapper}>
            <Formik
              validationSchema={signUpValidationSchema}
              initialValues={initialValues}
              onSubmit={handleFormSubmit}>
              {({ handleSubmit, isValid, values, setFieldValue }) => (
                <>
                  <Text style={styles.titleText}>
                    {!values.is_current_pin_set
                      ? 'Current ATM PIN'
                      : ' New ATM PIN'}
                  </Text>
                  <Text style={styles.subText}>
                    {!values.is_current_pin_set
                      ? 'Change your ATM\'s PIN directly from here. Please enter your current ATM\'s PIN'
                      : 'Change your ATM\'s PIN directly from here. Please enter your new ATM\'s PIN'}

                  </Text>
                  <View style={styles.otpInputWrapper}>
                    {!values.is_current_pin_set ? (
                      <OTPInputView
                        style={styles.otpInput}
                        pinCount={6}
                        code={values.current_pin}
                        onCodeChanged={code => setFieldValue('current_pin', code)
                        }
                        autoFocusOnLoad={Platform.OS === 'ios'}
                        codeInputFieldStyle={styles.underlineStyleBase}
                        codeInputHighlightStyle={
                          styles.underlineStyleHighLighted
                        }
                        placeholderCharacter="-"
                        secureTextEntry={!showPin}
                        editable={!isSubmitting}
                      // onCodeFilled={code => setFieldValue('current_pin', code)}
                      />
                    ) : (
                      <OTPInputView
                        style={styles.otpInput}
                        pinCount={6}
                        code={values.new_pin}
                        onCodeChanged={code => setFieldValue('new_pin', code)}
                        autoFocusOnLoad={Platform.OS === 'ios'}
                        codeInputFieldStyle={styles.underlineStyleBase}
                        codeInputHighlightStyle={
                          styles.underlineStyleHighLighted
                        }
                        placeholderCharacter="-"
                        secureTextEntry={!showPin}
                        editable={!isSubmitting}
                      // onCodeFilled={code => setFieldValue('new_pin', code)}
                      />
                    )}

                    <TouchableOpacity
                      style={styles.showToggleWrapper}
                      onPress={handleShowPinToggle}>
                      {showPin ? (
                        <EyeOffIcon size={24} style={styles.iconToggle} />
                      ) : (
                        <EyeIcon size={24} style={styles.iconToggle} />
                      )}
                      <Text style={styles.showToggleText}>
                        {showPin ? 'Hide PIN' : 'Show PIN'}
                      </Text>
                    </TouchableOpacity>
                  </View>

                  {!values.is_current_pin_set ? (
                    <FormButton
                      title="Next"
                      titleStyle={styles.submitText}
                      buttonStyle={styles.submitButton}
                      onPress={() => setFieldValue('is_current_pin_set', true)}
                      disabled={
                        !isValid || isSubmitting || values.current_pin === ''
                      }
                      loading={isSubmitting}
                      disabledStyle={styles.submitBtnDisabled}
                      disabledTitleStyle={styles.submitBtnTextDisabled}
                    />
                  ) : (
                    <FormButton
                      title="Set New ATM PIN"
                      titleStyle={styles.submitText}
                      buttonStyle={styles.submitButton}
                      onPress={handleSubmit}
                      disabled={
                        !isValid
                        || isSubmitting
                        || values.current_pin === ''
                        || values.new_pin === ''
                      }
                      loading={isSubmitting}
                      disabledStyle={styles.submitBtnDisabled}
                      disabledTitleStyle={styles.submitBtnTextDisabled}
                    />
                  )}

                  <View style={styles.cancelBtnWrapper}>
                    <FormButton
                      title={'Cancel'}
                      titleStyle={styles.cancelText}
                      buttonStyle={styles.cancelButton}
                      disabled={isSubmitting || isCancelling}
                      // onPress={Tester}
                      onPress={handleCancel}
                      disabledStyle={styles.cancelBtnDisabled}
                    />
                  </View>
                </>
              )}
            </Formik>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default ChangeAtmPin;
