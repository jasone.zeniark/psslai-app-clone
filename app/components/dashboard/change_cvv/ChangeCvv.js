/* eslint-disable object-curly-newline */
/* eslint-disable arrow-parens */
import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { SafeAreaView, Text, View, ScrollView, Alert } from 'react-native';
import { useFocusEffect } from '@react-navigation/native';
import { Formik, Field } from 'formik';
import * as yup from 'yup';

import { decryptObject } from '../../../request/RequestEncryption';
import { apiGenerateNewCvv } from '../../../request/CardApi';
import { reloadCardDetails } from '../../../store/actions/CardActions';
// import Routes from '../../../globals/navigations/Routes';
import FormButton from '../../../globals/form/button/FormButton';
import Header from '../../../globals/header/CommonHeader';
import FormikInput from '../../../globals/form/input/FormikInput';
import globalStyles from '../../../globals/styles';
import styles from './Styles';

const ChangeCvv = ({ navigation }) => {
  const dispatch = useDispatch();
  const login = useSelector(state => state.login);
  const { token } = login.result;
  const [isCancelling, setIsCancelling] = useState(false);
  const [isSubmitting, setIsSubmitting] = useState(false);

  const [isConnected, setConnected] = useState(true);

  const initialValues = {
    visa_card_no: '',
  };
  const signUpValidationSchema = yup.object().shape({
    visa_card_no: yup
      .string()
      .required('Card Number is required')
      .min(16, () => 'Should be 16 digits only')
      .max(16, () => 'Should be 16 digits only'),
  });
  const handleCancel = () => {
    navigation.goBack();
    setIsCancelling(true);
  };

  const handleFormSubmit = async (formData, { resetForm }) => {
    setIsSubmitting(true);
    const apiParams = {
      visa_card_no: formData.visa_card_no.replace(/[^\w/s/g]/gi, ''),
    };

    if (isConnected) {
      // console.log('apiParams: ', apiParams);
      const apiResponse = await apiGenerateNewCvv(apiParams, token);
      // console.log('apiResponse', apiResponse);
      if (apiResponse.status === 200) {
        const { data } = apiResponse;
        const decryptedMsg = decryptObject(data.message, false).replace(
          /["]+/g,
          '',
        );
        Alert.alert('Success', decryptedMsg);
        resetForm({});
        setIsSubmitting(false);
        dispatch(reloadCardDetails(login, navigation));
        navigation.goBack();
      } else {
        // handle errors here
        const { data } = apiResponse;
        if (typeof data.errors !== 'undefined') {
          let errMsg = '';
          for (let ctr = 0; ctr < data.errors.length; ctr += 1) {
            const errorObj = data.errors[ctr];
            errMsg += `${errorObj.msg}\r\n`;
          }
          Alert.alert('Generate CVV Error', errMsg);
          setIsSubmitting(false);
        } else {
          setIsSubmitting(false);
          Alert.alert('Generate CVV Error', data.message);
          navigation.goBack();
        }
      }
    } else {
      Alert.alert(
        'Network Connection Error',
        'Please check your mobile network connection',
      );

      setIsSubmitting(false);
    }
  };

  useFocusEffect(
    React.useCallback(() => {
      setIsCancelling(false);
    }, []),
  );

  useEffect(() => {
    setConnected(login.connected);
  }, [login.connected]);

  return (
    <SafeAreaView>
      <Header title="Back" onBackPress={() => navigation.goBack()} />
      <ScrollView style={globalStyles.scrollView}>
        <View style={styles.wrapper}>
          <Text style={styles.titleText}>Change Card's CVV</Text>
          <Text style={styles.subText}>
            Change your card's CVV directly from here. This will override the
            CVV printed on your card.
          </Text>
          <View style={styles.formWrapper}>
            <Formik
              validationSchema={signUpValidationSchema}
              initialValues={initialValues}
              onSubmit={handleFormSubmit}>
              {({ handleSubmit, isValid, values }) => (
                <>
                  <Field
                    component={FormikInput}
                    name="visa_card_no"
                    placeholderText="Enter Card Number"
                    labelText="Card Number"
                    // maxLength={16}
                    keyboardType={'numeric'}
                    editable={!isSubmitting}
                  />
                  <FormButton
                    title="Generate New CVV"
                    titleStyle={styles.submitText}
                    buttonStyle={styles.submitButton}
                    onPress={handleSubmit}
                    disabled={
                      !isValid || isSubmitting || values.visa_card_no === ''
                    }
                    loading={isSubmitting}
                    disabledStyle={styles.submitBtnDisabled}
                    disabledTitleStyle={styles.submitBtnTextDisabled}
                  />
                  <View style={styles.cancelBtnWrapper}>
                    <FormButton
                      title={'Cancel'}
                      titleStyle={styles.cancelText}
                      buttonStyle={styles.cancelButton}
                      disabled={isSubmitting || isCancelling}
                      // onPress={Tester}
                      onPress={handleCancel}
                      disabledStyle={styles.cancelBtnDisabled}
                    />
                  </View>
                </>
              )}
            </Formik>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default ChangeCvv;
