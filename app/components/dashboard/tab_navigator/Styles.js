import { StyleSheet, Dimensions } from 'react-native';

import {
  DARK_BLUE,
  GRAY,
  SEMI_LIGHT,
  WHITE,
  FONT,
  BLACK,
  BLUE,
} from '../../../globals/Theme';

const { width, height } = Dimensions.get('screen');

export default StyleSheet.create({
  safeview: {
    flex: 1,
  },
  modalClose: {
    alignSelf: 'flex-end',
  },
  modalContent: {
    width: width * 0.8,
    justifyContent: 'center',
    paddingBottom: 30,
  },
  modalTitle: {
    alignSelf: 'center',
    textAlign: 'center',
    fontFamily: 'Montserrat-Bold',
    color: BLACK,
    fontSize: height * 0.025,
    paddingVertical: 8,
  },
  modalSubTitle: {
    fontFamily: 'Montserrat-Medium',
    textAlign: 'center',
    color: BLACK,
    fontSize: height * 0.0181,
    paddingVertical: 3,
    marginLeft: 2,
  },
  modalList: {
    marginTop: 15,
    alignItems: 'center',
    justifyContent: 'center',
  },
  modalDeductText: {
    textAlign: 'center',
    alignSelf: 'center',
    fontFamily: 'Montserrat-Regular',
    marginTop: 10,
    marginBottom: 20,
    width: width * 0.7,
    color: BLACK,
    fontSize: height * 0.0185,
  },
  buttonGroup: {
    marginTop: 20,
    alignSelf: 'center',
    width: '85%',
  },
  logoutButton: {
    height: height * 0.055,
    // borderRadius: 36,
    borderWidth: 2,
    marginBottom: 10,
    borderColor: GRAY,
  },
  logoutTitle: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#000',
    fontSize: height * 0.0185,
    fontWeight: '600',
  },
  hereButton: {
    height: height * 0.055,
    backgroundColor: DARK_BLUE,
    // borderRadius: 36,
  },
  hereTitle: {
    ...FONT,
    color: WHITE,
    fontWeight: '600',
    fontSize: height * 0.019,
  },
});
