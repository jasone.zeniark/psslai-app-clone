import React, { useState, useEffect, useRef } from 'react';
import { View } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import BuyLoad from '../tabs/buy_load/BuyLoadTab';
import Dashboard from '../tabs/dashboard/DashboardTab';
import More from '../tabs/more/MoreTab';
import PayBills from '../tabs/pay_bills/PayBillsTab';
import TransferFundsNoCard from '../tabs/transfer_funds/TransferFundsNoCard';
import TransferFunds from '../tabs/transfer_funds/TransferFunds';
import DashboardNoCard from '../tabs/dashboard/DashboardNoCard';

import routes from '../../../globals/navigations/Routes';
import styles from './Styles';

import {
  BuyLoadActiveTabIcon,
  BuyLoadTabIcon,
  HomeActiveTabIcon,
  HomeTabIcon,
  MoreActiveTabIcon,
  MoreTabIcon,
  PayBillsActiveTabIcon,
  PayBillsTabIcon,
  TransferFundsActiveTabIcon,
  TransferFundsTabIcon,
  CloseIcon,
} from '../../../images/icons/UserIcons';

const Tabs = ({ navigation }) => {
  const { Navigator, Screen } = createBottomTabNavigator();

  const {
    DASHBOARD,
    SUB_DASHBOARD,
    TRANSFER_FUNDS,
    PAY_BILLS,
    BUY_LOAD,
    CARD_SETTINGS,
    CHANGE_ATM_PIN,
    CHANGE_CVV,
    MORE,
  } = routes;

  const bottomTabs = () => (
    <Navigator
      initialRouteName={DASHBOARD}
      tabBarOptions={{
        activeTintColor: '#201751',
        labelStyle: { fontSize: 9, fontWeight: 'bold' },
      }}>
      <Screen
        name={DASHBOARD}
        component={Dashboard}
        options={{
          tabBarIcon: ({ focused }) =>
            focused ? <HomeActiveTabIcon /> : <HomeTabIcon />,
        }}
      />
      <Screen
        name={TRANSFER_FUNDS}
        component={TransferFunds}
        options={{
          tabBarIcon: ({ focused }) =>
            focused ? <TransferFundsActiveTabIcon /> : <TransferFundsTabIcon />,
        }}
      />
      <Screen
        name={MORE}
        component={More}
        options={{
          tabBarIcon: ({ focused }) =>
            focused ? <MoreActiveTabIcon /> : <MoreTabIcon />,
        }}
      />
    </Navigator>
  );

  return <View style={styles.safeview}>{bottomTabs()}</View>;
};

export default Tabs;
