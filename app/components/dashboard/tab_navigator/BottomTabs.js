import React, { useState, useEffect, useRef } from 'react';
import {
  LogBox,
  View,
  PanResponder,
  Text,
  Alert,
  SafeAreaView,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { createStackNavigator } from '@react-navigation/stack';
import BackgroundTimer from 'react-native-background-timer';

import { apiGetNotifications } from '../../../request/NotificationApi';
import { decryptObject } from '../../../request/RequestEncryption';

import { logoutUser } from '../../../store/actions/LoginActions';

import CardSettings from '../card_settings/CardSettings';
import ChangeCvv from '../change_cvv/ChangeCvv';
import ChangeAtmPin from '../change_atm_pin/ChangeAtmPin';

import FormButton from '../../../globals/form/button/FormButton';
import routes from '../../../globals/navigations/Routes';
import CustomAlert from '../../../globals/alert/CustomAlert';
import Tabs from './Tabs';
import styles from './Styles';

import { CloseIcon } from '../../../images/icons/UserIcons';

const BottomTabs = ({ navigation }) => {
  const DashboardStack = createStackNavigator();

  const login = useSelector(state => state.login);
  const dispatch = useDispatch();

  const { SUB_DASHBOARD, CARD_SETTINGS, CHANGE_ATM_PIN, CHANGE_CVV } = routes;

  const timerId = useRef(false);
  const interval = useRef(false);
  const notificationInterval = useRef(false);
  const [timeForInactivityInSecond, setTimeForInactivityInSecond] =
    useState(3600);
  const [isVisible, setIdleVisible] = useState(false);
  const [maxTime, setMaxTime] = useState(30);

  const toggleIdle = () => {
    setIdleVisible(!isVisible);

    if (isVisible) {
      setMaxTime(30);

      return BackgroundTimer.clearInterval(interval.current);
    }
  };

  // Handler for logging out user.
  const handleLogout = () => {
    setIdleVisible(false);
    dispatch(logoutUser({ message: 'Idle for 30 seconds', status: 0 }));
  };

  // Handler for countdown timer (30 secs).
  const handleCountdownTimer = () => {
    interval.current = BackgroundTimer.setInterval(() => {
      setMaxTime(timer => {
        if (timer === 1) {
          closeInterval();
        }
        return timer - 1;
      });
    }, 1000);

    const closeInterval = () => {
      setMaxTime(30);
      handleLogout();

      return BackgroundTimer.clearInterval(interval.current);
    };
  };

  const resetInactivityTimeout = () => {
    BackgroundTimer.clearTimeout(timerId.current);
    timerId.current = BackgroundTimer.setTimeout(() => {
      setIdleVisible(true);
      handleCountdownTimer();
    }, 300000);
  };

  useEffect(() => {
    resetInactivityTimeout();

    // Run in dev mode
    LogBox.ignoreLogs(['Setting a timer for a long period of time']);
  }, []);

  const panResponder = useRef(
    PanResponder.create({
      onStartShouldSetPanResponderCapture: () => {
        // console.log('touched');
        resetInactivityTimeout();
      },
    }),
  ).current;

  // Handler for getting all notifications.
  const handleFetchNotifications = async () => {
    const apiResponse = await apiGetNotifications(
      login.result.token,
      login.result.guid,
    );

    console.log('Notifications response:', apiResponse);

    if (apiResponse.status === 200) {
      const { data } = apiResponse;
      const result = decryptObject(data.results);
      // console.log('Notifications result:', result);
    } else {
      // handle errors here
      const { data } = apiResponse;
      if (typeof data.errors !== 'undefined') {
        let errMsg = '';
        for (let ctr = 0; ctr < data.errors.length; ctr += 1) {
          const errorObj = data.errors[ctr];
          errMsg += `${errorObj.msg}\r\n`;
        }
        Alert.alert('Error', errMsg);
      } else if (apiResponse.status === 401) {
        // Alert.alert('Authentication Error', data.message);
        handleLogout();
      } else {
        Alert.alert('Error', data.message);
      }
    }
  };

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      // The screen is focused
      notificationInterval.current = BackgroundTimer.setInterval(() => {
        handleFetchNotifications();
      }, 5000);
    });

    // Return the function to unsubscribe from the event so it gets removed on unmount
    return unsubscribe;
  }, []);

  useEffect(() => {
    navigation.addListener('blur', () =>
      BackgroundTimer.clearInterval(notificationInterval.current),
    );
  }, []);

  return (
    <SafeAreaView style={styles.safeview} {...panResponder.panHandlers}>
      <DashboardStack.Navigator headerMode={'none'}>
        <DashboardStack.Screen name={SUB_DASHBOARD} component={Tabs} />
        <DashboardStack.Screen name={CARD_SETTINGS} component={CardSettings} />
        <DashboardStack.Screen name={CHANGE_CVV} component={ChangeCvv} />
        <DashboardStack.Screen name={CHANGE_ATM_PIN} component={ChangeAtmPin} />
      </DashboardStack.Navigator>
      <CustomAlert
        visible={isVisible}
        // toggle={toggleIdle}
        styles={{ borderRadius: 5 }}>
        <CloseIcon
          size={29}
          onPress={toggleIdle}
          color={'black'}
          style={styles.modalClose}
        />
        <View style={styles.modalContent}>
          <View style={styles.modalSubContent}>
            <Text style={styles.modalTitle}>Are you still there?</Text>
            <View style={styles.modalList}>
              <Text style={styles.modalSubTitle}>
                You were idle for 5 mins.
              </Text>
              <Text style={styles.modalSubTitle}>
                You will be automatically logged out after {maxTime} seconds.
              </Text>
            </View>
            <View style={styles.buttonGroup}>
              <FormButton
                title="Logout now"
                type="outline"
                onPress={handleLogout}
                buttonStyle={styles.logoutButton}
                titleStyle={styles.logoutTitle}
              />
              <FormButton
                title="Yes! I'm still here"
                onPress={toggleIdle}
                buttonStyle={styles.hereButton}
                titleStyle={styles.hereTitle}
              />
            </View>
          </View>
        </View>
      </CustomAlert>
    </SafeAreaView>
  );
};

export default BottomTabs;
