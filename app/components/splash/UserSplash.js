import React, { useEffect } from 'react';
import { ImageBackground, Image } from 'react-native';
import { StackActions, useFocusEffect } from '@react-navigation/native';
import { useDispatch, useSelector } from 'react-redux';
import JailMonkey from 'jail-monkey';

import routes from '../../globals/navigations/Routes';
import styles from './Styles';

const Splash = ({ navigation }) => {
  const dispatch = useDispatch();
  const login = useSelector(state => state.login);

  const { LANDING, MAIN_DASHBOARD, JAIL_BROKEN_ROOTED } = routes;

  useFocusEffect(() => {
    setTimeout(
      () =>
        navigation.dispatch(
          StackActions.replace(
            // eslint-disable-next-line no-nested-ternary
            //
            // !JailMonkey.isDebuggedMode() && JailMonkey.isJailBroken()
            JailMonkey.isJailBroken()
              ? JAIL_BROKEN_ROOTED
              : login.result.token
              ? MAIN_DASHBOARD
              : LANDING,
          ),
        ),
      1000,
    );
  });

  return (
    <ImageBackground
      style={styles.container}
      source={require('../../images/assets/landing-background-image.png')}>
      <Image
        style={styles.image}
        source={require('../../images/assets/psslai_logo.png')}
      />
    </ImageBackground>
  );
};

export default Splash;
