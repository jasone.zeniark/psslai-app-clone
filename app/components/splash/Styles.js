import {StyleSheet, Dimensions} from 'react-native';

const {height} = Dimensions.get('screen');


const Styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: height * 0.22,
    height: height * 0.062,
  },
});

export default Styles;