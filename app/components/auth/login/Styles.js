import { StyleSheet, Dimensions } from 'react-native';

const { width } = Dimensions.get('screen');

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  loginButton: {
    height: width * 0.15,
    marginVertical: 30,
    marginHorizontal: 10,
    backgroundColor: '#201751',
    borderRadius: 4,
  },

  disclaimerContainer: {
    alignSelf: 'center',
    marginTop: '50%',
    marginHorizontal: 15,
  },

  textDisclaimer: {
    textAlign: 'center',
    color: '#9e9e9e',
    fontSize: 12,
  },

  error: {
    marginTop: -18,
    marginStart: 12,
    fontSize: 10,
    marginBottom: 10,
    color: 'red',
  },
});

export default Styles;
