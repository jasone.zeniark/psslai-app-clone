/* eslint-disable no-shadow */
import React, { useState, useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { View, Text, BackHandler, Alert } from 'react-native';
import BackgroundTimer from 'react-native-background-timer';
import { getUniqueId, getBrand, getModel } from 'react-native-device-info';

import {
  EyeoPasswordIcon,
  EyePasswordIcon,
} from '../../../images/icons/UserIcons';

import {
  loginUser,
  loginUserAttempt,
  resetLoginUserAttempt,
} from '../../../store/actions/LoginActions';

import Container from '../../../globals/layout/Container';
import Header from '../../../globals/header/CommonHeader';
import FormButton from '../../../globals/form/button/FormButton';
import FormInput from '../../../globals/form/input/FormInput';
import ListItem from '../../../globals/item/ListItems';
import routes from '../../../globals/navigations/Routes';
import { decryptObject } from '../../../request/RequestEncryption';
import { NavigateWeb } from '../../../globals/Helpers';

import config from '../../../request/Config';
import styles from './Styles';
import { apiLogin, apiProfile } from '../../../request/Auth';

import { createNativeWrapper } from 'react-native-gesture-handler';
import { color } from 'react-native-reanimated';

const Login = ({ navigation }) => {
  const login = useSelector(state => state.login);
  const dispatch = useDispatch();

  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [usernameFocus, setUsernameFocus] = useState(false);
  const [passwordFocus, setPasswordFocus] = useState(false);
  const [isPassVisible, setPassVisible] = useState(false);
  const [isSubmit, setSubmit] = useState(false);
  const [isConnected, setConnected] = useState(true);
  const [comparedDate, setComparedDate] = useState(0);

  const [attempts, setAttempts] = useState(3);
  const [reset, setReset] = useState(false);

  // const [alertVisible, setAlertVisible] = useState(false)
  // const [alertMessage, setAlertMessage] = useState('')

  const [errorUsername, setUsernameError] = useState('');
  const [errorPassword, setPasswordError] = useState('');
  const interval = useRef(false);

  const { OTP, TERMS_AND_CONDITIONS, PRIVACY_POLICY } = routes;

  const [message, setMessage] = useState('');

  const {
    psslai_download_form,
    psslai_login_problem,
    psslai_terms_conditions,
  } = config;

  const items = [
    {
      title: 'Forgot username or password',
      handler: () => NavigateWeb(navigation, psslai_login_problem),
    },
    {
      title: 'Update mobile number',
      handler: () => NavigateWeb(navigation, psslai_download_form),
    },
    {
      title: 'Terms & Conditions',
      handler: () => navigation.navigate(TERMS_AND_CONDITIONS),
    },
  ];

  const requestLoginBody = {
    username,
    password,
    deviceId: getUniqueId(),
    brand: getBrand(),
    phoneModel: getModel(),
  };

  useEffect(() => {
    const backAction = () => {
      navigation.navigate(routes.LANDING);
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);

  useEffect(() => {
    setConnected(login.connected);
    if (!login.connected) {
      setSubmit(false);
    }
  }, [login.connected]);

  useEffect(() => {
    // console.log('Attempts: ', login.loginAttempts);
    setAttempts(login.loginAttempts);
  }, [attempts, login.loginAttempts, login.attemptDate]);

  useEffect(() => {
    if (reset) {
      setReset(false);
      handleNetworkLogin();
    }
  }, [reset]);

  // Handler for setting the state of passwords.
  const handlePasswordIcon = () => {
    setPassVisible(!isPassVisible);
  };

  const fetchProfile = async data => {
    let fetchResult;
    let fetchError;
    const { token, guid } = data.result;
    const apiResponse = await apiProfile(token, guid);
    if (apiResponse.status === 200) {
      const { data } = apiResponse;
      // fetchResult = { success: true, data: decryptObject(data.result) }; 
      fetchResult = { success: true, data: data.result }; // CHANGES 05-31-22 due to no encrption on BE
      console.log('profile: ', fetchResult);
    } else {
      // handle errors here
      const { data } = apiResponse;
      if (typeof data.errors !== 'undefined') {
        let errMsg = '';
        for (let ctr = 0; ctr < data.errors.length; ctr += 1) {
          const errorObj = data.errors[ctr];
          errMsg += `${errorObj.msg}\r\n`;
        }
        fetchError = errMsg;
      } else {
        fetchError = data.message;
      }
      fetchResult = { success: false, error: fetchError };
    }
    return fetchResult;
  };

  // Handler for submitting login credentials.
  const handleLogin = async () => {
    if (isValidUser()) {
      let apiResponse = '';

      setSubmit(true);

      const dateAttempt = new Date(login.attemptDate);

      const compareDateInMinutes = Math.round(
        (((new Date() - dateAttempt) % 86400000) % 3600000) / 60000,
      );

      if (login.loginAttempts === 0 && compareDateInMinutes >= 15) {
        dispatch(resetLoginUserAttempt());
        setReset(true);
      }

      if (login.loginAttempts !== 0) {
        apiResponse = await apiLogin(requestLoginBody);
      }

      console.log('login response: ', apiResponse);

      if (typeof apiResponse.data !== 'undefined') {
        const { data } = apiResponse;
        if (typeof data.status !== 'undefined') {
          if (data.status === 1) {
            const saveUserProfileData = await fetchProfile(data);
            if (saveUserProfileData.success === true) {
              dispatch(loginUser(data, saveUserProfileData.data));
              dispatch(resetLoginUserAttempt());
              // navigation.navigate(routes.MAIN_DASHBOARD);
              setSubmit(false);
            } else {

              Alert.alert('', saveUserProfileData.fetchError);
            }
          } else {
            let errMsg = '';
            for (let ctr = 0; ctr < data.errors.length; ctr += 1) {
              const errorObj = data.errors[ctr];
              errMsg += `${errorObj.msg}\r\n`;
            }
            Alert.alert('', data.message);
            setSubmit(false);
          }
        } else {
          let errMsg = '';
          Object.keys(data).forEach(key => {
            errMsg += `${data[key]}\r\n`;
          });
          setSubmit(false);
          Alert.alert('', data.message);
        }
      } else {
        setSubmit(false);

        if (login.loginAttempts === 0) {
          const remainingTime = 15 - compareDateInMinutes;
          if (remainingTime >= 1) {
            Alert.alert(
              'Sign In Error',
              `Login failed. Please try again after ${remainingTime} minute${
                remainingTime === 1 ? '.' : 's.'
              }`,
            );
          }
        } else {
          setAttempts(prev => prev - 1);
          dispatch(loginUserAttempt(login.loginAttempts - 1, new Date()));
          Alert.alert('Sign In Error', apiResponse.message);
        }
        // setAlertVisible(true)
        // setAlertMessage(apiResponse.message)
      }
    }
  };

  const isValidUser = () => {
    if (username == '') {
      setUsernameError('Please enter username');
      return false;
    }
    if (password == '') {
      setPasswordError('Please enter password');
      return false;
    }
    return true;
  };

  const handleNetworkLogin = () => {
    if (isConnected) {
      handleLogin();
    } else {
      Alert.alert(
        'Network Connection Error',
        'Please check your mobile network connection',
      );
    }
  };

  return (
    <>
      <Header
        title="Log In"
        onBackPress={() => navigation.navigate(routes.LANDING)}
      />

      <Container style={{ paddingTop: '3%' }}>
        <FormInput
          name="username"
          onChangeText={(value: any) => {
            setUsername(value);
            setUsernameError('');
          }}
          error={isSubmit && !username}
          onFocus={() => setUsernameFocus(true)}
          onBlur={() => setUsernameFocus(false)}
          isFocus={usernameFocus}
          placeholder="Username"
        />

        <Text style={[styles.error]}>{errorUsername}</Text>

        <FormInput
          name="password"
          onChangeText={(value: any) => {
            setPassword(value);
            setPasswordError('');
          }}
          error={isSubmit && !password}
          onFocus={() => setPasswordFocus(true)}
          onBlur={() => setPasswordFocus(false)}
          isFocus={passwordFocus}
          rightIcon={() =>
            isPassVisible ? (
              <EyePasswordIcon
                size={24}
                color={!passwordFocus ? '#D1D5DD' : '#2B2D33'}
                onPress={handlePasswordIcon}
              />
            ) : (
              <EyeoPasswordIcon
                size={24}
                color={!passwordFocus ? '#D1D5DD' : '#2B2D33'}
                onPress={handlePasswordIcon}
              />
            )
          }
          rightIconContainerStyle={{ height: 24 }}
          placeholder="Password"
          secureTextEntry={!isPassVisible}
        />

        <Text style={[styles.error]}>{errorPassword}</Text>

        <FormButton
          title="Log In"
          buttonStyle={styles.loginButton}
          loading={isSubmit}
          onPress={handleNetworkLogin}
        />

        <ListItem list={items} />

        <View style={[styles.disclaimerContainer]}>
          <Text style={[styles.textDisclaimer]}>
            PSSLAI is regulated by the Bangko Sentral ng Pilipinas
          </Text>
          <Text style={[styles.textDisclaimer]}>
            (02) 8706 7067 • consumeraffairs@bsp.gov.ph
          </Text>
        </View>
      </Container>

      {/* {showAlertMessage()} */}
    </>
  );
};

export default Login;
