import React, { useState, useEffect } from 'react';
import { View, Image, ImageBackground, Text, ScrollView } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';

import {
  AddUserIcon,
  CardPersonalizedIcon,
} from '../../../images/icons/UserIcons';

import { LinkingData, NavigateWeb } from '../../../globals/Helpers';

import FormButton from '../../../globals/form/button/FormButton';
import NetworkBanner from '../../../globals/header/NetworkBanner';
import routes from '../../../globals/navigations/Routes';

import config from '../../../request/Config';
import styles from './Styles';

const { LOGIN, SIGNUP } = routes;

const { psslai_become_member, psslai_download_form } = config;

const AuthLanding = ({ navigation }) => {
  const login = useSelector(state => state.login);
  const dispatch = useDispatch();

  const [isConnected, setConnected] = useState(true);

  useEffect(() => {
    setConnected(login.connected);
  }, [login.connected]);

  return (
    <>
      <NetworkBanner connected={isConnected} />
      <ScrollView>
        <ImageBackground
          source={require('../../../images/assets/landing-background-image.png')}
          style={styles.imageBackground}>
          <View style={styles.headContainer}>
            <Image
              style={styles.image}
              source={require('../../../images/assets/psslai_logo.png')}
            />
            {/* <FormButton
        title="Log in"
        type="outline"
        onPress={() => navigation.navigate(LOGIN)}
        buttonStyle={styles.loginButton}
        titleStyle={styles.loginTitle}
      /> */}
          </View>
          <Text style={styles.textStyle}>
            View your balances, send money, pay bills, buy load and more!
          </Text>
          <FormButton
            title="Log in"
            type="outline"
            onPress={() => navigation.navigate(LOGIN)}
            buttonStyle={styles.loginButton}
            titleStyle={styles.loginTitle}
          />
          <FormButton
            title="Sign up"
            onPress={() => navigation.navigate(SIGNUP)}
            buttonStyle={styles.signUpButton}
            titleStyle={styles.signUpTitle}
          />
          <View style={styles.clearButtonContainer}>
            <FormButton
              icon={<CardPersonalizedIcon size={styles.iconSize.height} />}
              title="Update mobile no. or membership info."
              type="clear"
              buttonStyle={styles.clearButton}
              titleStyle={styles.clearButtonTitle}
              onPress={() => NavigateWeb(navigation, psslai_download_form)}
            />
            <FormButton
              icon={<AddUserIcon size={styles.iconSize.height} />}
              title="Become a PSSLAI member"
              type="clear"
              buttonStyle={styles.clearButton}
              titleStyle={styles.clearButtonTitle}
              onPress={() => NavigateWeb(navigation, psslai_become_member)}
            />
            <Text style={styles.infoText}>
              PSSLAI is regulated by the Banko Sentral ng Pilipinas.
              <Text
                style={styles.underlinedText}
                onPress={() => LinkingData(`tel: (02) 8706-7067`)}>
                {' '}
                (02) 8706-7067{' '}
              </Text>{' '}
              •
              <Text
                style={styles.underlinedText}
                onPress={() =>
                  LinkingData(
                    'mailto:consumeraffairs@bsp.gov.ph?subject=Psslai Send Mail&body=Hello Sir/Madam',
                  )
                }>
                {' '}
                consumeraffairs@bsp.gov.ph
              </Text>
            </Text>
          </View>
        </ImageBackground>
      </ScrollView>
    </>
  );
};

export default AuthLanding;
