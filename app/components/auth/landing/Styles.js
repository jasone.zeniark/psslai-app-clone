import { StyleSheet, Dimensions, Platform } from 'react-native';
import {
  DARK_BLUE,
  GRAY,
  WHITE,
  FONT,
  SEMI_LIGHT,
} from '../../../globals/Theme';

const { width, height } = Dimensions.get('screen');

const os = Platform.OS === 'ios';

const Styles = StyleSheet.create({
  imageBackground: {
    height,
    paddingHorizontal: 24,
    paddingTop: 28,
  },
  headContainer: {
    marginTop: 28,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  image: {
    marginTop: 4,
    width: height * 0.17,
    height: height * 0.047,
  },
  loginButton: {
    height: height * 0.055,
    borderRadius: 36,
    borderWidth: 2,
    marginBottom: 10,
    borderColor: DARK_BLUE,
  },
  loginTitle: {
    ...FONT,
    color: '#000',
    fontSize: height * 0.0185,
    fontWeight: '600',
  },
  textStyle: {
    fontFamily: 'Montserrat-Medium',
    alignSelf: 'center',
    textAlign: 'justify',
    lineHeight: 40,
    marginTop: height * 0.15,
    marginBottom: width * 0.1,
    fontSize: height * 0.028,
    color: DARK_BLUE,
    fontWeight: '600',
  },
  signUpButton: {
    height: height * 0.055,
    backgroundColor: DARK_BLUE,
    borderRadius: 36,
  },
  signUpTitle: {
    ...FONT,
    color: WHITE,
    fontWeight: '600',
    fontSize: height * 0.019,
  },
  clearButtonContainer: {
    marginTop: 20,
    marginBottom: 32,
    justifyContent: 'space-between',
    height: height * 0.25,
  },
  clearButton: {
    borderRadius: 36,
    shadowColor: os ? GRAY : SEMI_LIGHT,
    backgroundColor: WHITE,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.15,
    shadowRadius: 15.19,
    elevation: os ? 2 : 0,
    minHeight: height * 0.05,
    justifyContent: 'center',
    paddingVertical: 10,
    flexWrap: 'wrap',
  },
  clearButtonTitle: {
    ...FONT,
    color: DARK_BLUE,
    marginLeft: 12,
    marginRight: 'auto',
    textAlign: 'justify',
    width: '87%',
    fontSize: height * 0.0181,
    fontWeight: '600',
  },
  underlinedText: {
    textDecorationLine: 'underline',
  },
  infoText: {
    ...FONT,
    color: GRAY,
    fontSize: height * 0.0166,
    textAlign: 'center',
  },
  iconSize: {
    height: height * 0.025,
  },
});

export default Styles;
