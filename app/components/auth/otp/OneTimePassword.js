import React, { useState, useEffect } from 'react';
import {
  Text,
  TouchableOpacity,
  View,
  Platform,
  PermissionsAndroid,
  Alert,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { StackActions } from '@react-navigation/native';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import BackgroundTimer from 'react-native-background-timer';
//import SmsListener from 'react-native-android-sms-listener';

import {
  resendOTP,
  verifyOTP,
  resetOTP,
} from '../../../store/actions/OTPActions';
import {
  apiVerifyOtp,
  apiResendOtp,
  apiRequestOtp,
} from '../../../request/Auth';

import Container from '../../../globals/layout/Container';
import ErrorActivity from '../../../globals/form/error/ErrorActivity';
import Header from '../../../globals/header/CommonHeader';
import Loader from '../../../globals/loader/UserLoader';
import routes from '../../../globals/navigations/Routes';

import styles from './Styles';

const OneTimePassword = ({ navigation, route }) => {
  const dispatch = useDispatch();
  const otp = useSelector(state => state.otp);
  const login = useSelector(state => state.login);
  const signup = useSelector(state => state.signup);

  const [code, setCode] = useState('');
  const [isFilled, setFilled] = useState(false);
  const [maxTime, setMaxTime] = useState(180);
  const [isResend, setResend] = useState(false);
  const [verifyStatus, setVerifyStatus] = useState(0);
  const [resendStatus, setResendStatus] = useState(0);
  const [errorMessage, setErrorMessage] = useState('');

  const [isConnected, setConnected] = useState(true);

  let mobile = route.params.prevData.result.mobileNumber;
  let { pinId } = route.params.prevData.result;

  if (typeof route.params.requestOTP !== 'undefined') {
    mobile = route.params.prevData.result.profile.mobileNumber;
  }

  const { LOGIN, MAIN_DASHBOARD } = routes;

  // Handler for otp resend timer (180 secs).
  const handleResendTimer = () => {
    setResend(!isResend);

    const interval = BackgroundTimer.setInterval(() => {
      setMaxTime(timer => {
        if (timer === 1) {
          closeInterval();
        }
        return timer - 1;
      });
    }, 1000);

    const closeInterval = () => {
      setResend(false);
      setMaxTime(180);

      return BackgroundTimer.clearInterval(interval);
    };
  };

  // Handler for resending OTP code.
  const handleResend = async () => {
    setFilled(true);
    dispatch(resetOTP());

    const params = {
      memberGuid: route.params.prevData.result.guid,
    };

    if (isConnected) {
      let apiResponse;
      if (typeof route.params.requestOTP !== 'undefined') {
        apiResponse = await apiRequestOtp(params);
      } else {
        apiResponse = await apiResendOtp(params);
      }

      console.log('resend response: ', apiResponse);

      if (typeof apiResponse.data !== 'undefined') {
        const { data } = apiResponse;

        setResendStatus(data.status);
        setFilled(false);

        if (data.status === 1) {
          setErrorMessage('');
          dispatch(resendOTP(data));
          // Timer (180 secs).
          handleResendTimer();
        } else {
          const { data } = apiResponse;
          if (typeof data.errors !== 'undefined') {
            let errMsg = '';
            for (let ctr = 0; ctr < data.errors.length; ctr += 1) {
              const errorObj = data.errors[ctr];
              errMsg += `${errorObj.msg}\r\n`;
            }
            setErrorMessage(errMsg);
            setFilled(false);
          } else {
            setFilled(false);
            setErrorMessage(data.message);
          }
        }
      } else {
        setFilled(false);
        setErrorMessage(apiResponse.message);
      }
    } else {
      Alert.alert(
        'Network Connection Error',
        'Please check your mobile network connection',
      );

      setFilled(false);
    }
  };

  // Handler for navigating other screens from OTP page.
  const handleNavigate = () => {
    if (typeof route.params.requestOTP !== 'undefined') {
      navigation.goBack();
    } else {
      let screen = '';
      if (route.params.prevRoute === 'signup') {
        screen = LOGIN;
      } else {
        screen = MAIN_DASHBOARD;
      }
      navigation.dispatch(StackActions.replace(screen));
    }
  };

  // Handler for verifying OTP code.
  const handleVerifyOtp = async value => {
    setFilled(true);
    if (typeof route.params.requestOTP !== 'undefined' || resendStatus === 1) {
      pinId = otp.pinId;
    } else {
      dispatch(resetOTP());
    }

    const params = {
      memberGuid: route.params.prevData.result.guid,
      pinId,
      pin: value.toString(),
    };

    if (login.connected) {
      const apiResponse = await apiVerifyOtp(params);

      console.log('verify otp response: ', apiResponse);

      if (typeof apiResponse.data !== 'undefined') {
        const { data } = apiResponse;

        setVerifyStatus(data.status);
        setFilled(false);

        if (data.status === 1) {
          setErrorMessage('');
          dispatch(verifyOTP(data));
          setCode('');
          handleNavigate();
        } else {
          setErrorMessage(data.message);
          setCode('');
        }
      } else {
        setFilled(false);
        setCode('');
        setErrorMessage(apiResponse.message);
      }
    } else {
      Alert.alert(
        'Network Connection Error',
        'Please check your mobile network connection',
      );

      setFilled(false);
    }
  };

  // Handler for asking permission to verify otp.
  const requestReadSmsPermission = async () => {
    try {
      let granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_SMS,
        {
          title: 'Auto Verification OTP',
          message: 'Need access to read sms, to verify OTP',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.RECEIVE_SMS,
          {
            title: 'Receive SMS',
            message: 'Need access to receive sms, to verify OTP',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          const listener = SmsListener.addListener(message => {
            const body = message.body.match(/\d{6}/).toString();

            setCode(body);
            setTimeout(() => {
              handleVerifyOtp(body);
              listener.remove();
            }, 1000);
            // message.body will have the message.
            // message.originatingAddress will be the address.
          });
        } else {
          alert('RECEIVE_SMS permissions denied');
        }
      } else {
        alert('READ_SMS permissions denied');
      }
    } catch (err) {
      alert(err);
    }
  };

  // Handler for OTP code while changing/typing.
  const handleCodeChange = code => {
    setCode(code);
    setErrorMessage('');
  };

  useEffect(() => {
    console.log('new pinID: ', otp.pinId);
  }, [otp.pinId]);

  useEffect(() => {
    handleResendTimer();
  }, []);

  useEffect(() => {
    if (Platform.OS === 'android') {
      // requestReadSmsPermission();
    }
  }, []);

  useEffect(() => {
    setConnected(login.connected);
  }, [login.connected]);

  return (
    <>
      <Header
        title="One Time Password"
        onBackPress={() => navigation.goBack()}
      />
      <Container style={styles.otpFormContainer}>
        <Text style={styles.verifyTextTitle}>
          {'Verify your mobile number'}
        </Text>
        <Text style={styles.sentTextTitle}>
          {`We sent a One-Time Password to +63******${mobile.slice(
            mobile.length - 4,
          )}`}
        </Text>

        <OTPInputView
          style={styles.otpInput}
          pinCount={6}
          code={code}
          onCodeChanged={code => handleCodeChange(code)}
          autoFocusOnLoad={Platform.OS === 'ios'}
          codeInputFieldStyle={styles.underlineStyleBase}
          codeInputHighlightStyle={styles.underlineStyleHighLighted}
          placeholderCharacter="-"
          onCodeFilled={code => handleVerifyOtp(code)}
        />

        <View style={styles.activityContainer}>
          {isFilled ? (
            <Loader />
          ) : errorMessage ? (
            verifyStatus === 0 || resendStatus === 0 ? (
              <ErrorActivity message={errorMessage} />
            ) : null
          ) : null}
        </View>

        <Text style={styles.recieveText}>{"Didn't receive code?"}</Text>
        {isResend ? (
          <Text style={styles.resendTextNow}>RESEND IN ({maxTime} SECS)</Text>
        ) : (
          <TouchableOpacity onPress={handleResend}>
            <Text style={styles.resendText}>RESEND OTP NOW</Text>
          </TouchableOpacity>
        )}
      </Container>
    </>
  );
};

export default OneTimePassword;
