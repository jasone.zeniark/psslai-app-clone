import { StyleSheet, Dimensions } from 'react-native';

import colors from '../../../globals/colors';
import { FONT, WHITE, DARK_BLUE, ERROR } from '../../../globals/Theme';

const { primary_hard, light, semi_light } = colors;

const { height, width } = Dimensions.get('screen');

export default StyleSheet.create({
  otpFormContainer: {
    alignItems: 'center',
    height: 300,
  },
  activityContainer: {
    marginVertical: height * 0.02,
  },
  otpInput: {
    marginTop: '12%',
    alignItems: 'center',
    width: '80%',
    height: height * 0.1,
  },
  verifyTextTitle: {
    fontFamily: 'Montserrat-Bold',
    marginTop: '20%',
    fontSize: height * 0.029,
    marginVertical: 20,
  },
  sentTextTitle: {
    fontFamily: 'Montserrat-Regular',
    fontSize: height * 0.0178,
    width: 'auto',
  },
  underlineStyleBase: {
    paddingHorizontal: 10,
    width: height * 0.05,
    height: height * 0.075,
    borderRadius: 5,
    borderWidth: 1,
    fontSize: height * 0.027,
    color: primary_hard,
    backgroundColor: semi_light,
  },
  underlineStyleHighLighted: {
    borderWidth: 2,
    borderColor: DARK_BLUE,
    backgroundColor: WHITE,
  },
  resendText: {
    ...FONT,
    marginTop: 5,
    fontSize: height * 0.03,
    color: DARK_BLUE,
  },
  resendTextNow: {
    ...FONT,
    opacity: 0.55,
    marginTop: 5,
    fontSize: height * 0.03,
    color: DARK_BLUE,
  },
  recieveText: {
    marginTop: '8%',
    fontFamily: 'Montserrat-Regular',
    fontSize: height * 0.023,
  },
  errorText: {
    fontFamily: 'Montserrat-Regular',
    fontSize: height * 0.02,
    color: ERROR,
  },
});
