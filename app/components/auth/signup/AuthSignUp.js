/* eslint-disable prettier/prettier */
/* eslint-disable operator-linebreak */
import React, { useState, useEffect } from 'react';
import { Text, View, ScrollView, Platform, Alert } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { Formik, Field } from 'formik';
import * as yup from 'yup';

import Container from '../../../globals/layout/Container';
import Header from '../../../globals/header/CommonHeader';
import FormButton from '../../../globals/form/button/FormButton';
import FormikInput from '../../../globals/form/input/FormikInput';
import FormCheckbox from '../../../globals/form/checkbox/FormCheckbox';
import ListItem from '../../../globals/item/ListItems';
import routes from '../../../globals/navigations/Routes';
import { NavigateWeb } from '../../../globals/Helpers';
import regExp from '../../../globals/utils/RegExp';

import {
  EyeoPasswordIcon,
  EyePasswordIcon,
} from '../../../images/icons/UserIcons';

import {
  registerUser,
  signupReset,
} from '../../../store/actions/SignUpActions';
import config from '../../../request/Config';
import { apiSignUpUser } from '../../../request/Auth';

import styles from './Styles';

const AuthSignUp = ({ navigation }) => {
  const dispatch = useDispatch();
  const login = useSelector(state => state.login);
  const signup = useSelector(state => state.signup);

  const [isSubmitting, setIsSubmitting] = useState(false);
  const [isAgreementChecked, setChecked] = useState(false);
  const [isPassVisible, setPassVisible] = useState(false);
  const [isConfirmPassVisible, setConfirmPassVisible] = useState(false);
  const [passwordFocus, setPasswordFocus] = useState(false);
  const [confirmPassFocus, setConfirmPassFocus] = useState(false);
  const [isConnected, setConnected] = useState(true);

  const { OTP, TERMS_AND_CONDITIONS, PRIVACY_POLICY } = routes;
  /* eslint-disable camelcase */
  const { psslai_become_member } = config;

  const items = [
    {
      title: 'Become a PSSLAI member',
      handler: () => NavigateWeb(navigation, psslai_become_member),
    },
  ];

  const initialValues = {
    psslai_member_number: '',
    username: '',
    password: '',
    confirm_password: '',
    agreement: '',
  };

  const inputErrors = {
    username:
      'Username must have 8-20 characters and use alphanumeric characters only.',
    password:
      'Password must be 8-20 characters composed of the following: lower and upper case letters, numbers and special characters',
    confirmPass: 'Setting and confirmation of password are not the same.',
    agreement:
      'You must agree to the Privacy Policy and the Terms and Conditions.',
  };

  const handleSignUp = async (formData, { resetForm }) => {
    setIsSubmitting(true);
    dispatch(signupReset());

    if (!isConnected) {
      Alert.alert(
        'Network Connection Error',
        'Please check your mobile network connection',
      );

      setIsSubmitting(false);
    } else if (
      formData.psslai_member_number === '' ||
      formData.username === '' ||
      formData.password === '' ||
      formData.confirm_password === ''
    ) {
      Alert.alert('Sign up error', 'Mandatory fields not completed.');

      setIsSubmitting(false);
    } else if (!isAgreementChecked) {
      Alert.alert('Sign up error', inputErrors.agreement);
      setIsSubmitting(false);
    } else {
      const apiResponse = await apiSignUpUser({ ...formData, agreement: true });
      if (apiResponse.status === 200) {
        const { data } = apiResponse;
        dispatch(registerUser(data));
        navigation.navigate(OTP, { prevRoute: 'signup', prevData: data });
        // resetForm({});
        // setChecked(false);
        setIsSubmitting(false);
      } else {
        // handle errors here
        const { data } = apiResponse;
        if (typeof data.errors !== 'undefined') {
          let errMsg = '';
          for (let ctr = 0; ctr < data.errors.length; ctr += 1) {
            const errorObj = data.errors[ctr];
            errMsg += `${errorObj.msg}\r\n`;
          }
          Alert.alert('Sign up error', errMsg);
          setIsSubmitting(false);
        } else {
          setIsSubmitting(false);
          Alert.alert('Sign up error', data.message);
        }
      }
    }
  };

  const signUpValidationSchema = yup.object().shape({
    psslai_member_number: yup.string(),
    username: yup
      .string()
      .matches(regExp.alphanumeric, inputErrors.username)
      .min(8, () => inputErrors.username),
    password: yup.string().matches(regExp.password, inputErrors.password),
    confirm_password: yup
      .string()
      .oneOf([yup.ref('password')], inputErrors.confirmPass),
  });

  const handlePasswordIcon = (value, setValue) => {
    setValue(!value);
  };

  const passwordIconRendering = (focus, value, setValue) =>
    value ? (
      <EyePasswordIcon
        size={24}
        color={!focus ? '#D1D5DD' : '#2B2D33'}
        onPress={() => handlePasswordIcon(value, setValue)}
      />
    ) : (
      <EyeoPasswordIcon
        size={24}
        color={!focus ? '#D1D5DD' : '#2B2D33'}
        onPress={() => handlePasswordIcon(value, setValue)}
      />
    );

  useEffect(() => {
    dispatch(signupReset());
  }, []);

  useEffect(() => {
    setConnected(login.connected);
    if (!login.connected) {
      setIsSubmitting(false);
    }
  }, [login.connected]);

  return (
    <>
      <Header title="Sign Up" onBackPress={() => navigation.goBack()} />
      <ScrollView
        showsVerticalScrollIndicator={false}
        keyboardShouldPersistTaps="handled"
        style={styles.scrollView}>
        <Container>
          <View style={styles.formWrapper}>
            <Formik
              validationSchema={signUpValidationSchema}
              initialValues={initialValues}
              onSubmit={handleSignUp}>
              {({ handleSubmit }) => (
                <>
                  <Field
                    component={FormikInput}
                    name="psslai_member_number"
                    placeholderText="Your 8-digit membership no."
                    labelText="Membership No."
                    keyboardType={
                      Platform.OS === 'ios'
                        ? 'numbers-and-punctuation'
                        : 'numeric'
                    }
                    editable={!isSubmitting}
                  />
                  <Field
                    component={FormikInput}
                    name="username"
                    placeholderText="At least 8 characters"
                    labelText="Username"
                    maxLength={20}
                    editable={!isSubmitting}
                  />
                  <Field
                    component={FormikInput}
                    name="password"
                    placeholderText="Enter password"
                    labelText="Set Password"
                    maxLength={20}
                    secureTextEntry={!isPassVisible}
                    onFocusText={() => setPasswordFocus(true)}
                    onBlurText={() => setPasswordFocus(false)}
                    isFocus={passwordFocus}
                    rightIcon={() =>
                      passwordIconRendering(
                        passwordFocus,
                        isPassVisible,
                        setPassVisible,
                      )
                    }
                    rightIconContainerStyle={{ height: 24 }}
                    editable={!isSubmitting}
                  />
                  <Field
                    component={FormikInput}
                    name="confirm_password"
                    placeholderText="Must match password"
                    labelText="Confirm Password"
                    maxLength={20}
                    secureTextEntry={!isConfirmPassVisible}
                    onFocusText={() => setConfirmPassFocus(true)}
                    onBlurText={() => setConfirmPassFocus(false)}
                    isFocus={confirmPassFocus}
                    rightIcon={() =>
                      passwordIconRendering(
                        confirmPassFocus,
                        isConfirmPassVisible,
                        setConfirmPassVisible,
                      )
                    }
                    rightIconContainerStyle={{ height: 24 }}
                    editable={!isSubmitting}
                  />

                  <View style={styles.checkboxContainer}>
                    <FormCheckbox
                      checked={isAgreementChecked}
                      onPress={() => setChecked(!isAgreementChecked)}
                      name="agreement"
                      disabled={isSubmitting}
                    />
                    <Text style={styles.checkboxLabel}>
                      By signing up, I have read and agree with PSSLAI’s{' '}
                      <Text
                        style={styles.privacyLabel}
                        onPress={() => navigation.navigate(PRIVACY_POLICY)}>
                        Privacy Policy
                      </Text>{' '}
                      and the{' '}
                      <Text
                        style={styles.privacyLabel}
                        onPress={() =>
                          navigation.navigate(TERMS_AND_CONDITIONS)
                        }>
                        Terms and Conditions
                      </Text>
                    </Text>
                  </View>
                  <FormButton
                    title="Sign Up"
                    titleStyle={styles.signUpText}
                    buttonStyle={styles.signUpButton}
                    onPress={handleSubmit}
                    disabled={isSubmitting}
                    loading={isSubmitting}
                    disabledStyle={styles.signUpBtnDisabled}
                  />
                </>
              )}
            </Formik>
          </View>
          <ListItem list={items} />
        </Container>
      </ScrollView>
    </>
  );
};

export default AuthSignUp;
