import { StyleSheet, Dimensions } from 'react-native';
import colors from '../../../globals/colors';

const { white } = colors;
const { width } = Dimensions.get('screen');

const Styles = StyleSheet.create({
  scrollView: {
    backgroundColor: white,
  },
  formWrapper: {
    paddingHorizontal: 10,
  },
  checkboxContainer: {
    flexDirection: 'row',
    marginLeft: -10,
  },
  checkboxLabel: {
    fontSize: 16,
    width: '85%',
    fontWeight: '100',
    color: '#2B2D33',
    fontFamily: 'Montserrat-Regular',
  },
  privacyLabel: {
    color: '#2A3D99',
    fontFamily: 'Montserrat-Regular',
  },
  signUpText: {
    fontFamily: 'Montserrat-Regular',
  },
  signUpButton: {
    paddingVertical: 15,
    marginTop: 20,
    marginBottom: 40,
    marginHorizontal: 0,
    backgroundColor: '#201751',
    fontSize: 36,
    fontFamily: 'Montserrat-Regular',
    borderRadius: 4,
  },
  signUpBtnDisabled: {
    backgroundColor: '#201751',
  },
});

export default Styles;
