import { StyleSheet, Dimensions } from 'react-native';
import { DARK_BLUE } from '../../../globals/Theme';

const { height } = Dimensions.get('screen');

const Styles = StyleSheet.create({
  container: {
    paddingHorizontal: 25,
  },
  title: {
    fontFamily: 'Montserrat-Bold',
    alignSelf: 'center',
    marginBottom: 20,
    fontSize: height * 0.028,
    color: DARK_BLUE,
  },
  header: {
    fontFamily: 'Montserrat-Regular',
    marginBottom: 10,
    fontSize: height * 0.025,
    color: DARK_BLUE,
    fontWeight: '600',
    textAlign: 'center',
  },
  headerVisaCard: {
    fontFamily: 'Montserrat-Regular',
    marginBottom: 10,
    fontSize: height * 0.025,
    color: DARK_BLUE,
    fontWeight: '600',
    marginTop: 20,
    textAlign: 'center',
  },
  linkText: {
    fontFamily: 'Montserrat-Regular',
    color: DARK_BLUE,
  },
  text: {
    fontFamily: 'Montserrat-Regular',
    marginBottom: 10,
    fontSize: height * 0.018,
    fontWeight: '600',
  },
  textAddress: {
    fontFamily: 'Montserrat-Regular',
    fontSize: height * 0.018,
    fontWeight: '600',
  },
  textBottom: {
    fontFamily: 'Montserrat-Regular',
    marginTop: 15,
    marginBottom: 10,
    fontSize: height * 0.018,
    fontWeight: '600',
  },
  textTable: {
    fontFamily: 'Montserrat-Regular',
    fontSize: height * 0.018,
    fontWeight: '600',
    padding: 8,
  },
  textHeadTable: {
    fontFamily: 'Montserrat-Regular',
    fontSize: height * 0.018,
    fontWeight: 'bold',
    padding: 10,
  },
  boldText: {
    fontFamily: 'Montserrat-Bold',
    marginBottom: 10,
    fontSize: height * 0.018,
  },
  center: {
    textAlign: 'center',
  },
  table: {
    borderWidth: 1,
    borderColor: '#000',
  },
  tableWrapper: {
    marginBottom: 25,
  },
});

export default Styles;
