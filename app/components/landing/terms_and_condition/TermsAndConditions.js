import React from 'react';
import { View, ScrollView } from 'react-native';
import { Text } from 'react-native-elements';
import { Table, Row, Rows } from 'react-native-table-component';

import Container from '../../../globals/layout/Container';
import Header from '../../../globals/header/CommonHeader';
import BulletItems from '../../../globals/bullet/bulletItems';
import { NavigateWeb } from '../../../globals/Helpers';

import styles from './Styles';
// const AuthLanding = ({ navigation }) => (

const TermsAndConditions = ({ navigation }) => {
  const bulletListA = [
    {
      bullet: 'I.',
      content: (
        <>
          <Text style={styles.boldText}>Definition of Terms</Text>
          <Text style={styles.text}>
            The following words and expressions carry the following meaning:
          </Text>
        </>
      ),
      subContent: [
        {
          bullet: 'A.',
          content: (
            <Text style={styles.text}>
              <Text style={styles.boldText}>"Account" </Text>
              shall refer to any or all of the member’s accounts - capital
              contribution (Capcon), savings account, current loan, or any other
              accounts with the Association which may be accessed via any
              Digital Services offered by the Association.
            </Text>
          ),
        },
        {
          bullet: 'B.',
          content: (
            <Text style={styles.text}>
              <Text style={styles.boldText}>"Member Codes" </Text>
              shall refer to any of the member number, iTrack password, iTrack
              Mobile Personal Identification Number (MPIN), Kiosk PIN and
              username.
            </Text>
          ),
        },
        {
          bullet: 'C.',
          content: (
            <Text style={styles.text}>
              <Text style={styles.boldText}>"Member Number" </Text>
              means the unique number combination issued by the Association to
              members upon membership enrolment.
            </Text>
          ),
        },
        {
          bullet: 'D.',
          content: (
            <Text style={styles.text}>
              <Text style={styles.boldText}>"Account Holder" </Text>
              shall refer to a member who is maintaining a Capcon, savings,
              investment and/or loan account with the Association.
            </Text>
          ),
        },
        {
          bullet: 'E.',
          content: (
            <Text style={styles.text}>
              <Text style={styles.boldText}>"Account Terms" </Text>
              shall refer to the terms and conditions of the relevant Account(s)
              and/or Third Party Account(s) which may be accessed through the
              PSSLAI Digital Services.
            </Text>
          ),
        },
        {
          bullet: 'F.',
          content: (
            <Text style={styles.text}>
              <Text style={styles.boldText}>"Association" </Text>
              shall refer to the Public Safety Savings and Loan Association,
              Inc.
            </Text>
          ),
        },
        {
          bullet: 'G.',
          content: (
            <Text style={styles.text}>
              <Text style={styles.boldText}>"Business Day" </Text>
              means any day from Monday to Friday when the Association is open
              for business in the Philippines.
            </Text>
          ),
        },
        // {
        //   bullet: 'G.',
        //   content: (
        //     <Text style={styles.text}>
        //       <Text style={styles.boldText}>"Bill" </Text>
        //       means the latest bill, renewal notice and/or payment demand
        //       received by the member from an accredited Payee
        //       Corporation/Institution.
        //     </Text>
        //   ),
        // },
        // {
        //   bullet: 'H.',
        //   content: (
        //     <Text style={styles.text}>
        //       <Text style={styles.boldText}>"Business Day" </Text>
        //       means any day from Monday to Friday when the Association is open
        //       for business in the Philippines.
        //     </Text>
        //   ),
        // },
        {
          bullet: 'H.',
          content: (
            <Text style={styles.text}>
              <Text style={styles.boldText}>"First Time User’s Code" </Text>
              means the temporary 6-digit code given by the Association to the
              member upon registration for the usage of any of the PSSLAI
              Digital Services and shall be used for the first time logon to the
              PSSLAI Bilis Mobile Application and Bilis Online facilities.
            </Text>
          ),
        },
        {
          bullet: 'I.',
          content: (
            <Text style={styles.text}>
              <Text style={styles.boldText}>"One-Time Transaction Pin" </Text>
              means the 6-digit code sent by the Association via SMS for every
              transaction using the PSSLAI Digital Services which shall be used
              to complete the requested financial transaction.
            </Text>
          ),
        },
        {
          bullet: 'J.',
          content: (
            <Text style={styles.text}>
              <Text style={styles.boldText}>"Instruction" </Text>
              means any/all instructions given by the member with respect to
              his/her Account(s) or any other transaction using the PSSLAI
              Digital Services.
            </Text>
          ),
        },
        {
          bullet: 'K.',
          content: (
            <Text style={styles.text}>
              <Text style={styles.boldText}>"Internet" </Text>
              means the global network of computers and telecommunications
              systems which facilitate communication.
            </Text>
          ),
        },
        {
          bullet: 'L.',
          content: (
            <Text style={styles.text}>
              <Text style={styles.boldText}>"Network Service Provider" </Text>
              means any Internet service provider providing connection to the
              Internet.
            </Text>
          ),
        },
        {
          bullet: 'M.',
          content: (
            <Text style={styles.text}>
              <Text style={styles.boldText}>"Password" </Text>
              means a unique string of characters the member has chosen and
              known only to the member. This must be keyed in by the member for
              authentication of username and grant of access to the PSSLAI
              Digital Services.
            </Text>
          ),
        },
        // {
        //   bullet: 'O.',
        //   content: (
        //     <Text style={styles.text}>
        //       <Text style={styles.boldText}>"Payee Corporation" </Text>
        //       means billing corporations accredited by the Association or its
        //       Third-Party partners from time to time, displayed and accessible
        //       via the PSSLAI Digital Services and PSSLAI website, whose bills can be
        //       paid using the various PSSLAI Digital Services.
        //     </Text>
        //   ),
        // },
        {
          bullet: 'N.',
          content: (
            <Text style={styles.text}>
              <Text style={styles.boldText}>"Philippine Peso" or "PhP" </Text>
              means the lawful currency of the Republic of the Philippines.
            </Text>
          ),
        },
        {
          bullet: 'O.',
          content: (
            <Text style={styles.text}>
              <Text style={styles.boldText}>"Terms and Conditions" </Text>
              means these Terms and Conditions governing the use of the PSSLAI
              Digital Services.
            </Text>
          ),
        },
        {
          bullet: 'P.',
          content: (
            <Text style={styles.text}>
              <Text style={styles.boldText}>"Third Party Account(s)" </Text>
              means the member’s account(s) with other bank(s), Payee
              Corporations, or other third parties. This also includes accounts
              with the Association which are not enrolled in the PSSLAI Digital
              Services.
            </Text>
          ),
        },
        {
          bullet: 'Q.',
          content: (
            <Text style={styles.text}>
              <Text style={styles.boldText}>"Transaction Record" </Text>
              means the confirmation of the receipt and/or execution of an
              Instruction initiated by the member through the PSSLAI Digital
              Services as reflected in the Transaction History page.
            </Text>
          ),
        },
        {
          bullet: 'R.',
          content: (
            <Text style={styles.text}>
              <Text style={styles.boldText}>"Username" </Text>
              means the unique name created and chosen using a string of
              characters by the member that must be inputted in order for the
              PSSLAI Digital Services system to associate the Username with the
              member’s user profile and Account(s).
            </Text>
          ),
        },
      ],
    },
    {
      bullet: 'II.',
      content: (
        <Text style={styles.boldText}>General Terms and Conditions</Text>
      ),
      subContent: [
        {
          bullet: 'A.',
          content: (
            <Text style={styles.text}>
              You hereby agree that the Association’s records and any records of
              the Instructions, communications, operations or transactions made
              or performed, processed or effected through the PSSLAI Digital
              Services accessed and used via use of your Password and Username
              and any record of transactions relating to the operation of any of
              the PSSLAI Digital Services shall be binding and conclusive on you
              for all purposes whatsoever and shall be conclusive evidence of
              the transaction and your liability to the Association.
            </Text>
          ),
        },
        {
          bullet: 'B.',
          content: (
            <Text style={styles.text}>
              You agree that the Association has the sole and absolute right to
              require you to maintain a minimum balance at any one time in your
              Account(s). Your failure to maintain such a minimum balance as
              required by the Association could, at the Association’s
              discretion, result in a penalty being imposed against you or lead
              to a suspension or termination of the PSSLAI Digital Services as
              provided for in these terms and conditions.
            </Text>
          ),
        },
        {
          bullet: 'C.',
          content: (
            <Text style={styles.text}>
              The Association does not warrant the accuracy of any information
              pertaining to your Account(s), Third Party Account(s) or
              transactions as reported through the PSSLAI Digital Services.
              Hence, you agree to ensure that the information you provide to the
              Association in relation to the PSSLAI Digital Services is true,
              complete, updated and accurate.
            </Text>
          ),
        },
        {
          bullet: 'D.',
          content: (
            <Text style={styles.text}>
              Any Instructions you have transmitted after the relevant cut-off
              time on any day will be posted in the books and records of the
              Association on or for the next Business Day following the date of
              the Instruction.
            </Text>
          ),
        },
        {
          bullet: 'E.',
          content: (
            <Text style={styles.text}>
              You shall not be entitled to use the PSSLAI Digital Services if
              there exists any restrictions whatsoever in relation to your
              Account(s) or Third Party Account(s) either imposed by the
              Association or by any relevant authorities.
            </Text>
          ),
        },
        {
          bullet: 'F.',
          content: (
            <Text style={styles.text}>
              The provisions of our kiosks and all computer devices and/or
              terminals (Property) available for your use at our branches and
              satellite offices are specially for your use and access of the
              PSSLAI Digital Services only and shall not be used for or in
              connection with any illegal purpose or activity.
            </Text>
          ),
        },
        {
          bullet: 'G.',
          content: (
            <Text style={styles.text}>
              You agree that it is your obligation to maintain the personal
              device or terminal used in accessing the PSSLAI Digital Services.
            </Text>
          ),
        },
        {
          bullet: 'H.',
          content: (
            <Text style={styles.text}>
              The Association reserves the right to add, modify, delete or vary
              any of these Terms and Conditions by way of notice as deemed
              suitable by the Association.
            </Text>
          ),
        },
        {
          bullet: 'I.',
          content: (
            <Text style={styles.text}>
              You agree to view these Terms and Conditions regularly and your
              continued access or use of the www.psslai.com
              <Text
                style={styles.linkText}
                onPress={() =>
                  // eslint-disable-next-line implicit-arrow-linebreak
                  NavigateWeb(navigation, 'https://www.psslai.com')
                }>
                {' '}
                www.psslai.com{' '}
              </Text>
              website and/or PSSLAI Digital Services after any such additions,
              modifications, deletions or variations become effective will
              constitute your acceptance to the variation of these Terms and
              Conditions.
            </Text>
          ),
        },
      ],
    },
    {
      bullet: 'III.',
      content: (
        <Text style={styles.boldText}>
          Application / Registration to Use the PSSLAI Digital Services
        </Text>
      ),
      subContent: [
        {
          bullet: 'A.',
          content: (
            <Text style={styles.text}>
              You may apply to use the PSSLAI Digital Services provided that you
              meet the following requirements:
            </Text>
          ),
          subContent: [
            {
              bullet: '1.',
              content: (
                <Text style={styles.text}>
                  A bonafide member of PSSLAI; and
                </Text>
              ),
            },
            {
              bullet: '2.',
              content: (
                <Text style={styles.text}>
                  Has an updated mobile number in PSSLAI’s membership profile
                  system.
                </Text>
              ),
            },
          ],
        },
        {
          bullet: 'B.',
          content: (
            <Text style={styles.text}>
              It is the Association’s sole and absolute discretion whether to
              reject or accept your application for PSSLAI Digital Services.
            </Text>
          ),
        },
        {
          bullet: 'C.',
          content: (
            <Text style={styles.text}>
              In cases where the Association has disapproved your application,
              the Association is not obliged to provide any reason for such
              rejection or to respond to any request for information. If you
              have previously applied for registration to the PSSLAI Digital
              Services and thereafter cancelled or deactivated the service for
              whatever reason and you wish to re-avail of the said services, you
              shall have to go through the same registration process as if you
              are applying for the first time.
            </Text>
          ),
        },
        {
          bullet: 'D.',
          content: (
            <Text style={styles.text}>
              For joint account holders, kindly take note of the following, viz:
            </Text>
          ),
          subContent: [
            {
              bullet: '1.',
              content: (
                <Text style={styles.text}>
                  "And" joint accounts are not eligible for this facility.
                </Text>
              ),
            },
            {
              bullet: '2.',
              content: (
                <Text style={styles.text}>
                  Owners of "And-Or" joint accounts may apply for separate and
                  independent access to these services. For "And/Or" joint
                  accounts, you understand and agree that all transactions to be
                  made by any one of you via the PSSLAI Digital Services is/are
                  deemed done with the consent of all co-account owners.
                </Text>
              ),
            },
          ],
        },
      ],
    },
    {
      bullet: 'IV.',
      content: (
        <Text style={styles.boldText}>
          {' '}
          Access to the PSSLAI Digital Services
        </Text>
      ),
      subContent: [
        {
          bullet: 'A.',
          content: (
            <Text style={styles.text}>
              If you are a member of the Association and wishes to gain access
              via all the services available in the PSSLAI Digital Services such
              as but not limited to the Kiosk, PSSLAI Bilis Mobile Application
              and Bilis Online facilities, you may subscribe to the same with
              the following procedure:
            </Text>
          ),
          subContent: [
            {
              bullet: '1.',
              content: (
                <Text style={styles.text}>
                  You must be a member of the Association with an updated mobile
                  number in PSSLAI’s membership system.
                </Text>
              ),
            },

            {
              bullet: '2.',
              content: (
                <Text style={styles.text}>
                  You should download the PSSLAI Bilis Mobile Application from
                  either Apple App Store or Google Play, depending on the gadget
                  that you are using. For the PSSLAI Bilis Online, you can
                  access the online facility by visiting/clicking
                  <Text
                    style={styles.linkText}
                    onPress={() =>
                      // eslint-disable-next-line implicit-arrow-linebreak
                      NavigateWeb(
                        navigation,
                        'https://www.bilisonline.psslai.com',
                      )
                    }>
                    {' '}
                    https://www.bilisonline.psslai.com
                  </Text>
                  .
                </Text>
              ),
            },
            {
              bullet: '3.',
              content: (
                <Text style={styles.text}>
                  For your first login to either the PSSLAI Bilis Mobile or
                  Bilis Online facilities, you will be required to enroll /
                  register first for the use of either facility. After
                  registration, you are required to key in your nominated
                  username and password as well as the{' '}
                  <Text style={styles.boldText}>First Time User’s Code </Text>
                  (FTUC) which will be sent to either your iTrack registered
                  number or mobile number indicated in your membership profile
                  in PSSLAI’s system.
                </Text>
              ),
            },
            {
              bullet: '4.',
              content: (
                <Text style={styles.text}>
                  Upon the on-screen confirmation of your Username, Password and
                  FTUC, your access to the PSSLAI Bilis Mobile or Bilis Online
                  will be activated.
                </Text>
              ),
            },
          ],
        },
        {
          bullet: 'B.',
          content: (
            <Text style={styles.text}>
              The Association has no obligation to verify the authority of the
              person/s giving the Instruction sent through the PSSLAI Digital
              Services or to check the accuracy and completeness of the
              transaction performed. Such instruction shall be deemed correct,
              complete and binding. You hereby accept full responsibility for
              all transactions executed with the use of your Username and
              Password.
            </Text>
          ),
        },
        {
          bullet: 'C.',
          content: (
            <Text style={styles.text}>
              To access the PSSLAI Digital Services, you are required to key in
              the following:
            </Text>
          ),
          subContent: [
            {
              bullet: '1.',
              content: <Text style={styles.text}>Your Username; and</Text>,
            },
            {
              bullet: '2.',
              content: <Text style={styles.text}>Your Password; or</Text>,
            },
            {
              bullet: '3.',
              content: (
                <Text style={styles.text}>
                  Personal Identification Number (PIN) for the kiosks; or
                </Text>
              ),
            },
            {
              bullet: '4.',
              content: <Text style={styles.text}>Mobile PIN for iTrack.</Text>,
            },
          ],
        },
        {
          bullet: 'D.',
          content: (
            <Text style={styles.text}>
              You agree that the Association has the right to invalidate your
              Username and Password without being obliged to offer you any
              explanation or prior notice and that you will not hold the
              Association liable for any loss or damage which you may suffer as
              a result of such invalidation of Username and Password.
            </Text>
          ),
        },
        {
          bullet: 'E.',
          content: (
            <Text style={styles.text}>
              You may change your Password at any time. You agree that the
              Association uses your Username and Password to identify you and
              hereby acknowledges that you must keep both your Username and
              Password strictly confidential and to exercise reasonable care to
              prevent unauthorized access and/or use.
            </Text>
          ),
        },
        {
          bullet: 'F.',
          content: (
            <Text style={styles.text}>
              You are responsible for ensuring that you have logged off the
              service at the end of each session. Once you have logged on to the
              PSSLAI Digital Services, you must not leave the terminal or other
              devices from which you have accessed the PSSLAI Digital Services
              at any time or let anyone else use it until you have logged off.
            </Text>
          ),
        },
        {
          bullet: 'G.',
          content: (
            <Text style={styles.text}>
              In cases when the Association may have to request from you your
              Username in order to provide maintenance services to you, please
              be assured that your Username shall be kept safe and confidential.
              Do not give your password to anyone and be assured that the
              Association will never ask for your password.
            </Text>
          ),
        },
        {
          bullet: 'H.',
          content: (
            <Text style={styles.text}>
              It is understood that if you have not made any activities via the
              PSSLAI Digital Services within 6 months from the date of last
              transaction, the system will automatically suspend your account.
              You need to contact the Association’s Member Care Group to
              re-activate the account. Likewise, if you have not done any
              activities within 1 year, the system will automatically cancel
              your PSSLAI Digital Services access. You will then need to
              re-apply for your PSSLAI Digital Services access.
            </Text>
          ),
        },
        {
          bullet: 'I.',
          content: (
            <Text style={styles.text}>
              PSSLAI Digital Services will use either your registered and
              activated iTrack mobile number or mobile number indicated in your
              membership profile (if you are not an iTrack user) for the sending
              of the One-Time Pin (OTP). If you do not receive your FTUC/OTP
              within 24 hours from registration, you are advised to call the
              Association’s Member Care Group so that the Association may take
              appropriate measures accordingly.
            </Text>
          ),
        },
        {
          bullet: 'J.',
          content: (
            <Text style={styles.text}>
              It shall be your duty to immediately inform the Association of any
              change / update of your mobile number by filling out the required
              request form at the Membership Management Relations and Care
              Department (MMRCD) at the PSSLAI Head Office or you may visit any
              PSSLAI Branch or Satellite Offices nationwide. The Association
              will not recognize any mobile numbers that are not duly registered
              (or changed) in accordance with the procedure provided herein. It
              is understood that the Association shall not be held liable for
              any damage, loss or injury that you may suffer on account of your
              non-compliance with the required procedure for the change in the
              registered mobile number.
            </Text>
          ),
        },
        {
          bullet: 'K.',
          content: (
            <Text style={styles.text}>
              All information, including but not limited to FTUC and OTP sent by
              the Association to your registered mobile number shall be
              considered duly received by you. Thus, you shall be solely
              responsible for securing any such information received through
              your registered mobile number and holds the Association free and
              harmless from any liability you may suffer on account of the
              Association’s sending information to your registered mobile
              number, unless you have duly informed the Association earlier in
              the manner prescribed herein, regarding any change in your mobile
              number.
            </Text>
          ),
        },
      ],
    },
    {
      bullet: 'V.',
      content: (
        <Text style={styles.boldText}>Using the PSSLAI Digital Services</Text>
      ),
      subContent: [
        {
          bullet: 'A.',
          content: (
            <Text style={styles.text}>
              Upon the activation of the PSSLAI Digital Services, you will be
              able to access various digital services as the Association and/or
              any other 3rd Party may from time to time make available to you on
              the website or mobile application.
            </Text>
          ),
        },
        {
          bullet: 'B.',
          content: (
            <Text style={styles.text}>
              When the Association introduces new services under the PSSLAI
              Digital Services, the Association may provide them on such
              supplementary terms which shall be notified to you.
            </Text>
          ),
        },
        {
          bullet: 'C.',
          content: (
            <Text style={styles.text}>
              You agree and accept that any Instructions which you may issue
              shall at all times be subject to such limits and conditions as may
              be fixed or specified by the Association from time to time at its
              absolute discretion.
            </Text>
          ),
        },
        {
          bullet: 'D.',
          content: (
            <Text style={styles.text}>
              In cases which involve or require the consent or approval of third
              parties, the Association’s obligation to perform any Instructions
              would be subject to such consent and approval being obtained by
              you.
            </Text>
          ),
        },
        {
          bullet: 'E.',
          content: (
            <Text style={styles.text}>
              You shall ensure that there are sufficient funds at all times
              available in your Account(s) to perform any of your Instructions.
            </Text>
          ),
        },
        {
          bullet: 'F.',
          content: (
            <Text style={styles.text}>
              You agree that the Association is entitled to debit your
              Account(s) to effect any transaction and any corresponding fees
              and charges you have instructed.
            </Text>
          ),
        },
        {
          bullet: 'G.',
          content: (
            <Text style={styles.text}>
              You agree that the Association has the absolute right to add,
              suspend, limit, withdraw, cancel or vary any transactions,
              facilities, services and products that you can access through the
              PSSLAI Digital Services and the scope and/or extent of such
              transactions, facilities, services and products.
            </Text>
          ),
        },
      ],
    },
    {
      bullet: 'VI.',
      content: <Text style={styles.boldText}>Authorization </Text>,
      subContent: [
        {
          bullet: 'A.',
          content: (
            <Text style={styles.text}>
              You hereby authorize the Association to comply with all
              Instruction(s) given using the PSSLAI Digital Services accessed
              using your Username and Password and to deal with the said
              Instruction(s) as properly authorized by you even if they may
              conflict with any other mandate given at any time concerning your
              Accounts or affairs.
            </Text>
          ),
        },
        {
          bullet: 'B.',
          content: (
            <Text style={styles.text}>
              You agree that such Instruction(s) shall be binding on you upon
              its transmission to the Association. Once transmitted, the
              Instructions cannot be changed or withdrawn without the
              Association’s consent and the Association is not obliged to check
              further the authenticity of such Instruction(s).
            </Text>
          ),
        },
      ],
    },
    {
      bullet: 'VII. ',
      content: <Text style={styles.boldText}>Guidelines on Instructions </Text>,
      subContent: [
        {
          bullet: 'A.',
          content: (
            <Text style={styles.text}>
              You hereby agree that it is your responsibility to review the
              Transaction Record pertaining to any PSSLAI Digital Services
              transaction initiated on your given Instruction/s.
            </Text>
          ),
        },
        {
          bullet: 'B.',
          content: (
            <Text style={styles.text}>
              Should you have any reason to believe that an Instruction has not
              been accurately or completely received by the Association, you
              shall inform the Association by telephone no later than twelve
              (12) hours after transmission of the relevant Instruction(s).
            </Text>
          ),
        },
        {
          bullet: 'C.',
          content: (
            <Text style={styles.text}>
              Any Instruction to the Association for cancellation, revocation,
              reversal or amendment or clarification of your earlier
              Instructions can only be effected if your request is received
              before the earlier Instruction is executed.
            </Text>
          ),
        },
        {
          bullet: 'D.',
          content: (
            <Text style={styles.text}>
              The Association reserves the right at its sole discretion, to
              refuse to carry out any of your Instructions where your
              Instructions are inconsistent with the laws or the Association’s
              policies or for any other reasons whatsoever.
            </Text>
          ),
        },
        {
          bullet: 'E.',
          content: (
            <Text style={styles.text}>
              Where you give Instructions to the Association to effect
              transactions in relation to the PSSLAI Digital Services, you shall
              provide accurate and complete details as required by the
              Association.
            </Text>
          ),
        },
        {
          bullet: 'F.',
          content: (
            <Text style={styles.text}>
              The Association shall not be liable for any failure, delay or
              shortcoming, however caused, by any third party with whom you have
              accounts or otherwise when they are executing the Association’s
              instructions to them.
            </Text>
          ),
        },
      ],
    },
    {
      bullet: 'VIII. ',
      content: <Text style={styles.boldText}> Service Availability </Text>,
      subContent: [
        {
          bullet: 'A.',
          content: (
            <Text style={styles.text}>
              The PSSLAI Digital Services is intended to be available seven (7)
              days a week, twenty-four (24) hours a day but there is no warranty
              that the same will be available at all times. The Association will
              use reasonable efforts to inform you of any or all services under
              the PSSLAI Digital Services which are not available from time to
              time. Relatively, corresponding cut-off time/period for fund
              transfers, eloading and balance inquiry are indicated within the
              appropriate tab (for Bilis Online and Bilis Mobile App) or
              included in the reply messages for iTrack.
            </Text>
          ),
        },
        {
          bullet: 'B.',
          content: (
            <Text style={styles.text}>
              It is understood however that some or all of the services may not
              be available at certain times due to system maintenance and/or
              computer, telecommunication, electrical or network failure, or any
              other analogous reason beyond the control of the Association. You
              acknowledge that the Association will not be liable in case you
              are unable to access the system. The Association is not under any
              obligation to notify or explain to you the reason for any system
              unavailability as provided herein.
            </Text>
          ),
        },
      ],
    },
    {
      bullet: 'IX. ',
      content: (
        <>
          <Text style={styles.boldText}>
            {' '}
            Services Available in the PSSLAI Digital Services{' '}
          </Text>
          <Text style={styles.text}>
            You can perform or initiate transactions using the PSSLAI Digital
            Services from your accounts with the Association. Financial
            transactions shall be available for Capcon and Savings Account only.
            Non-financial or inquiry transactions shall also be available for
            all your PSSLAI accounts, as well as for your outstanding loan
            accounts.
          </Text>
          <Text style={styles.text}>
            The following are the services that are available for specific
            accounts using the PSSLAI Digital Services which are also provided
            in the website.
          </Text>
        </>
      ),
      subContent: [
        {
          bullet: 'A.',
          content: (
            <>
              <Text style={styles.text}>Account Inquiries</Text>
              <Text style={styles.text}>
                You can inquire about the balances of your Capcon, Savings and
                Loan accounts in PSSLAI as well as the balances of your prepaid
                card account with our accredited Third Party partners.
              </Text>
            </>
          ),
        },
        {
          bullet: 'B.',
          content: (
            <>
              <Text style={styles.text}>Fund Transfer</Text>
            </>
          ),
          subContent: [
            {
              bullet: '1.',
              content: (
                <Text style={styles.text}>
                  Transfer of funds to either your own Account or to an account
                  of a third party which is maintained in another financial
                  institution, will be transacted and received by the payee
                  based on the Association’s agreement with the financial
                  institution’s respective Third Party partners.
                </Text>
              ),
            },
            {
              bullet: '2.',
              content: (
                <Text style={styles.text}>
                  Fund Transfer Instructions shall be subject to such limits and
                  conditions as may be imposed and/or revised from time to time
                  by the Association or by the relevant rules of government
                  authorities, at their absolute discretion.
                </Text>
              ),
            },
            {
              bullet: '3.',
              content: (
                <Text style={styles.text}>
                  Fund Transfer Instructions where you are transferring funds to
                  your own account or the accounts of third parties with any
                  other financial institution shall also be subject to the terms
                  and conditions approved by the Association and/or the
                  financial institution with which that other account is
                  maintained. For the avoidance of doubt, this Fund Transfer
                  service shall be available for the transfer of funds to your
                  own account or the accounts of third parties with such
                  financial institution(s) as may be determined by the
                  Association from time to time.
                </Text>
              ),
            },
            {
              bullet: '4.',
              content: (
                <Text style={styles.text}>
                  You acknowledge and agree that the Association shall not be
                  responsible or liable for any delay or failure to effect Fund
                  Transfer where the other financial institution to which the
                  Fund Transfer is made has delayed, rejected, refused or is
                  otherwise unable to accept such Fund Transfer. The Association
                  will immediately or, as soon as practicable, inform you of
                  such delay, rejection, refusal or otherwise non-acceptance of
                  such Fund Transfer through the Transaction status display
                  and/or the Transaction History feature of the system.
                </Text>
              ),
            },
            {
              bullet: '5.',
              content: (
                <Text style={styles.text}>
                  The Association shall only be able to effect the fund transfer
                  if the source account has sufficient balance to cover for the
                  charges.
                </Text>
              ),
            },
          ],
        },

        // {
        //   bullet: 'C.',
        //   content: (
        //     <>
        //       <Text style={styles.text}>Loan Application</Text>
        //     </>
        //   ),
        //   subContent: [
        //     {
        //       bullet: '1.',
        //       content: (
        //         <Text style={styles.text}>
        //           Transfer of funds to either your own Account or to an account
        //           of a third party which is maintained in another financial
        //           institution, will be transacted and received by the payee
        //           based on the Association’s agreement with the financial
        //           institution’s respective Third Party partners.
        //         </Text>
        //       ),
        //     },
        //     {
        //       bullet: '2.',
        //       content: (
        //         <Text style={styles.text}>
        //           Fund Transfer Instructions shall be subject to such limits and
        //           conditions as may be imposed and/or revised from time to time
        //           by the Association or by the relevant rules of government
        //           authorities, at their absolute discretion.
        //         </Text>
        //       ),
        //     },
        //     {
        //       bullet: '3.',
        //       content: (
        //         <Text style={styles.text}>
        //           Fund Transfer Instructions where you are transferring funds to
        //           your own account or the accounts of third parties with any
        //           other financial institution shall also be subject to the terms
        //           and conditions approved by the Association and/or the
        //           financial institution with which that other account is
        //           maintained. For the avoidance of doubt, this Fund Transfer
        //           service shall be available for the transfer of funds to your
        //           own account or the accounts of third parties with such
        //           financial institution(s) as may be determined by the
        //           Association from time to time.
        //         </Text>
        //       ),
        //     },
        //     {
        //       bullet: '4.',
        //       content: (
        //         <Text style={styles.text}>
        //           You acknowledge and agree that the Association shall not be
        //           responsible or liable for any delay or failure to effect Fund
        //           Transfer where the other financial institution to which the
        //           Fund Transfer is made has delayed, rejected, refused or is
        //           otherwise unable to accept such Fund Transfer. The Association
        //           will immediately or, as soon as practicable, inform you of
        //           such delay, rejection, refusal or otherwise non-acceptance of
        //           such Fund Transfer through the Transaction status display
        //           and/or the Transaction History feature of the system.
        //         </Text>
        //       ),
        //     },
        //     {
        //       bullet: '5.',
        //       content: (
        //         <Text style={styles.text}>
        //           The Association shall only be able to effect the fund transfer
        //           if the source account has sufficient balance to cover for the
        //           charges.
        //         </Text>
        //       ),
        //     },
        //   ],
        // },
        {
          bullet: 'C.',
          content: (
            <>
              <Text style={styles.text}>Loan Application</Text>
              <Text style={styles.text}>
                You may submit your application for loans in the PSSLAI Digital
                Services as applicable. It is understood however that this
                facility shall only be for the purpose of receiving your
                application for loan. The Association shall start processing
                your application on the next business day subject to the
                Association’s usual requirements and procedure for approval.
              </Text>
            </>
          ),
        },
        {
          bullet: 'D.',
          content: (
            <Text style={styles.text}>Prepaid Top Up / Loading Facility</Text>
          ),
          subContent: [
            {
              bullet: '1.',
              content: (
                <Text style={styles.text}>
                  The Association may amend its list of Mobile Networks/ Top Up
                  Institutions without assigning any reason and you agree that
                  the Association shall not be liable for any loss or damage
                  which you may suffer as a result of the Association’s actions.
                </Text>
              ),
            },
            {
              bullet: '2.',
              content: (
                <Text style={styles.text}>
                  The Prepaid Top Up/Loading Facility service of the Association
                  shall be used only for the purpose of reloading your mobile
                  phones or other amenities. You hereby agree to abide by the
                  procedures, requirements and terms of each Mobile Network/ Top
                  Up Institutions which may change from to time in respect of
                  settling their mobile top ups/ reloads.
                </Text>
              ),
            },
            {
              bullet: '3.',
              content: (
                <Text style={styles.text}>
                  The Association shall not be liable for any loss or damage
                  suffered by you as a result of any delay in sending reload
                  Mobile Networks/ Top Up Institutions on account of force
                  majeure or reasons beyond the control of the Association,
                  unless such failure is due to the Association’s gross
                  negligence.
                </Text>
              ),
            },
            {
              bullet: '4.',
              content: (
                <Text style={styles.text}>
                  The Association shall not be under any duty to ensure punctual
                  top up by you and neither shall the Association be under any
                  duty to monitor reload.
                </Text>
              ),
            },
            {
              bullet: '5.',
              content: (
                <Text style={styles.text}>
                  The Association shall only effect the prepaid top up reload if
                  the source account has sufficient balance to cover for the
                  charges.
                </Text>
              ),
            },
            {
              bullet: '6.',
              content: (
                <Text style={styles.text}>
                  Notices sent to you pursuant to the Prepaid Top Up service
                  will not be considered an official record of the Association.
                  The information as per the Association’s system and records
                  shall be binding and final proof of any transaction carried
                  over the Prepaid Top Up service. In case of conflict, the
                  official records of the Association will be conclusive and
                  prevail over any such notices.
                </Text>
              ),
            },
          ],
        },
      ],
    },
    {
      bullet: 'X.',
      content: <Text style={styles.boldText}>Fees and Charges</Text>,
      subContent: [
        {
          bullet: 'A.',
          content: (
            <Text style={styles.text}>
              You acknowledge that the Association shall be entitled to levy or
              impose service charges or transaction fees from time to time in
              respect of your use of or access to the PSSLAI Digital Services.
            </Text>
          ),
        },
        {
          bullet: 'B.',
          content: (
            <Text style={styles.text}>
              You acknowledge that you are responsible for all charges imposed
              by service providers in enabling you to access and/or connect to
              the PSSLAI Digital Services. You are also responsible for any fees
              and charges imposed by any Network Service Provider.
            </Text>
          ),
        },
        {
          bullet: 'C.',
          content: (
            <Text style={styles.text}>
              Subject to the Account Terms of your Account(s), you acknowledge
              that the Association reserves the right to debit your relevant
              Account(s) for any amounts due and any Government charges, or
              taxes payable as a result of the use of the PSSLAI Digital
              Services.
            </Text>
          ),
        },
      ],
    },
    {
      bullet: 'XI.',
      content: (
        <Text style={styles.boldText}>
          Deactivation, Cancellation and Re-activation of the PSSLAI Digital
          Services.
        </Text>
      ),
      subContent: [
        {
          bullet: 'A.',
          content: (
            <Text style={styles.text}>
              Upon completion of registration process, you understand that your
              enrollment in the PSSLAI Digital Services shall be effective upon
              receipt of the Association’s confirmation of the activation of
              your account(s) in PSSLAI Digital Services and shall remain in
              full force and effect until the Association receives from you a
              written notice of its termination and filling out of the required
              form for its termination/deactivation in any PSSLAI Branch or
              Satellite Offices or any instruction of termination received from
              you through the Association's Member Care Group. You understand
              however that the Association may, at any time, terminate this
              arrangement without prior written notice of termination.
            </Text>
          ),
        },
      ],
    },
    {
      bullet: 'XII.',
      content: (
        <Text style={styles.boldText}>
          Suspension or Termination of Services
        </Text>
      ),
      subContent: [
        {
          bullet: 'A.',
          content: (
            <Text style={styles.text}>
              Notwithstanding anything herein to the contrary, the Association
              may at any time, in its absolute discretion, suspend or terminate
              your right of access to any of the PSSLAI Digital Services without
              notice for any reason whatsoever and without any obligation to
              give any reasons.
            </Text>
          ),
        },
        {
          bullet: 'B.',
          content: (
            <Text style={styles.text}>
              The Association shall automatically terminate your right of access
              to the PSSLAI Digital Services should you terminate your
              membership with the Association or cease to maintain any
              Account(s) with the Association which can be accessed via the
              PSSLAI Digital Services or should your access to such Account(s)
              be restricted by the Association for any reason.
            </Text>
          ),
        },
        {
          bullet: 'C.',
          content: (
            <Text style={styles.text}>
              You may terminate the PSSLAI Digital Services immediately by
              contacting the Association’s Member Care Group. You agree that the
              Association shall not be obliged to effect any of your
              Instructions received on any day falling after the receipt of your
              notice of termination. The Association reserves the right to
              immediately terminate the PSSLAI Digital Services for any reason
              whatsoever.
            </Text>
          ),
        },
        {
          bullet: 'D.',
          content: (
            <Text style={styles.text}>
              You acknowledge that the termination of the service as provided
              herein will not affect your liability or obligations in respect of
              the instructions processed by the Association on your behalf prior
              to any termination.
            </Text>
          ),
        },
      ],
    },
    {
      bullet: 'XIII.',
      content: <Text style={styles.boldText}>Confidentiality</Text>,
      subContent: [
        {
          bullet: 'A.',
          content: (
            <Text style={styles.text}>
              You accept that you will be responsible for the confidentiality
              and use of your Member Codes and that you shall at no time and
              under no circumstances reveal your Member Codes to anyone
              including the staff of the Association.
            </Text>
          ),
        },
        {
          bullet: 'B.',
          content: (
            <Text style={styles.text}>
              You undertake to observe all security measures prescribed by the
              Association concerning your Member Codes or generally in respect
              of the use of the PSSLAI Digital Services. The Association shall
              be indemnified and not be held liable against any and all
              proceedings, claims, losses, damages or expenses, including legal
              costs, that may arise from any of the following conditions:
            </Text>
          ),
          subContent: [
            {
              bullet: '1.',
              content: (
                <Text style={styles.text}>
                  Any of your instructions verified by us or by the System as
                  conveyed with the use of your Access Number, Username and
                  Password;
                </Text>
              ),
            },
            {
              bullet: '2.',
              content: (
                <Text style={styles.text}>
                  Any loss or damage due to your interference or tampering with,
                  or alteration or misuse of, or amendment of the website.
                </Text>
              ),
            },
          ],
        },
        {
          bullet: 'C.',
          content: (
            <Text style={styles.text}>
              Should you have any reason to believe that any of your Access
              Codes have been misused and/or compromised by disclosure or
              discovery, you must inform the Association immediately.
            </Text>
          ),
        },
        {
          bullet: 'D.',
          content: (
            <Text style={styles.text}>
              Should you receive any data and information through the PSSLAI
              Digital Services which is not intended for you, you agree that all
              such data or information shall be deleted from your computer
              system device immediately and that you will promptly notify the
              Association of such fact by telephone no later than twelve (12)
              hours from your receipt thereof.
            </Text>
          ),
        },
        {
          bullet: 'E.',
          content: (
            <Text style={styles.text}>
              While the Association has endeavored to put in place reasonable
              measures to secure the System, the Association does not assume any
              warranty on the confidentiality, secrecy and security of any
              information sent out through any internet service provider,
              network system or other similar system via PSSLAI Digital
              Services.
            </Text>
          ),
        },
      ],
    },
    {
      bullet: 'XIV. ',
      content: (
        <>
          <Text style={styles.boldText}>Transmission of Information</Text>
          <Text style={styles.text}>
            You understand that while the Association will use its best
            endeavors to ensure that all information transmitted or received
            using the PSSLAI Digital Services is secure and cannot be accessed
            by unauthorized third parties, the Association does not warrant the
            security of any information transmitted by you using the PSSLAI
            Digital Services. Accordingly, you agree to accept the risk that any
            information transmitted or received using the PSSLAI Digital
            Services may be accessed by unauthorized third parties and you agree
            not to hold the Association liable for any such unauthorized access
            or any loss or damage suffered as a result thereof.
          </Text>
        </>
      ),
    },
    {
      bullet: 'XV.',
      content: <Text style={styles.boldText}>Liabilities</Text>,
      subContent: [
        {
          bullet: 'A.',
          content: (
            <Text style={styles.text}>
              You acknowledge that the Association makes no warranties or
              representations of any kind with respect to the PSSLAI Digital
              Services, whether express or implied, including but not limited to
              merchantability or fitness for a particular purpose. Neither the
              Association nor any other party involved in the creation,
              production or delivery of the PSSLAI Digital Services assumes any
              responsibilities with respect to your use thereof. No oral or
              written information or advice given by the Association or its
              employees shall create a warranty or in any way increase the scope
              of this warranty and you may not rely on any such information or
              advice.
            </Text>
          ),
        },
        {
          bullet: 'B.',
          content: (
            <Text style={styles.text}>
              To the extent permitted by applicable laws, the Association shall
              not be responsible or liable for any direct, incidental,
              consequential, indirect damages (including loss of profits, loss
              of opportunity, loss of savings, and business interruption) or
              special or exemplary damages (incurred or suffered by you or any
              other person) as a consequence of using the PSSLAI Digital
              Services).
            </Text>
          ),
        },
        {
          bullet: 'C.',
          content: (
            <Text style={styles.text}>
              Due to the nature of the PSSLAI Digital Services, the Association
              will not be responsible for any loss of or damage to your data,
              software, equipment, network access or other equipment used to
              access the PSSLAI Digital Services.
            </Text>
          ),
        },
        {
          bullet: 'D.',
          content: (
            <Text style={styles.text}>
              The Association shall not be held liable for any and all
              proceedings, claims, losses, damages or expenses, including legal
              costs, that may arise from any of the following conditions:
            </Text>
          ),
          subContent: [
            {
              bullet: '1.',
              content: (
                <Text style={styles.text}>
                  Any of your instructions verified by us or by the System as
                  conveyed with the use of your Member Codes, Member Number,
                  FTUC, OTP, Password and Username;
                </Text>
              ),
            },
            {
              bullet: '2.',
              content: (
                <Text style={styles.text}>
                  Any unauthorized use of the Association’s PSSLAI Digital
                  Services, your Member Codes, Member Number, FTUC, OTP,
                  Password and Username;
                </Text>
              ),
            },
            {
              bullet: '3.',
              content: (
                <Text style={styles.text}>
                  Any loss or damage caused by any access through your Member
                  Codes, Member Number, FTUC, OTP, Password and Username when
                  the same is prohibited, restricted, delayed or otherwise
                  affected by:
                </Text>
              ),
              subContent: [
                {
                  bullet: 'a.',
                  content: (
                    <Text style={styles.text}>
                      Any law or regulation of the country from where
                      Association’s PSSLAI Digital Services is accessed and/or
                      the terms and conditions prescribed by your internet
                      service or information service provider; or
                    </Text>
                  ),
                },
                {
                  bullet: 'b.',
                  content: (
                    <Text style={styles.text}>
                      Any law or regulation of any jurisdiction, regional or
                      international authority which governs any use of any
                      component of the Association’s PSSLAI Digital Services;
                    </Text>
                  ),
                },
                {
                  bullet: 'c.',
                  content: (
                    <Text style={styles.text}>
                      Any loss or damage caused by any act or omission of your
                      internet service, information service provider or network
                      provider;
                    </Text>
                  ),
                },
                {
                  bullet: 'd.',
                  content: (
                    <Text style={styles.text}>
                      Any loss or damage due to your interference or tampering
                      with, or alteration or misuse of, or amendment to, the
                      Association’s PSSLAI Digital Services.
                    </Text>
                  ),
                },
                {
                  bullet: 'e.',
                  content: (
                    <Text style={styles.text}>
                      Any communications facility malfunction that affects the
                      timeliness or accuracy of the instructions sent through
                      this facility.
                    </Text>
                  ),
                },
              ],
            },
          ],
        },
      ],
    },
    {
      bullet: 'XVI.',
      content: (
        <>
          <Text style={styles.boldText}>Proprietary Rights</Text>
          <Text style={styles.text}>
            You acknowledge that all proprietary rights and intellectual
            property rights in the PSSLAI Digital Services (including without
            limitation, the psslai.com website) belong to the Association at all
            times.
          </Text>
        </>
      ),
    },
    {
      bullet: 'XVII.',
      content: <Text style={styles.boldText}>International Use</Text>,
      subContent: [
        {
          bullet: 'A.',
          content: (
            <Text style={styles.text}>
              The use of the PSSLAI Digital Services outside of the Philippines
              is subject to: 1) the applicable rules and regulations of the
              Bangko Sentral ng Pilipinas (BSP) or any fiscal or exchange
              control requirements operating in the country where the
              transaction is effected or requested; and 2) the laws and
              regulations of the Philippines and the country where the
              transaction is effected or requested.
            </Text>
          ),
        },
        {
          bullet: 'B.',
          content: (
            <Text style={styles.text}>
              The maximum amount of a transaction and the purpose for which it
              is effected may be determined by the BSP and other relevant laws
              and regulations of the Philippines.
            </Text>
          ),
        },
        {
          bullet: 'C.',
          content: (
            <Text style={styles.text}>
              You hereby agree that you are using the PSSLAI Digital Services at
              your own initiative and are responsible for your compliance with
              local laws, rules and regulations.
            </Text>
          ),
        },
      ],
    },
    {
      bullet: 'XVIII.',
      content: (
        <>
          <Text style={styles.boldText}>Indemnity</Text>
          <Text style={styles.text}>
            You hereby agree to indemnify and keep the Association indemnified
            from and against any and all claims, losses, liabilities, cost and
            expenses (including but not limited to any legal fees) arising
            directly or indirectly or which may arise out of your breach or
            violation of these Terms and Conditions or any third party rights or
            your use or purported use of the PSSLAI Digital Services or due to
            the delay and/or failure of such PSSLAI Digital Services.
          </Text>
        </>
      ),
    },
    {
      bullet: 'XIX.',
      content: <Text style={styles.boldText}>Notices</Text>,
      subContent: [
        {
          bullet: 'A.',
          content: (
            <Text style={styles.text}>
              You hereby consent to all notices and other communications which
              concern the PSSLAI Digital Services or are required under these
              Terms and Conditions or may be given by the Association in any one
              of the following manners:
            </Text>
          ),
          subContent: [
            {
              bullet: '1.',
              content: (
                <Text style={styles.text}>
                  By ordinary post to your last address in the Association’s
                  records and such notification shall be deemed received two (2)
                  days after posting.
                </Text>
              ),
            },
            {
              bullet: '2.',
              content: (
                <Text style={styles.text}>
                  By electronic mail to your last known email address in the
                  Association's records and such notification shall be deemed
                  received twenty four (24) hours after sending.
                </Text>
              ),
            },
            {
              bullet: '3.',
              content: (
                <Text style={styles.text}>
                  By being displayed on the Association’s (Corporate, Branch and
                  Satellite Offices) premises and such notification shall be
                  deemed effective upon such display.
                </Text>
              ),
            },
            {
              bullet: '4.',
              content: (
                <Text style={styles.text}>
                  Broadcasting a message on the official PSSLAI website or other
                  official social media pages of the Association.
                </Text>
              ),
            },
            {
              bullet: '5.',
              content: (
                <Text style={styles.text}>
                  If notified to you in any other manner as the Association
                  deems fit.
                </Text>
              ),
            },
          ],
        },
        {
          bullet: 'B.',
          content: (
            <Text style={styles.text}>
              Except for concerns which may be or have already been addressed by
              the Association’s Member Care Group personnel, or as otherwise
              advised by them, all notices to the Association concerning the
              PSSLAI Digital Services and these Terms and Conditions shall be in
              writing, signed by you and sent to the Association at the
              following address or in such other way as the Association may
              notify you from time to time:
            </Text>
          ),
          subContent: [
            {
              bullet: '',
              content: (
                <>
                  <Text style={styles.textAddress}>
                    Membership Management Relations and Care Department (MMRCD){' '}
                  </Text>
                  <Text style={styles.textAddress}>
                    Public Safety Savings and Loan Association Inc.{' '}
                  </Text>
                  <Text style={styles.textAddress}>
                    G/F PSSLAI Corporate Office, 524 EDSA Northbound,{' '}
                  </Text>
                  <Text style={styles.text}>
                    Brgy. Socorro, Cubao, Quezon City{' '}
                  </Text>
                </>
              ),
            },
          ],
        },
        {
          bullet: 'C.',
          content: (
            <Text style={styles.text}>
              The Association shall endeavor to address any concern or complaint
              of the MEMBER which is properly brought to the Association’s
              knowledge within a reasonable time.
            </Text>
          ),
        },
      ],
    },
    {
      bullet: 'XX.',
      content: (
        <>
          <Text style={styles.boldText}>Waiver</Text>
          <Text style={styles.text}>
            You agree that the failure of the Association, whether continuous or
            otherwise, to exercise any rights, power, privileges, remedies or
            provisions of these Terms and Conditions or the Association’s
            failure or delay to insist on compliance with any of these clauses
            shall not constitute a waiver by the Association of any such rights,
            power, privileges, remedies or provisions of these Terms and
            Conditions.
          </Text>
        </>
      ),
    },
    {
      bullet: 'XXI.',
      content: (
        <>
          <Text style={styles.boldText}>Severability</Text>
          <Text style={styles.text}>
            You agree that if any undertakings and/or part of these Terms and
            Conditions are held to be invalid or unenforceable pursuant to
            applicable law, the validity, legality or unenforceability of the
            remaining provisions contained herein shall not in any way be
            affected or impaired and shall continue in full force and effect.
            The Association shall replace the invalid, illegal or unenforceable
            part/s with valid, legal and enforceable terms which most closely
            match the intent of the original provision thereby superseding the
            latter.
          </Text>
        </>
      ),
    },
    {
      bullet: 'XXII.',
      content: <Text style={styles.boldText}>Dispute and Inquiries</Text>,
      subContent: [
        {
          bullet: 'A.',
          content: (
            <Text style={styles.text}>
              If you have any inquiries or require any assistance, you may call
              the Association’s Member Care Group at (02) 8705-2100,
              0998-9622081, 0925-5457493, 0917-8567443.
            </Text>
          ),
        },
        {
          bullet: 'B.',
          content: (
            <Text style={styles.text}>
              In the event that you have any complaints and/or disputes arising
              from the PSSLAI Digital Services and these Terms and Conditions,
              please refer the matter to the Association at the address stated
              above specifying the nature of your complaint and/or dispute. You
              hereby agree to try to settle the matter amicably.
            </Text>
          ),
        },
      ],
    },

    {
      bullet: 'XXIII.',
      content: (
        <>
          <Text style={styles.boldText}>Governing Law</Text>
          <Text style={styles.text}>
            The Terms and Conditions will be governed by the Laws of the
            Philippines and all disputes shall be resolved by the Philippine
            courts.
          </Text>
        </>
      ),
    },
    {
      bullet: 'XXIV.',
      content: (
        <>
          <Text style={styles.boldText}>
            Transfer, Disclosure and Retrieval of Information
          </Text>
          <Text style={styles.text}>
            You hereby authorize us to obtain and verify any information about
            you as we deem fit in our absolute discretion. You authorize the
            transfer, disclosure and communication of any information relating
            to you (including information PSSLAI obtains from third parties such
            as your Branch of Service, any credit bureau, banking or credit
            industry association, credit information service providers, credit
            and loan providers), your Account or any information thereon, or any
            of your properties or investments with PSSLAI, from PSSLAI to and
            between offices, branches, agents and representatives of PSSLAI and
            third parties selected by any of them or PSSLAI (collectively
            referred to as the "Receiving and Disclosing Parties"), wherever
            situated, for use (including for use in connection with the
            provision of any service or product to you or relating to your
            Accounts, properties or investments, and for data processing and
            storage, customer satisfaction surveys, product and service offers
            made through mail, email, fax, SMS or telephone, anti-money
            laundering, sanctions, review and reporting, statistical and risk
            analysis and risk management purposes). In addition to the
            foregoing, PSSLAI or any of the Receiving and Disclosing Parties may
            transfer and disclose any such information as may be required by any
            law, regulation, court, regulator or legal process. Without
            prejudice to the generality of the foregoing, where PSSLAI is a
            user, member of, or subscriber for the information sharing services
            or activities of, any credit bureau, banking or credit industry
            association, credit information service provider, credit and loan
            providers, financial institutions, (individually and collectively
            referred to as "Credit Institutions), or party to any outsourcing
            agreement (the "Outsourcing Agreement") , you expressly authorize
            (a) PSSLAI to transfer and disclose to any such Credit Institutions
            or PSSLAI’s counterparty in an Outsourcing Agreement (the
            "Outsourcing Partner"); and (b) any such Credit Institutions to
            transfer and disclose to any fellow member or subscriber, any
            information (and updates or corrections), whether positive or
            negative, relating to you and/or your Accounts, properties or
            investments (and for such purposes). You acknowledge that such
            information shall include basic credit data and negative information
            under the Credit Information System Act (R.A. 9510), i.e., personal
            information or demographics, monthly account information and account
            performance data, including account history and account status.
            Where you have existing unsecured credit facilities with PSSLAI, you
            agree and consent to PSSLAI reviewing and adjusting the credit limit
            of such unsecured facilities in its absolute discretion in
            accordance with its credit and risk management policies. The
            foregoing constitutes your written consent for any transfer and
            disclosure of information relating to you, your Accounts, properties
            and investments to, between and among PSSLAI, the Receiving and
            Disclosing Parties, Credit Institutions or Outsourcing Partner for
            any of the purposes indicated above or under applicable law,
            regulation, court, regulator or legal process. You agree to hold
            PSSLAI free and harmless from any liability that may arise from any
            transfer, disclosure or storage of information relating to you, your
            Accounts, properties or investments. If you do not wish to receive
            telephone and/or mail solicitations, you may notify us in writing or
            by calling PSSLAI’s Member Care Group at membercare@psslai.com or
            via the numbers: (02) 8705-2100, 0998-9622081, 0925-5457493,
            0917-8567443.
          </Text>
        </>
      ),
    },
  ];

  const bulletListB = [
    {
      bullet: '1.',
      content: (
        <>
          <Text style={styles.boldText}>Definition</Text>
          <Text style={styles.text}>
            “ATM” or “Automated Teller Machine” shall mean the cash dispensing
            machines that are linked to the Bancnet and Visa networks.
          </Text>
          <Text style={styles.text}>
            “Cardholder” shall refer to the person in whose name the Card is
            issued. Each Cardholder can only avail of one Card.
          </Text>
          <Text style={styles.text}>
            “Personal Identification Number” refers the numerical code used in
            electronic financial transactions that is known only to the
            Cardholder.
          </Text>
          <Text style={styles.text}>
            “PSSLAI” refers to the Public Safety Savings and Loan Association
            Inc.
          </Text> 
          <Text style={styles.text}>
            “PSSLAI Member” refers to individuals under the public safety sector
            such as the Philippine National Police (PNP) and Bureau of Fire
            Protection (BFP) and their families.
          </Text>
          <Text style={styles.text}>
            “PSSLAI Prepaid Card” or “Card” shall mean the card issued by the
            Union Bank of the Philippines (“UBP”), through its EON Banking
            Group, to a qualified PSSLAI Member and available in select PSSLAI
            accredited branches and satellite offices. It is a reloadable
            multi-purpose value card used for crediting of the member’s loans.
            Funds received and maintained in the Card shall not earn interest
            and are withdrawable via ATMs. Each Card is valid for five (5) years
            from activation, unless earlier terminated by UBP or voluntarily
            cancelled or returned by Cardholder.
          </Text>
        </>
      ),
    },
    {
      bullet: '2.',
      content: (
        <>
          <Text style={styles.boldText}>Activation</Text>
          <Text style={styles.text}>
            The Card is activated through the PSSLAI Portal. Cardholder shall
            nominate his/her PIN once he/she activates the card, and Cardholder
            agrees to not disclose the nominated PIN to anyone. Once activated,
            withdrawals and balance inquiries via ATMs can be made only by using
            a correct PIN. Transactions processed through the Card shall be
            conclusively presumed to be with full knowledge and authority of the
            Cardholder. Cardholder shall hold PSSLAI free and harmless from any
            and all losses, damages or liabilities that may arise as a result of
            his/her disclosure of PIN.
          </Text>
        </>
      ),
    },
    {
      bullet: '3.',
      content: (
        <>
          <Text style={styles.boldText}>Card Usage / Limits</Text>
          <Text style={styles.text}>
            The Card may be used by the Cardholder to (a) send money via the
            PSSLAI Portal; (b) receive money via loan disbursement and cash-in;
            (c) withdraw cash via ATMs; (d) make purchases via online and
            point-of-sale transactions; and (e) inquire Card balance via ATMs
            and through the PSSLAI Portal. Cardholder may maintain more than One
            Million Pesos (PhP1,000,000.00) balance but transactions will be
            capped at PhP 1,000,000.00 monthly. Any balance in excess will only
            be reflected in the account’s current balance and may not be used.
            Cardholder may withdraw a total amount of Fifty Thousand Pesos (PhP
            50,000.00) daily.
          </Text>
          <Text style={styles.text}>
            Cardholder can top-up the Card via UBP branches and select PeraHub
            and 7-Eleven branches. There shall be no limit on top up amount via
            UBP and PeraHub branches, while top-ups made via 7-Eleven branches
            are capped at PhP 500.00. The Cardholder agrees to be responsible
            for monitoring the funds in his/her Card and hereby holds UBP free
            and harmless from any and all losses, claims, liabilities or damages
            which the Cardholder may suffer resulting from unsuccessful
            transactions through POS terminals or online.
          </Text>
        </>
      ),
    },
    {
      bullet: '4.',
      content: (
        <>
          <Text style={styles.boldText}>Fees, Rates, and Other Charges</Text>
          <Text style={styles.text}>
            Cardholder acknowledges UBP’s right to impose fees and charges for
            the use of the Card and other related services such as, but not
            limited to, Balance Inquiry and ATM Withdrawal. The Cardholder
            agrees to pay such service fees and charges presently imposed or may
            in the future be imposed by UBP at its option. The Schedule of
            Applicable Fees is provided below. UBP reserves the right to change
            the amount of said fees and charges as may be necessary from time to
            time.
          </Text>
          <View style={styles.tableWrapper}>
            <Table borderStyle={styles.table}>
              <Row data={['Service', 'Fee']} textStyle={styles.textHeadTable} />
              <Rows
                data={[
                  ['ATM Balance Inquiry (Local)', 'PhP 5.00'],
                  ['ATM Cash Withdrawal (Local)', 'PhP 50.00'],
                  ['ATM Balance Inquiry (Intl)', '$ 1.00'],
                  ['ATM Cash Withdrawal (Intl)', '$ 3.00'],
                  ['Fund Transfer (UBP-PSSLAI)', 'PhP 10.00'],
                  ['Fund Transfer (To Other Local Banks)', 'PhP 15.00'],
                  ['Cash-In', 'PhP 10.00'],
                  ['Annual Fee', 'Free'],
                ]}
                textStyle={styles.textTable}
              />
            </Table>
          </View>
        </>
      ),
    },
    {
      bullet: '5.',
      content: (
        <>
          <Text style={styles.boldText}>Privacy / Confidentiality</Text>
          <Text style={styles.text}>
            Cardholder gives his/her consent to and authorizes UBP, its
            directors, officers, employees, agents and representatives to
            perform any and all of the following:
          </Text>
        </>
      ),
      subContent: [
        {
          bullet: '(a)',
          content: (
            <Text style={styles.text}>
              collect, use, store, consolidate, and process information relating
              to Cardholder and/or his/her account;
            </Text>
          ),
        },
        {
          bullet: '(b)',
          content: (
            <Text style={styles.text}>
              outsource the processing of information to service providers,
              whether within or outside the Philippines;
            </Text>
          ),
        },
        {
          bullet: '(c)',
          content: (
            <Text style={styles.text}>
              verify or validate Cardholder information from any and all sources
              and in any reasonable manner;
            </Text>
          ),
        },
        {
          bullet: '(d)',
          content: (
            <Text style={styles.text}>
              disclose and share of information Cardholder has provided in
              relation to this Card to any judicial, governmental, supervisory,
              regulatory or equivalent body of the Philippines and such other
              persons or entities that PSSLAI may deem necessary or appropriate
              to facilitate the above-stated purposes or those that may relate
              to or arise there from, as and when required by the circumstances.
            </Text>
          ),
        },
      ],
    },
    {
      bullet: '',
      content: (
        <Text style={styles.text}>
          For the above purposes, Cardholder expressly waives his/her rights to
          confidentiality under applicable confidentiality and data privacy laws
          of the Philippines and other jurisdiction and agree to hold UBP free
          and harmless from any liabilities, claims, damages and suits of
          whatever kind and nature, that may arise in connection with the
          implementation and consents given by Cardholder for the above
          purposes. The foregoing consent shall continue for the duration of,
          and shall survive the termination of, the use of the Card.
        </Text>
      ),
    },
    {
      bullet: '6.',
      content: (
        <>
          <Text style={styles.boldText}>Limitation of Liability</Text>
          <Text style={styles.text}>
            Cardholder agrees to hold UBP free and harmless from any loss,
            liabilities, claims, costs, damages and suits of whatever kind and
            nature, that may arise in connection with use or misuse of the Card,
            the implementation and compliance.
          </Text>
        </>
      ),
    },
    {
      bullet: '7.',
      content: (
        <>
          <Text style={styles.boldText}>Lost/Stolen Card</Text>
          <Text style={styles.text}>
            Lost or stolen Card shall be immediately reported by the Cardholder
            to UBP or to any PSSLAI accredited branches and satellite offices
            together with account details. Cards reported stolen or lost shall
            automatically be blocked, and the Cardholder holds UBP free and
            harmless against any liability, costs, damages or causes of action
            related to such action by UBP. All transactions undertaken with the
            Card prior to such notification to UBP or PSSLAI shall be presumed
            to have been personally done by or with authority of the Cardholder,
            and, pursuant to the sole responsibility of the Cardholder for the
            care and safety of the Card under section 2 hereof, shall be binding
            upon the Cardholder. Upon compliance with the requirements
            above-mentioned and provided that the lost/stolen Card is not
            expired, deactivated, nor terminated pursuant to these Terms and
            Conditions, a new Card will be issued to the Cardholder upon payment
            of One Hundred Fifty Pesos (PhP 150.00) fee via any PSSLAI
            accredited branches and satellite offices.
          </Text>
        </>
      ),
    },
    {
      bullet: '8.',
      content: (
        <>
          <Text style={styles.boldText}>Warranties</Text>
          <Text style={styles.text}>
            All warranties, conditions and other terms implied by law or statute
            are excluded from this Terms. UBP does not warrant that the Card and
            the services associated with it will be uninterrupted, timely,
            secure, or free from error and that it will be honored at all times.
          </Text>
        </>
      ),
    },
    {
      bullet: '9.',
      content: (
        <>
          <Text style={styles.boldText}>Termination/Deactivation</Text>
          <Text style={styles.text}>
            The Card shall be deactivated upon written request by the
            Cardholder. The Card shall also automatically expire after five (5)
            years in the event of prolonged period of inactivity. Cardholder
            acknowledges and agrees that UBP may opt to terminate the
            Cardholder’s privilege and initiate an investigation without notice
            whenever it deems appropriate and necessary upon reasonable
            suspicion of fraud, irregularity, or illegal activity.
          </Text>
        </>
      ),
    },
    {
      bullet: '10.',
      content: (
        <>
          <Text style={styles.boldText}>Compliance with Existing Laws</Text>
          <Text style={styles.text}>
            Cardholder hereby agrees that his/her use of the Card does not and
            will not violate Anti-Money Laundering Law and its amendments, other
            applicable laws, rules or regulations. Cardholder further agrees to
            hold PSSLAI, its officers, employees and representatives free and
            harmless and indemnified from any liabilities, damages, suits or
            causes of action whatsoever which may arise from any violation of
            said laws, rules and regulations.
          </Text>
        </>
      ),
    },
    {
      bullet: '11. ',
      content: (
        <>
          <Text style={styles.boldText}>Amendment and Cancellation</Text>
          <Text style={styles.text}>
            UBP may amend or change the terms of this Agreement at any time. The
            Cardholder will be notified of any change in the manner provided by
            applicable law prior to the effective date of the change. However,
            if the change is made for security purposes and to prevent further
            losses, UBP can implement such change without prior notice.
          </Text>
        </>
      ),
    },
    {
      bullet: '12.',
      content: (
        <>
          <Text style={styles.boldText}>Assignment/transfer of Rights</Text>
          <Text style={styles.text}>
            The Card and the Cardholder’s obligations under this Agreement
            cannot be assigned. All rights and interests of UBP under these
            Terms and Conditions may be assigned or transferred by UBP without
            the consent of the Cardholder.
          </Text>
        </>
      ),
    },
    {
      bullet: '13.',
      content: (
        <>
          <Text style={styles.boldText}>Non-waiver of Rights</Text>
          <Text style={styles.text}>
            No failure or delay on the part of UBP to exercise any of its rights
            hereunder shall operate as a waiver thereof. No waiver by UBP of any
            of its rights hereunder shall be deemed to have been made unless
            made in writing and signed by its duly authorized representative(s).
          </Text>
        </>
      ),
    },
    {
      bullet: '14.',
      content: (
        <>
          <Text style={styles.boldText}>Governing Law</Text>
          <Text style={styles.text}>
            These terms shall be governed and construed by in accordance with
            Philippine laws including matters on enforcement and performance.
            All suits shall be filed with the proper courts of Pasig City, Metro
            Manila only, to the exclusion of all other courts.
          </Text>
        </>
      ),
    },
    {
      bullet: '15.',
      content: (
        <>
          <Text style={styles.boldText}>Customer Support</Text>
          <Text style={styles.text}>
            Any complaint regarding the Card or its use or both shall be
            communicated to Union Bank of the Philippines at
            heretohelp@eonbankph.com. For funding-related concerns from loan
            availments, please contact PSSLAI at customercare@psslai.com.
          </Text>
        </>
      ),
    },
  ];

  return (
    <>
      <Header title="Back" onBackPress={() => navigation.goBack()} />
      <ScrollView showsVerticalScrollIndicator={false}>
        <Container style={styles.container}>
          <Text h1 h1Style={styles.title}>
            Terms and Conditions
          </Text>
          <Text h2 h2Style={styles.header}>
            Electronic Services (Digital Services) Terms and Conditions
          </Text>
          <Text style={styles.text}>
            The following Terms and Conditions apply to the access and use of
            PSSLAI’s Digital Services (Digital Services) such as but not limited
            to the iTrack (accessed thru any type of mobile phone), Bilis Mobile
            (accessed thru Mobile application software) and Bilis Online
            (accessed thru internet and mobile browsers). By accessing or using
            any of the PSSLAI Digital Services, the member hereby agrees to be
            bound by these Terms and Conditions without limitation or
            qualification, as well as to assume the risks inherent to conducting
            any transaction via the Internet. PSSLAI encourages members to store
            or print a copy of these Terms and Conditions for records purposes.
          </Text>

          <BulletItems list={bulletListA} bulletWidth={40} />

          {/* <Text h2 h2Style={styles.headerVisaCard}>
            PSSLAI Visa Card Terms and Conditions
          </Text>
          <BulletItems list={bulletListB} bulletWidth={40} />
          <Text style={styles.textBottom}>
            THE TERMS AND CONDITIONS IN THIS AGREEMENT ARE ACCEPTED AND BINDING
            ON THE CARDHOLDER.
          </Text> */}
        </Container>
      </ScrollView>
    </>
  );
};

export default TermsAndConditions;
