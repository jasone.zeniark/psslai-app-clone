import React from 'react';
import { ScrollView } from 'react-native';
import { Text } from 'react-native-elements';

import Container from '../../../globals/layout/Container';
import Header from '../../../globals/header/CommonHeader';
import BulletItems from '../../../globals/bullet/bulletItems';

import styles from './Styles';

const PrivacyPolicy = ({ navigation }) => {
  const bulletListPrivacyZ = [
    {
      bullet: '',
      content: <></>,
      subContent: [
        {
          bullet: '1 )',
          content: (
            <Text style={styles.text}>
              1. About an individual’s race, ethnic origin, marital status, age,
              color, and religious, philosophical or political affiliations;
            </Text>
          ),
        },
        {
          bullet: '2 )',
          content: (
            <Text style={styles.text}>
              2. About an individual’s health, education, genetic or sexual life
              of a person, or to any proceeding for any offense committed or
              alleged to have been committed by such person, the disposal of
              such proceedings, or the sentence of any court in such
              proceedings;
            </Text>
          ),
        },
        {
          bullet: '3 )',
          content: (
            <Text style={styles.text}>
              3. Issued by government agencies peculiar to an individual which
              includes, but not limited to, social security numbers, previous or
              current health records, licenses or its denials, suspension or
              revocation, and tax returns; and
            </Text>
          ),
        },
        {
          bullet: '4 )',
          content: (
            <Text style={styles.text}>
              4. Specifically established by an executive order or an act of
              Congress to be kept classified.
            </Text>
          ),
        },
      ],
    },
  ];
  const bulletListPrivacyA = [
    {
      bullet: '',
      content: <></>,
      subContent: [
        {
          bullet: '1 )',
          content: (
            <Text style={styles.text}>
              Inquiring about a loan application status;
            </Text>
          ),
          subContent: [
            {
              bullet: '•',
              content: (
                <Text style={styles.text}>
                  Inquiring about a loan application status;
                </Text>
              ),
            },
            {
              bullet: '•',
              content: (
                <Text style={styles.text}>
                  Signing up for online or mobile applications and applying for
                  an Association Visa Card (for members only);{' '}
                </Text>
              ),
            },
            {
              bullet: '•',
              content: (
                <Text style={styles.text}>
                  Sending us any questions/concerns thru the Contact Us page;
                </Text>
              ),
            },
            {
              bullet: '•',
              content: (
                <Text style={styles.text}>
                  Expressing an interest to join our Team.
                </Text>
              ),
            },
          ],
        },
        {
          bullet: '2 )',
          content: (
            <Text style={styles.text}>
              Information we collect about you when you contact us, answer
              customer surveys, or view our online advertisements via our
              official communication channels such as website, email, social
              media page, member care hotlines, instant messaging application,
              and such others as we may hereafter launch.
            </Text>
          ),
        },
        {
          bullet: '3 )',
          content: (
            <Text style={styles.text}>
              Information we collect about you from other sources, such as
              commercially or publicly available sources.
            </Text>
          ),
        },
        {
          bullet: '4 )',
          content: (
            <Text style={styles.text}>
              Information where you have authorized us to gather data regarding
              any application or transaction you have with us.
            </Text>
          ),
        },
        {
          bullet: '5 )',
          content: (
            <Text style={styles.text}>
              5) All the information PSSLAI collects about you may be combined
              to improve our communications with you, and to provide excellent
              services.{' '}
            </Text>
          ),
        },
      ],
    },
  ];
  const bulletListPrivacyB = [
    {
      bullet: '',
      content: <></>,
      subContent: [
        {
          bullet: '•',
          content: <Text style={styles.text}>Member’s name</Text>,
        },
        {
          bullet: '•',
          content: <Text style={styles.text}>Qualifier</Text>,
        },
        {
          bullet: '•',
          content: <Text style={styles.text}>Contact Number</Text>,
        },
        {
          bullet: '•',
          content: <Text style={styles.text}>E-mail address</Text>,
        },
      ],
    },
  ];

  const bulletListPrivacyC = [
    {
      bullet: '',
      content: <></>,
      subContent: [
        {
          bullet: '•',
          content: <Text style={styles.text}>Name</Text>,
        },
        {
          bullet: '•',
          content: <Text style={styles.text}>Date of Birth</Text>,
        },
        {
          bullet: '•',
          content: <Text style={styles.text}>Birthplace</Text>,
        },
        {
          bullet: '•',
          content: <Text style={styles.text}>Civil Status</Text>,
        },
        {
          bullet: '•',
          content: <Text style={styles.text}>Gender</Text>,
        },
        {
          bullet: '•',
          content: <Text style={styles.text}>Nationality</Text>,
        },
        {
          bullet: '•',
          content: <Text style={styles.text}>Mobile Number</Text>,
        },
        {
          bullet: '•',
          content: <Text style={styles.text}>E-mail Address</Text>,
        },
        {
          bullet: '•',
          content: <Text style={styles.text}>House Address</Text>,
        },
        {
          bullet: '•',
          content: <Text style={styles.text}>Employment details as:</Text>,
          subContent: [
            {
              bullet: '•',
              content: <Text style={styles.text}>Source of Funds</Text>,
            },
            {
              bullet: '•',
              content: <Text style={styles.text}>Business Name</Text>,
            },
            {
              bullet: '•',
              content: <Text style={styles.text}>Employment Address</Text>,
            },
          ],
        },
        {
          bullet: '•',
          content: <Text style={styles.text}>TIN</Text>,
        },
        {
          bullet: '•',
          content: <Text style={styles.text}>ID Photo</Text>,
        },
      ],
    },
  ];
  const bulletListPrivacyD = [
    {
      bullet: '',
      content: <></>,
      subContent: [
        {
          bullet: '•',
          content: <Text style={styles.text}>Name</Text>,
        },
        {
          bullet: '•',
          content: <Text style={styles.text}>Contact Number</Text>,
        },
        {
          bullet: '•',
          content: <Text style={styles.text}>E-mail address</Text>,
        },
      ],
    },
  ];
  const bulletListPrivacyE = [
    {
      bullet: '',
      content: <></>,
      subContent: [
        {
          bullet: '•',
          content: <Text style={styles.text}>Name</Text>,
        },
        {
          bullet: '•',
          content: <Text style={styles.text}>Contact Number</Text>,
        },
        {
          bullet: '•',
          content: <Text style={styles.text}>E-mail</Text>,
        },
        {
          bullet: '•',
          content: (
            <Text style={styles.text}>Curriculum Vitae (as an attachment)</Text>
          ),
        },
      ],
    },
  ];
  const bulletListPrivacyEA = [
    {
      bullet: '',
      content: <></>,
      subContent: [
        {
          bullet: '•',
          content: (
            <Text style={styles.text}>
              Primary Member’s Name, Present Address, Permanent Address, Nature
              of Work, Name of Employer, Nature of Business if Self-Employed,
              Nationality, Birth Date, Birth Place, Sex, Civil Status, TIN, Tel.
              No., Cel. No., Email, Source of Funds
            </Text>
          ),
        },
        {
          bullet: '•',
          content: <Text style={styles.text}>Member Number</Text>,
        },
        {
          bullet: '•',
          content: <Text style={styles.text}>CCTV Footage</Text>,
        },
        {
          bullet: '•',
          content: (
            <Text style={styles.text}>
              Voice recordings of our conversations with you
            </Text>
          ),
        },
      ],
    },
  ];
  const bulletListPrivacyF = [
    {
      bullet: '1. ',
      content: (
        <Text style={styles.text}>
          PSSLAI uses the information we collect in this website and/or other
          variety of sources for the following purposes only:
        </Text>
      ),
      subContent: [
        {
          bullet: '•',
          content: <Text style={styles.text}>Membership Application</Text>,
        },
        {
          bullet: '•',
          content: (
            <Text style={styles.text}>Loan Application and Processing</Text>
          ),
        },
        {
          bullet: '•',
          content: (
            <Text style={styles.text}>
              Collection or Disbursement Transactions
            </Text>
          ),
        },
        {
          bullet: '•',
          content: (
            <Text style={styles.text}>
              Human Resource and Third Party Requirements
            </Text>
          ),
        },
        {
          bullet: '•',
          content: (
            <Text style={styles.text}>
              Reach out to you regarding products and services information,
              including offers and promotions
            </Text>
          ),
        },
        {
          bullet: '•',
          content: (
            <Text style={styles.text}>
              Comply with our operational, audit, administrative, credit and
              risk management processes, policies and procedures, the terms and
              conditions governing our products, services, facilities and
              channels, the Bangko Sentral ng Pilipinas rules and regulations,
              legal and regulatory requirements of government regulators,
              judicial, supervisory bodies, tax authorities or courts of
              competent jurisdiction, as the same may be amended or supplemented
              from time to time.{' '}
            </Text>
          ),
        },
        {
          bullet: '•',
          content: (
            <Text style={styles.text}>
              Perform other such activities permitted by law or with your
              consent.
            </Text>
          ),
        },
      ],
    },
    {
      bullet: '2. ',
      content: <Text style={styles.text}>PSSLAI shares your information:</Text>,
      subContent: [
        {
          bullet: '•',
          content: (
            <Text style={styles.text}>
              With our carefully selected business partners for co-promotions or
              other joint programs.
            </Text>
          ),
        },
        {
          bullet: '•',
          content: (
            <Text style={styles.text}>
              With our third-party service providers who perform PSSLAI business
              operations on our behalf.
            </Text>
          ),
          subContent: [
            {
              bullet: '•',
              content: <Text style={styles.text}>LBC Express, Inc.</Text>,
            },
            {
              bullet: '•',
              content: <Text style={styles.text}>UnionBank EON</Text>,
            },
            {
              bullet: '•',
              content: (
                <Text style={styles.text}>M Lhuillier Financial Services</Text>
              ),
            },
            {
              bullet: '•',
              content: (
                <Text style={styles.text}>
                  Rizal Commercial Banking Corporation
                </Text>
              ),
            },
            {
              bullet: '•',
              content: (
                <Text style={styles.text}>Philippine National Bank</Text>
              ),
            },
          ],
        },
        {
          bullet: '•',
          content: (
            <Text style={styles.text}>
              With our Account Representatives, Agents or related partners who
              work on behalf of PSSLAI for the purpose of communicating with you
              or protecting the rights of the Association to include enforcing
              its Terms and Conditions.
            </Text>
          ),
        },
        {
          bullet: '•',
          content: (
            <Text style={styles.text}>
              With our Account Representatives or related partners who work on
              behalf of PSSLAI and our business partners to send you joint
              communications that we hope you find interest.
            </Text>
          ),
        },
        {
          bullet: '•',
          content: (
            <Text style={styles.text}>
              To protect and defend PSSLAI (including enforcing our Terms and
              Conditions).
            </Text>
          ),
        },
        {
          bullet: '•',
          content: (
            <Text style={styles.text}>
              When required by law and/or government authorities such as BSP,
              NPC, BIR, etc.
            </Text>
          ),
        },
        {
          bullet: '•',
          content: (
            <Text style={styles.text}>
              If you do not wish to provide your personal information to us, we
              are not be able to provide the services you request, or to tell
              you about other services being offered by PSSLAI.
            </Text>
          ),
        },
      ],
    },
  ];

  return (
    <>
      <Header title="Back" onBackPress={() => navigation.goBack()} />
      <ScrollView showsVerticalScrollIndicator={false}>
        <Container style={styles.container}>
          <Text h1 h1Style={styles.title}>
            Privacy Policy
          </Text>

          <Text style={styles.boldText}>XXV. Privacy Notice</Text>
          <Text style={styles.text}>
            Public Safety Savings and Loan Association, Inc. (“PSSLAI”,
            “Association”, “We”, Us”) respect and uphold your data privacy
            rights. We ensure that all personal data collected and processed are
            in adherence to the general principles of transparency, legitimate
            purpose, proportionality, and in a manner compliant with the
            requirements of the Data Privacy Act of 2012, its Implementing Rules
            and Regulations, and other relevant policies, including issuances of
            the National Privacy Commission.
          </Text>

          <Text style={styles.boldTextUnderlined}>Scope of this Notice</Text>
          <Text style={styles.text}>
            This Notice describes the overall practices of PSSLAI with respect
            to our processes and content which are made available through our
            network of websites, our online and mobile applications, and social
            media pages (collectively referred to as “websites”). This Notice
            applies to individuals who interact with PSSLAI through this
            website, and/or apply for and avail of our products and services
            such as loans, investments, savings and financial needs when you
            interact with our employees, authorized representatives, agents and
            service providers. This Notice describes how your Personal Data are
            collected, used, processed, and protected by PSSLAI. It also tells
            you how you can exercise your rights as a data subject.
          </Text>

          <Text style={styles.boldTextUnderlined}>Our Privacy Practices</Text>
          <Text style={styles.text}>
            Public Safety Savings and Loan Association, Inc. (“PSSLAI”,
            “Association”, “We”, Us”) respect and uphold your data privacy
            rights. We ensure that all personal data collected and processed are
            in adherence to the general principles of transparency, legitimate
            purpose, proportionality, and in a manner compliant with the
            requirements of the Data Privacy Act of 2012, its Implementing Rules
            and Regulations, and other relevant policies, including issuances of
            the National Privacy Commission.
          </Text>
          <Text style={styles.text}>
            When you access our websites and/or availing of our services, you
            acknowledge and agree that your information may be transferred to
            and processed in the Philippines following the Philippines’ legal
            and regulatory standards for data protection.{' '}
          </Text>

          <Text style={styles.boldTextUnderlined}>PSSLAI Services</Text>
          <Text style={styles.text}>
            PSSLAI, offers loans with lowest interest rates in the industry
            while simultaneously integrating a wide array of investment
            opportunities with high income on savings and investment products
            for public safety personnel.
          </Text>

          <Text style={styles.boldTextUnderlined}>
            Information PSSLAI Collects
          </Text>
          <Text style={styles.text}>
            PSSLAI collects information thru this website or from a variety of
            sources, including but not limited to:
          </Text>
          <BulletItems list={bulletListPrivacyA} bulletWidth={20} />

          <Text style={styles.boldText}>Personal Information collected</Text>
          <Text style={styles.boldText}>Thru Loan Application Inquiry</Text>
          <BulletItems list={bulletListPrivacyB} bulletWidth={20} />

          <Text style={styles.boldText}>
            Thru Visa card application (Default - Same day / Personalized) for
            members only
          </Text>
          <BulletItems list={bulletListPrivacyC} bulletWidth={20} />

          <Text style={styles.text}>
            <Text style={styles.boldText}>Note: </Text>
            These personal data are pre-populated fields and displays once the
            Member confirms the application for Visa Card.
          </Text>

          <Text style={styles.boldText}>
            Thru How can we help you? - Contact Us
          </Text>
          <BulletItems list={bulletListPrivacyD} bulletWidth={20} />

          <Text style={styles.boldText}>Contact us – Careers</Text>
          <BulletItems list={bulletListPrivacyE} bulletWidth={20} />

          <Text style={styles.boldText}>
            Other information from a variety of sources
          </Text>
          <BulletItems list={bulletListPrivacyEA} bulletWidth={20} />

          <Text style={styles.boldTextUnderlined}>
            Uses/Information Sharing:
          </Text>
          <BulletItems list={bulletListPrivacyF} bulletWidth={20} />

          <Text style={styles.boldTextUnderlined}>
            Data Protection Measures:
          </Text>
          <Text style={styles.text}>
            To protect your personal information from unauthorized access and
            use, the Association implements information security measures that
            safeguards and secures personal data. The Association also maintain
            physical, electronic and procedural security to protect this
            information and limit access to information to those employees for
            whom access is authorized.
          </Text>

          <Text style={styles.boldTextUnderlined}>
            Security Measures for the Protection of Personal Data
          </Text>
          <Text style={styles.text}>
            PSSLAI implements information security measures to ensure the
            availability, confidentiality and integrity of personal data.
          </Text>

          <Text style={styles.text}>Organization Security Measures</Text>
          <Text style={styles.text}>
            Appointed Data Privacy Officer (DPO) is responsible for data
            protection law, monitor compliance of the organization and contact
            point for requests from individuals about the processing of their
            personal data.
          </Text>
          <Text style={styles.text}>Physical Security Measures</Text>
          <Text style={styles.text}>
            PSSLAI areas for processing personal data, the data center included
            are secured to control the risks of unauthorized disclosure,
            unauthorized change, loss, theft and other incidents relating to
            unauthorized access by means of deploying security personnel and
            installation of CCTV where personal information are being processed.
          </Text>
          <Text style={styles.text}>Technical Security Measures</Text>
          <Text style={styles.text}>
            PSSLAI ensures proper and enough safeguards to secure the processing
            of personal data, including firewalls, anti-virus and encryption and
            multi-factor authentication processes.
          </Text>

          <Text style={styles.boldTextUnderlined}>
            Location, Storage and Retention
          </Text>
          <Text style={styles.text}>
            Your Personal Information collected from this website is stored and
            retained in PSSLAI Data Center located at Quezon City, Philippines
            for a period of time in accordance with Association policies. The
            facilities are equipped and monitored with high security controls in
            terms of physical and technical/electronic access. Retention of
            personal data shall be up to its intended purpose only, taking into
            consideration the retention period of documents as per Association’s
            policies and as per mandated by laws, and other rules applicable by
            other regulations issued (BSP Policies, rules according to
            Anti-Monetary Laundering Act or AMLA, and other policies issued by
            the other government agencies).
          </Text>

          <Text style={styles.boldTextUnderlined}>
            Rights of the Data Subject
          </Text>
          <Text style={styles.text}>
            <Text style={styles.boldText}>Right to be Informed.</Text> As our
            data subject, you will be informed whether personal data pertaining
            to you shall be, are being, or have been processed, including if it
            should be used in automated decision-making and/or profiling.
          </Text>
          <Text style={styles.text}>
            <Text style={styles.boldText}>Right to Object.</Text> The data
            subject has the right to object to the processing of his/her
            personal data, including processing for direct marketing, automated
            processing or profiling.
          </Text>
          <Text style={styles.text}>
            <Text style={styles.boldText}>Right to Access.</Text> The data
            subject has the right to reasonable access to his / her personal
            data upon request with due process and approval.
          </Text>
          <Text style={styles.text}>
            <Text style={styles.boldText}>Right to Rectification.</Text> The
            data subject has the right to dispute the inaccuracy or error in the
            personal data and have the personal information controller correct
            it immediately and accordingly, unless the request is vexatious or
            otherwise unreasonable.
          </Text>
          <Text style={styles.text}>
            <Text style={styles.boldText}>Right to Erasure or Blocking.</Text>{' '}
            The data subject shall have the right to suspend, withdraw or order
            the blocking, removal or destruction of his/her personal data from
            the personal information controller’s filing system.
          </Text>
          <Text style={styles.text}>
            <Text style={styles.boldText}>Right to Damages.</Text> The data
            subject shall be indemnified for any damages sustained due to such
            inaccurate, incomplete, outdated, false, unlawfully obtained or
            unauthorized use of personal data, taking into account any violation
            of his or her rights and freedoms as data subject.
          </Text>

          <Text style={styles.boldTextUnderlined}>Your Rights:</Text>
          <Text style={styles.text}>
            PSSLAI allows you to exercise your rights in accordance to the DPA
            Law.
          </Text>
          <Text style={styles.text}>
            You have the right to object before we process any of your requests.
            Once your request is successful you can access, review and modify
            for any inaccuracy or error in your personal data and may request a
            copy of your personal data in an electronic or structured format
            through e-mail or personal request in our head office.
          </Text>
          <Text style={styles.text}>
            We will inform you for any update on our services using the contact
            details you have provided to us.
          </Text>
          <Text style={styles.text}>
            Email us if you wish to withdraw or order the blocking, removal or
            destruction of your personal data.
          </Text>
          <Text style={styles.text}>
            Whenever you have a problem or concern to our service, you have the
            right to complain and be indemnified if proven that we have
            committed a breach against your personal data.
          </Text>
          <Text style={styles.text}>
            If you wish to exercise any of your rights, you may contact our Data
            Protection Officer at dpo@psslai.com and +632 8-7052140
          </Text>

          <Text style={styles.boldTextUnderlined}>Changing this Notice:</Text>
          <Text style={styles.text}>
            PSSLAI reserves the right to amend this Privacy Notice at any time.
            In such case, an updated version of the Privacy Policy will be
            posted on this website. We will also inform you through our website
            and other means of communication for any changes to this Privacy
            Notice.
          </Text>

          <Text style={styles.boldTextUnderlined}>Contact Us:</Text>
          <Text style={styles.text}>
            For any questions regarding to your Personal Data or about this
            Privacy Notice, you may contact our Data Protection Officer using
            the information below:
          </Text>
          <Text style={styles.text}>Data Protection Officer:</Text>
          <Text style={styles.text}>
            dpo@psslai.com +632 8-7052100 local 1040
          </Text>
          <Text style={styles.boldTextUnderlined}>Definitions</Text>
          <Text style={styles.text}>
            Personal Information/ Data – refers to any information whether
            recorded in a material form or not, from which the identity of an
            individual is apparent or can be reasonably and directly ascertained
            by the entity holding the information, or when put together with
            other information would directly and certainly identify an
            individual.
          </Text>
          <Text style={styles.text}>
            Sensitive Personal Information/ Data – refers to personal
            information, to wit:{' '}
          </Text>
          <BulletItems list={bulletListPrivacyZ} bulletWidth={20} />
        </Container>
      </ScrollView>
    </>
  );
};

export default PrivacyPolicy;
