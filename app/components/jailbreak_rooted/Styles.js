import { StyleSheet, Dimensions } from 'react-native';
import {
  WHITE,
  GRAY,
  ERROR,
  BLACK,
  FONT,
  DARK_BLUE,
} from '../../globals/Theme';
import colors from '../../globals/colors';

const { width, height } = Dimensions.get('screen');

const Styles = StyleSheet.create({
  rootedErrorScreenContainer: {
    height: height * 0.75,
    alignItems: 'center',
    justifyContent: 'center',
  },
  rootedErrorTitle: {
    ...FONT,
    fontSize: height * 0.019,
    marginLeft: 7,
    color: WHITE,
  },
  rootedErrorScreenTitle: {
    marginTop: '3%',
    width: '85%',
    textAlign: 'center',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: height * 0.025,
    color: DARK_BLUE,
  },
  rootedIconSize: {
    height: height * 0.19,
  },
});

export default Styles;
