import React from 'react';
import { View, Platform } from 'react-native';
import { Text } from 'react-native-elements';

import { WarningRootedMobile } from '../../images/icons/UserIcons';

import styles from './Styles';

const isIOS = Platform.OS === 'ios';
const errorMsg = isIOS ? 'jailbroken' : 'rooted';

const JailBreakRooted = () => (
  <View style={styles.rootedErrorScreenContainer}>
    <WarningRootedMobile size={styles.rootedIconSize.height} />
    <Text style={styles.rootedErrorScreenTitle}>
      This application is not allowed to run in {errorMsg} device
    </Text>
  </View>
);

export default JailBreakRooted;
