import {
  SIGNUP_SUCCESS,
  USER_SIGNUP_LOADING,
  USER_SIGNUP_RESET,
} from '../types/SignUpTypes';

export const signupReset = () => dispatch => {
  dispatch({ type: USER_SIGNUP_RESET });
};

export const registerUser = data => dispatch => {
  dispatch({ type: USER_SIGNUP_LOADING, payload: true });
  dispatch({
    type: SIGNUP_SUCCESS,
    payload: data,
  });
};
