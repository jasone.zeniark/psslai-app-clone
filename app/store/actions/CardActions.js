/* eslint-disable arrow-parens */
import { Alert } from 'react-native';
import { apiCardDetails } from '../../request/CardApi';
import { decryptObject } from '../../request/RequestEncryption';
import { logoutUser } from './LoginActions';
import Routes from '../../globals/navigations/Routes';

import {
  GET_CARD_SUCCESS,
  GET_CARD_FAIL,
  CARD_RESET,
  CARD_LOCK,
  CARD_UNLOCK,
  CARD_LOADING,
  CARD_LOADED,
} from '../types/CardTypes';

export const cardLoaded = () => dispatch => {
  dispatch({ type: CARD_LOADED });
};

export const cardLoading = () => dispatch => {
  dispatch({ type: CARD_LOADING });
};

export const resetCardDetails = () => dispatch => {
  dispatch({ type: CARD_RESET });
};

export const fetchCardDetails = data => async dispatch => {
  dispatch({
    type: GET_CARD_SUCCESS,
    payload: data,
  });
};

export const lockCard = data => async dispatch => {
  dispatch({
    type: CARD_LOCK,
    payload: data,
  });
};

export const unlockCard = data => async dispatch => {
  dispatch({
    type: CARD_UNLOCK,
    payload: data,
  });
};

export const reloadCardDetails = (loginData, navigation) => async dispatch => {
  dispatch(resetCardDetails());
  dispatch(cardLoading());

  const apiResponse = await apiCardDetails(
    loginData.result.token,
    loginData.result.guid,
  );

  if (apiResponse.status === 200) {
    const { data } = apiResponse;
    const result = decryptObject(data.result);
    // console.log('Card details response:', result);

    dispatch(fetchCardDetails(result));
  } else {
    // handle errors here
    const { data } = apiResponse;
    if (typeof data.errors !== 'undefined') {
      let errMsg = '';
      for (let ctr = 0; ctr < data.errors.length; ctr += 1) {
        const errorObj = data.errors[ctr];
        errMsg += `${errorObj.msg}\r\n`;
      }
      dispatch(resetCardDetails());
      dispatch(cardLoaded());
      Alert.alert('Card Details Error', errMsg);
    } else if (data.message !== 'No active Visa Card') {
      dispatch(resetCardDetails());
      dispatch(cardLoaded());
      if (apiResponse.status === 401) {
        Alert.alert('Authentication Error', data.message);
        dispatch(logoutUser({ message: data.message, status: 0 }));
        navigation.navigate(Routes.LOGIN);
      } else {
        Alert.alert('Error', data.message);
      }
    }
  }
};
