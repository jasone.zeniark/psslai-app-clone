/* eslint-disable arrow-parens */
import { Alert } from 'react-native';
import { apiProfile } from '../../request/Auth';
import { decryptObject } from '../../request/RequestEncryption';
import Routes from '../../globals/navigations/Routes';
import {
  LOGIN_SUCCESS,
  LOGIN_ATTEMPT,
  RESET_LOGIN_ATTEMPT,
  LOGOUT_SUCCESS,
  USER_LOADING,
  CONNECTION_SUCCESS,
  SAVE_USER_PROFILE,
  USER_LOADED,
} from '../types/LoginTypes';

export const loginUser = (loginData, userProfile) => async dispatch => {
  dispatch({ type: USER_LOADING });
  dispatch({
    type: LOGIN_SUCCESS,
    payload: loginData,
  });
  // console.log('userProfile', userProfile);
  dispatch({
    type: SAVE_USER_PROFILE,
    payload: userProfile,
  });
};

export const loginUserAttempt = (attempts, date) => dispatch => {
  dispatch({
    type: LOGIN_ATTEMPT,
    attempts,
    date,
  });
};

export const resetLoginUserAttempt = () => dispatch => {
  dispatch({
    type: RESET_LOGIN_ATTEMPT,
  });
};

export const logoutUser = data => dispatch => {
  dispatch({
    type: LOGOUT_SUCCESS,
    payload: data,
  });
};

export const connected = isConnected => dispatch => {
  dispatch({
    type: CONNECTION_SUCCESS,
    payload: isConnected,
  });
};

export const loadUserProfile = (loginData, navigation) => async dispatch => {
  dispatch({ type: USER_LOADING });

  const apiResponse = await apiProfile(
    loginData.result.token,
    loginData.result.guid,
  );

  if (apiResponse.status === 200) {
    const { data } = apiResponse;
    const result = data.result;
    console.log('User Profile response:', result);

    dispatch({
      type: SAVE_USER_PROFILE,
      payload: result,
    });
    dispatch({ type: USER_LOADED });
  } else {
    // handle errors here
    const { data } = apiResponse;
    if (typeof data.errors !== 'undefined') {
      let errMsg = '';
      for (let ctr = 0; ctr < data.errors.length; ctr += 1) {
        const errorObj = data.errors[ctr];
        errMsg += `${errorObj.msg}\r\n`;
      }
      Alert.alert('User Profile Error', errMsg);
      dispatch({ type: USER_LOADED });
    } else {
      if (apiResponse.status === 401) {
        Alert.alert('Authentication Error', data.message);
        dispatch(logoutUser({ message: data.message, status: 0 }));
        navigation.navigate(Routes.LOGIN);
      } else {
        Alert.alert('Error', data.message);
      }
      dispatch({ type: USER_LOADED });
    }
  }
};
