import {
  VERIFY_OTP_SUCCESS,
  RESEND_OTP_SUCCESS,
  USER_OTP_RESET,
  USER_OTP_LOADING,
} from '../types/OTPTypes';

export const resetOTP = () => dispatch => {
  dispatch({ type: USER_OTP_RESET });
};

export const verifyOTP = data => async dispatch => {
  dispatch({ type: USER_OTP_LOADING });
  dispatch({
    type: VERIFY_OTP_SUCCESS,
    payload: data,
  });
};

export const resendOTP = data => async dispatch => {
  dispatch({ type: USER_OTP_LOADING });
  dispatch({
    type: RESEND_OTP_SUCCESS,
    payload: data,
  });
};
