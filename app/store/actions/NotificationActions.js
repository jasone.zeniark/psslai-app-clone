/* eslint-disable arrow-parens */
import {
  GET_NOTIFICATIONS_SUCCESS,
  READ_NOTIFICATION_SUCCESS,
  NOTIFICATION_RESET,
} from '../types/NotificationTypes';

export const resetCardDetails = () => dispatch => {
  dispatch({ type: NOTIFICATION_RESET });
};

export const fetchCardDetails = data => async dispatch => {
  dispatch({
    type: GET_NOTIFICATIONS_SUCCESS,
    payload: data,
  });
};
