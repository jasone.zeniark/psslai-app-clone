export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAIL = 'LOGIN_FAIL';
export const LOGIN_ATTEMPT = 'LOGIN_ATTEMPT';
export const RESET_LOGIN_ATTEMPT = 'RESET_LOGIN_ATTEMPT';
export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';
export const USER_LOADING = 'USER_LOADING';
export const USER_LOADED = 'USER_LOADED';
export const CONNECTION_SUCCESS = 'CONNECTION_SUCCESS';
export const SAVE_USER_PROFILE = 'SAVE_USER_PROFILE';
