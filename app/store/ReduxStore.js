import React from 'react';
import thunk from 'redux-thunk';
import { persistStore, persistReducer } from 'redux-persist';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { combineReducers, createStore, applyMiddleware, compose } from 'redux';

import LoginReducer from './reducers/LoginReducers';
import SignUpReducer from './reducers/SignUpReducers';
import OTPReducers from './reducers/OTPReducers';
import CardReducers from './reducers/CardReducers';
import NotificationReducers from './reducers/NotificationReducers';

const loginConfig = {
  key: 'login',
  storage: AsyncStorage,
  whitelist: ['loginAttempts', 'attemptDate'],
};

const rootReducer = combineReducers({
  login: persistReducer(loginConfig, LoginReducer),
  signup: SignUpReducer,
  otp: OTPReducers,
  card: CardReducers,
  notification: NotificationReducers,
});

const initialState = {};

const middleware = [thunk];

const store = createStore(
  rootReducer,
  initialState,
  compose(applyMiddleware(...middleware)),
);

const persistor = persistStore(store);

export { store, persistor };
