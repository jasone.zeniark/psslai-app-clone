import {
  SIGNUP_SUCCESS,
  SIGNUP_FAIL,
  USER_SIGNUP_LOADING,
  USER_SIGNUP_RESET,
} from '../types/SignUpTypes';

const initialState = {
  status: 0,
  connected: false,
  isSuccess: false,
  isLoading: false,
  message: '',
  result: {
    guid: '',
    mobileNumber: '',
    pinId: '',
    delaySec: 0,
  },
};

const SignUpReducer = (state = initialState, action) => {
  switch (action.type) {
    case SIGNUP_SUCCESS:
      return {
        ...state,
        status: action.payload.status,
        isSuccess: true,
        isLoading: false,
        message: action.payload.message,
        result: {
          guid: action.payload.result.guid,
          mobileNumber: action.payload.result.mobileNumber,
          pinId: action.payload.result.pinId,
          // delaySec: action.payload.result.delaySec,
        },
      };
    case SIGNUP_FAIL:
      return {
        ...state,
        status: action.payload.status,
        isSuccess: false,
        isLoading: false,
        message: action.payload.message,
        result: {
          guid: '',
          mobileNumber: '',
          pinId: '',
          delaySec: 0,
        },
      };
    case USER_SIGNUP_RESET:
      return {
        status: 0,
        connected: false,
        isSuccess: false,
        isLoading: false,
        message: '',
        result: {
          guid: '',
          mobileNumber: '',
          pinId: '',
          delaySec: 0,
        },
      };
    case USER_SIGNUP_LOADING:
      return {
        ...state,
        isLoading: action.payload,
      };
    default:
      return state;
  }
};

export default SignUpReducer;
