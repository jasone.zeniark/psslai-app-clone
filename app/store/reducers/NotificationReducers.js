import {
  GET_NOTIFICATIONS_SUCCESS,
  NOTIFICATION_RESET,
} from '../types/NotificationTypes';

const initialState = {
  status: 0,
  result: [],
};

const NotificationReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_NOTIFICATIONS_SUCCESS:
      return {
        ...state,
        status: action.payload.status,
        result: action.payload.data,
      };
    case NOTIFICATION_RESET:
      return {
        ...state,
        status: 0,
        result: [],
      };
    default:
      return state;
  }
};

export default NotificationReducer;
