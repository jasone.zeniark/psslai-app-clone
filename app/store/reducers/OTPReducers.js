import {
  VERIFY_OTP_SUCCESS,
  VERIFY_OTP_FAIL,
  RESEND_OTP_SUCCESS,
  RESEND_OTP_FAIL,
  USER_OTP_RESET,
  USER_OTP_LOADING,
} from '../types/OTPTypes';

const initialState = {
  otpStatus: 0,
  isSuccess: false,
  isLoading: false,
  message: '',
  guid: '',
  pinId: '',
  token: '',
};

const OTPReducers = (state = initialState, action) => {
  switch (action.type) {
    case VERIFY_OTP_SUCCESS:
      return {
        ...state,
        otpStatus: action.payload.status,
        isLoading: false,
        isSuccess: true,
        message: action.payload.message,
        guid: action.payload.result.guid,
        pinId: '',
        token: action.payload.result.token,
      };
    case VERIFY_OTP_FAIL:
      return {
        ...state,
        otpStatus: action.payload.status,
        isLoading: false,
        isSuccess: false,
        message: '',
        guid: '',
        pinId: '',
        token: '',
      };
    case RESEND_OTP_SUCCESS:
      return {
        ...state,
        otpStatus: action.payload.status,
        isLoading: false,
        isSuccess: false,
        message: action.payload.message,
        guid: action.payload.result.guid,
        pinId: action.payload.result.pinId,
        token: '',
      };
    case RESEND_OTP_FAIL:
      return {
        ...state,
        otpStatus: action.payload.status,
        isLoading: false,
        isSuccess: false,
        message: '',
        guid: '',
        pinId: '',
        token: '',
      };
    case USER_OTP_RESET:
      return {
        ...state,
        otpStatus: 0,
        isLoading: false,
        isSuccess: false,
        message: '',
        guid: '',
        pinId: '',
        token: '',
      };
    case USER_OTP_LOADING:
      return {
        ...state,
        isLoading: true,
      };
    default:
      return state;
  }
};

export default OTPReducers;
