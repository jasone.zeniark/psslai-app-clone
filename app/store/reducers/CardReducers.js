import {
  GET_CARD_SUCCESS,
  GET_CARD_FAIL,
  CARD_RESET,
  CARD_LOCK,
  CARD_UNLOCK,
  CARD_LOADING,
  CARD_LOADED,
} from '../types/CardTypes';

const initialState = {
  isLoading: false,
  isActive: false,
  result: {},
};

const CardReducers = (state = initialState, action) => {
  switch (action.type) {
    case GET_CARD_SUCCESS:
      return {
        ...state,
        isActive: true,
        isLoading: false,
        result: action.payload,
      };
    case CARD_RESET:
      return {
        ...state,
        status: 0,
        isActive: false,
        result: {},
      };
    case CARD_LOCK:
      return {
        ...state,
        isActive: false,
        isLoading: false,
        result: action.payload.status
      };
    case CARD_UNLOCK:
      return {
        ...state,
        isActive: false,
        isLoading: false,
        result: action.payload.status
      };
    case CARD_LOADING:
      return {
        ...state,
        isLoading: true,
      };
    case CARD_LOADED:
      return {
        ...state,
        isLoading: false,
      };
    default:
      return state;
  }
};

export default CardReducers;
