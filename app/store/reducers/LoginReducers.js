import {
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  LOGIN_ATTEMPT,
  RESET_LOGIN_ATTEMPT,
  LOGOUT_SUCCESS,
  USER_LOADING,
  USER_LOADED,
  CONNECTION_SUCCESS,
  SAVE_USER_PROFILE,
} from '../types/LoginTypes';

import { decryptObject } from '../../request/RequestEncryption';

const initialState = {
  status: 0,
  connected: true,
  isAuthenticated: false,
  isLoading: false,
  message: '',
  loginAttempts: 3,
  attemptDate: '',
  result: {
    guid: '',
    mobileNumber: '',
    pinId: '',
    delaySec: 0,
    token: '',
    profile: {},
  },
};

const LoginReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_SUCCESS:
      return {
        ...state,
        status: action.payload.status,
        isAuthenticated: true,
        isLoading: false,
        message: action.payload.message,
        result: {
          guid: action.payload.result.guid,
          mobileNumber: action.payload.result.mobileNumber,
          pinId: action.payload.result.pinId,
          delaySec: action.payload.result.delaySec,
          token: action.payload.result.token,
          profile:
            typeof action.payload.result.profile !== 'undefined'
              ? decryptObject(action.payload.result.profile)
              : 'login success',
        },
      };
    case LOGIN_FAIL:
      return {
        ...state,
        status: action.payload.status,
        isAuthenticated: false,
        isLoading: false,
        message: action.payload.message,
        result: {
          guid: '',
          mobileNumber: '',
          pinId: '',
          delaySec: 0,
          token: '',
          profile: {},
        },
      };
    case LOGIN_ATTEMPT:
      return {
        ...state,
        loginAttempts: action.attempts,
        attemptDate: action.date,
      };
    case RESET_LOGIN_ATTEMPT:
      return {
        ...state,
        loginAttempts: 3,
        attemptDate: '',
      };
    case LOGOUT_SUCCESS:
      return {
        ...state,
        status: action.payload.status,
        isAuthenticated: false,
        message: action.payload.message,
        result: {
          guid: '',
          mobileNumber: '',
          pinId: '',
          delaySec: 0,
          token: '',
          profile: {},
        },
      };
    case SAVE_USER_PROFILE:
      return {
        ...state,
        result: {
          ...state.result,
          isLoading: false,
          profile: decryptObject(action.payload)
        },
      };
    case USER_LOADING:
      return {
        ...state,
        isLoading: true,
      };
    case USER_LOADED:
      return {
        ...state,
        isLoading: false,
      };
    case CONNECTION_SUCCESS:
      return {
        ...state,
        connected: action.payload,
      };
    default:
      return state;
  }
};

export default LoginReducer;
