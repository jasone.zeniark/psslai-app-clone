import React from 'react';
import { Button } from 'react-native-elements';

const FormButton = props => <Button {...props} />;

export default FormButton;
