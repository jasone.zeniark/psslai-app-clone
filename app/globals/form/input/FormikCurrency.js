import React, { useState } from 'react';
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native';
import { Input } from 'react-native-elements';
import CurrencyInput from 'react-native-currency-input';

const styles = StyleSheet.create({
  inputWrapper: {
    paddingHorizontal: 15,
    paddingVertical: 2,
    borderWidth: 1,
    borderColor: '#D1D5DD',
    color: '#2B2D33',
    borderRadius: 5,
    marginBottom: -5,
    fontSize: 16,
    fontFamily: 'Montserrat-Regular',
    marginHorizontal: -10,
    fontWeight: '600',
    backgroundColor: '#fff',
  },
  inputText: {
    color: '#2B2D33',
    fontSize: 16,
    fontFamily: 'Montserrat-Regular',
    fontWeight: '600',
  },
  inputNumber: {
    color: '#2B2D33',
    fontSize: 16,
    fontFamily: 'Montserrat-Regular',
    fontWeight: '600',
    textAlign: 'right',
  },
  activeInput: {
    borderColor: '#201751',
  },
  label: {
    fontSize: 14,
    fontWeight: '100',
    color: '#2B2D33',
    marginBottom: 10,
    fontFamily: 'Montserrat-Medium',
  },
  currencyLabel: {
    fontSize: 14,
    lineHeight: 50,
    fontWeight: '100',
    color: '#2B2D33',
    marginBottom: 0,
    fontFamily: 'Montserrat-Medium',
  },
  inputStyle: {
    fontFamily: 'Montserrat-Regular',
  },
  errorText: {
    fontSize: 12,
    color: 'red',
    marginTop: -20,
    marginBottom: 15,
  },
  errorInput: {
    borderColor: 'red',
    marginBottom: 5,
  },
  currency: {
    color: '#2B2D33',
    fontSize: 16,
    fontFamily: 'Montserrat-Regular',
    fontWeight: '600',
  },
  currencyStyle: {
    color: '#2B2D33',
    fontSize: 16,
    fontFamily: 'Montserrat-Regular',
    fontWeight: '600',
    textAlign: 'right',
    width: '100%',
    marginLeft: -40,
  },
  currencyWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 15,
    borderWidth: 1,
    borderColor: '#D1D5DD',
    color: '#2B2D33',
    borderRadius: 5,
    backgroundColor: '#fff',
    marginBottom: 20,
  },
  currencyWrapperError: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 15,
    borderWidth: 1,
    borderColor: 'red',
    color: '#2B2D33',
    borderRadius: 5,
    backgroundColor: '#fff',
    marginBottom: 20,
  },
});

const CustomInput = props => {
  const {
    field: { name, onBlur, onChange, value },
    form: { errors, touched, setFieldTouched },
    ...inputProps
  } = props;

  const hasError = errors[name] && touched[name];
  const [isFocus, setIsFocus] = useState(false);

  return (
    <>
      <Text style={styles.label}>{props.labelText}</Text>
      <View
        style={hasError ? styles.currencyWrapperError : styles.currencyWrapper}>
        <Text style={styles.currencyLabel}>PHP</Text>
        <CurrencyInput
          delimiter=","
          separator="."
          precision={2}
          // onChangeText={(formattedValue) => {
          //   console.log(formattedValue); // $2,310.46
          // }}
          style={styles.currencyStyle}
          leftIcon={<Text style={styles.currency}>PHP</Text>}
          {...inputProps}
          ignoreNegative={true}
          onChangeText={(text) => {
            onChange(name)(text);
            // setInputValue(value);
            // console.log(props.setValue);
          }}
          onBlur={() => {
            setFieldTouched(name);
            onBlur(name);
            setIsFocus(false);
          }}
        />
      </View>
      {hasError && <Text style={styles.errorText}>{errors[name]}</Text>}
    </>
  );
};

export default CustomInput;
