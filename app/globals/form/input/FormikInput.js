import React, { useState } from 'react';
import { Text, StyleSheet } from 'react-native';
import { Input } from 'react-native-elements';

const styles = StyleSheet.create({
  inputWrapper: {
    paddingHorizontal: 15,
    paddingVertical: 2,
    borderWidth: 1,
    borderColor: '#D1D5DD',
    color: '#2B2D33',
    borderRadius: 5,
    marginBottom: -5,
    fontSize: 16,
    fontFamily: 'Montserrat-Regular',
    marginHorizontal: -10,
    fontWeight: '600',
    backgroundColor: '#fff',
  },
  inputText: {
    color: '#2B2D33',
    fontSize: 16,
    fontFamily: 'Montserrat-Regular',
    fontWeight: '600',
  },
  inputNumber: {
    color: '#2B2D33',
    fontSize: 16,
    fontFamily: 'Montserrat-Regular',
    fontWeight: '600',
    textAlign: 'right',
  },
  activeInput: {
    borderColor: '#201751',
  },
  label: {
    fontSize: 14,
    fontWeight: '100',
    color: '#2B2D33',
    marginBottom: 10,
    fontFamily: 'Montserrat-Medium',
  },
  inputStyle: {
    fontFamily: 'Montserrat-Regular',
  },
  errorText: {
    fontSize: 12,
    color: 'red',
    marginTop: -20,
    marginBottom: 15,
  },
  errorInput: {
    borderColor: 'red',
    marginBottom: 5,
  },
});

const CustomInput = props => {
  const {
    field: { name, onBlur, onChange, value },
    form: { errors, touched, setFieldTouched },
    ...inputProps
  } = props;

  const hasError = errors[name] && touched[name];
  const [isFocus, setIsFocus] = useState(false);

  return (
    <>
      <Text style={styles.label}>{props.labelText}</Text>
      <Input
        style={props.currencyFormat ? styles.inputNumber : styles.inputText}
        ref={ref =>
          ref &&
          ref.setNativeProps({ style: { fontFamily: 'Montserrat-Regular' } })
        }
        // inputContainerStyle={styles.inputWrapper}
        onFocus={() => {
          setIsFocus(true);
          if (name === 'password' || name === 'confirm_password') {
            props.onFocusText();
          }
        }}
        inputContainerStyle={[
          styles.inputWrapper,
          hasError && styles.errorInput,
          isFocus && styles.activeInput,
        ]}
        value={value}
        onChangeText={(text) => {
          onChange(name)(text);
          // setInputValue(value);
          // console.log(props.setValue);
        }}
        placeholder={props.placeholderText}
        onBlur={() => {
          setFieldTouched(name);
          onBlur(name);
          setIsFocus(false);
          if (name === 'password' || name === 'confirm_password') {
            props.onBlurText();
          }
        }}
        {...inputProps}
      />
      {hasError && <Text style={styles.errorText}>{errors[name]}</Text>}
    </>
  );
};

export default CustomInput;
