import React from 'react';
import { Input } from 'react-native-elements';
import styles from './Styles';

const FormInput = props => (
  <Input
    inputContainerStyle={!props.isFocus ? styles.container : styles.activeInput}
    labelStyle={styles.label}
    {...props}
    autoCapitalize="none"
    autoCorrect={false}
  />
);

export default FormInput;
