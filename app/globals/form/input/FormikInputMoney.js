import React, { useState } from 'react';
import { Text, StyleSheet, Keyboard, Platform, View } from 'react-native';
import { Input } from 'react-native-elements';
import { formatNumber } from 'react-native-currency-input';
import RegExp from '../../utils/RegExp';

const styles = StyleSheet.create({
  inputWrapper: {
    borderWidth: 0,
    borderColor: '#fff',
    fontFamily: 'Montserrat-Regular',
    backgroundColor: 'transparent',
  },
  inputText: {
    color: '#2B2D33',
    fontSize: 16,
    fontFamily: 'Montserrat-Regular',
    fontWeight: '600',
  },
  inputNumber: {
    color: '#2B2D33',
    fontSize: 16,
    fontFamily: 'Montserrat-Regular',
    fontWeight: '600',
    textAlign: 'right',
  },
  activeInput: {
    borderColor: '#201751',
  },
  label: {
    fontSize: 14,
    fontWeight: '100',
    color: '#2B2D33',
    marginBottom: 10,
    fontFamily: 'Montserrat-Medium',
  },
  currencyLabel: {
    fontSize: 14,
    lineHeight: 50,
    fontWeight: '100',
    color: '#2B2D33',
    marginBottom: 0,
    fontFamily: 'Montserrat-Medium',
  },
  inputStyle: {
    fontFamily: 'Montserrat-Regular',
  },
  errorText: {
    fontSize: 12,
    color: 'red',
    marginTop: -20,
    marginBottom: 15,
  },
  errorInput: {
    borderColor: 'red',
    marginBottom: 5,
  },
  currency: {
    color: '#2B2D33',
    fontSize: 16,
    fontFamily: 'Montserrat-Regular',
    fontWeight: '600',
  },
  currencyStyle: {
    color: '#2B2D33',
    fontSize: 16,
    fontFamily: 'Montserrat-Regular',
    fontWeight: '600',
    textAlign: 'right',
    width: '100%',
    marginLeft: -40,
  },
  currencyWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 15,
    borderWidth: 1,
    borderColor: '#D1D5DD',
    color: '#2B2D33',
    borderRadius: 5,
    marginBottom: 20,
    height: 50,
    paddingRight: 35,
    width: 'auto',
    backgroundColor:'#fff',
  },
  currencyWrapperError: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 15,
    borderWidth: 1,
    borderColor: 'red',
    color: '#2B2D33',
    borderRadius: 5,
    marginBottom: 20,
    height: 50,
    paddingRight: 35,
    width: 'auto',
    backgroundColor:'#fff',
  },
});

const CustomInput = props => {
  const {
    field: { name, onBlur, onChange, value },
    form: { errors, touched, setFieldTouched },
    ...inputProps
  } = props;

  const hasError = errors[name] && touched[name];
  const [isFocus, setIsFocus] = useState(false);
  const [inputValue, setInputValue] = useState(value);

  return (
    <>
      <Text style={styles.label}>{props.labelText}</Text>
      <View
        style={hasError ? styles.currencyWrapperError : styles.currencyWrapper}>
        <Text style={styles.currencyLabel}>PHP</Text>
        <Input
          style={styles.inputNumber}
          ref={ref =>
            ref &&
            ref.setNativeProps({ style: { fontFamily: 'Montserrat-Regular' } })
          }
          inputContainerStyle={styles.inputWrapper}
          onFocus={() => {
            props.setMaxMoneyLength(9);
            setIsFocus(true);
            const unformattedValue = inputValue.replace(/,/g, '');
            console.log('focus', unformattedValue % 1);

            if (unformattedValue % 1 === 0) {
              setInputValue(unformattedValue.replace('.00', ''));
              props.setFieldValue('transferAmount', unformattedValue.replace('.00', ''));
              console.log('no need zero');
            } else {
              props.setFieldValue('transferAmount', unformattedValue);
              setInputValue(unformattedValue);
            }
          }}
          value={inputValue}
          onChangeText={text => {
            // if()
            // setInputValue(text.replace(/[^0-9.]/, ''));
            // onChange(name)(text);
            onChange(name)(text);

            if (isFocus) {
              setInputValue(text.replace(/,/g, ''));
              props.setFieldValue('transferAmount', text.replace(/,/g, ''));
            } else {
              setInputValue(text);
              props.setFieldValue('transferAmount', text);
            }
            // const unformattedValue = inputValue.replace(/,/g, '');

            // console.log(text.length);
            // amount: parseFloat(formData.transferAmount.replace(/,/g, '')),

            // const unformattedValue = inputValue.replace(/,/g, '');
            // setInputValue(unformattedValue);

            // if (unformattedValue % 1 !== 0) {
            //   if (text.length < 16) {
            //     setInputValue(text);
            //     props.setFieldValue('transferAmount', text);
            //   } else {
            //     setInputValue(inputValue);
            //     props.setFieldValue('transferAmount', inputValue);
            //   }
            // } else {
            //   // eslint-disable-next-line no-lonely-if
            //   if (text.length < 11) {
            //     setInputValue(text);
            //     props.setFieldValue('transferAmount', text);
            //   } else {
            //     setInputValue(inputValue);
            //     props.setFieldValue('transferAmount', inputValue);
            //   }
            // }
          }}
          placeholder={props.placeholderText}
          onBlur={() => {
            props.setMaxMoneyLength(16);
            console.log('unfocus');

            if (RegExp.money.test(inputValue)) {
              const formattedValue = formatNumber(inputValue, {
                separator: '.',
                precision: 2,
                delimiter: ',',
                ignoreNegative: true,
              });
              if (formattedValue !== 'NaN') {
                setInputValue(formattedValue);
                props.setFieldValue('transferAmount', formattedValue);
              }
            }

            setFieldTouched(name);
            onBlur(name);
            setIsFocus(false);
            if (name === 'password' || name === 'confirm_password') {
              props.onBlurText();
            }
          }}
          {...inputProps}
        />
      </View>
      {hasError && <Text style={styles.errorText}>{errors[name]}</Text>}
    </>
  );
};

export default CustomInput;
