import { StyleSheet, Dimensions } from 'react-native';

import { ERROR } from '../../Theme';

const { height } = Dimensions.get('screen');

const Styles = StyleSheet.create({
  errorText: {
    fontFamily: 'Montserrat-Regular',
    fontSize: height * 0.02,
    color: ERROR,
  },
});

export default Styles;
