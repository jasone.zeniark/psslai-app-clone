import React from 'react';
import { Text } from 'react-native';
import styles from './Styles';

const ErrorActivity = ({ errorStyles, message }) => (
  <Text style={[styles.errorText, errorStyles]}>{message}</Text>
);

export default ErrorActivity;
