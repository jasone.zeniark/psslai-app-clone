import React from 'react';
import { CheckBox } from 'react-native-elements';
import styles from './Styles';

const FormCheckbox = (props) => (
  <CheckBox containerStyle={styles.container} {...props} />
);

export default FormCheckbox;
