import React from 'react';
import { View, Text } from 'react-native';
import ModalDropdown from 'react-native-modal-dropdown';

import { DropdownIcon } from '../../../images/icons/UserIcons';
import styles from './Styles';

const FormInput = React.forwardRef((props, ref) => (
  <View style={props.disabled ? styles.wrapperDisabled : ''}>
    <Text style={props.disabled ? styles.textDisabled : styles.formLabel}>
      {props.label}
    </Text>
    <ModalDropdown
      {...props}
      isFullWidth={true}
      animated={false}
      // defaultIndex={defaultIndex}
      ref={ref}
      renderRightComponent={() => (
        <DropdownIcon style={styles.mdDropdownIcon} />
      )}
      renderSeparator={() => <View />}
      style={styles.mdStyle}
      dropdownStyle={styles.mdDropdownStyle}
      dropdownTextStyle={styles.mdDropdownTextStyle}
      textStyle={styles.mdTextStyle}
    />
  </View>
));

export default FormInput;
