import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  wrapperDisabled: {
    opacity: 0.6,
  },
  textDisabled: {
    opacity: 0.55,
    fontSize: 14,
    fontWeight: '100',
    color: '#2B2D33',
    fontFamily: 'Montserrat-Medium',
  },
  formLabel: {
    fontSize: 14,
    fontWeight: '100',
    color: '#2B2D33',
    fontFamily: 'Montserrat-Medium',
  },
  mdStyle: {
    backgroundColor: '#fff',
    borderWidth: 1,
    borderColor: '#D1D5DD',
    borderRadius: 5,
    marginTop: 10,
    marginBottom: 17,
  },
  mdDropdownStyle: {
    padding: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    elevation: 12,
    marginTop: -25,
  },
  mdDropdownTextStyle: {
    padding: 10,
    color: '#2B2D33',
    fontSize: 16,
    fontFamily: 'Montserrat-Regular',
    fontWeight: '600',
  },
  mdTextStyle: {
    padding: 15,
    color: '#2B2D33',
    fontSize: 16,
    fontFamily: 'Montserrat-Regular',
    fontWeight: '600',
  },
  mdDropdownIcon: {
    marginLeft: 'auto',
    marginRight: 10,
  },
});
