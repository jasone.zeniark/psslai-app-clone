import { StyleSheet, Dimensions } from 'react-native';
import { DARK_BLUE } from '../Theme';

const { height } = Dimensions.get('screen');

const Styles = StyleSheet.create({
  title: {
    fontFamily: 'Montserrat-Regular',
    alignSelf: 'center',
    marginBottom: 20,
    fontSize: height * 0.028,
    color: DARK_BLUE,
    fontWeight: '700',
  },
  header: {
    fontFamily: 'Montserrat-Regular',
    marginBottom: 10,
    fontSize: height * 0.025,
    color: DARK_BLUE,
    fontWeight: '600',
  },
  text: {
    fontFamily: 'Montserrat-Regular',
    marginBottom: 10,
    fontSize: height * 0.018,
    fontWeight: '600',
  },
  boldText: {
    fontFamily: 'Montserrat-Regular',
    marginBottom: 10,
    fontSize: height * 0.018,
    fontWeight: '700',
  },
  center: {
    textAlign: 'center',
  },
  column: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    width: 200,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    flexWrap: 'wrap',
    flex: 1,
  },
  bulletParentStyle: {
    width: 40,
  },
  noBulletStyle: {
    width: 20,
  },
  bulletStyle: {
    width: 25,
  },
  bulletTextStyle: {
    fontFamily: 'Montserrat-Regular',
    fontSize: height * 0.018,
    fontWeight: '700',
  },
  bulletContent: {
    flex: 1,
  },
});

export default Styles;
