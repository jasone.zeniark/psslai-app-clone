/* eslint-disable prettier/prettier */
import React from 'react';
import { View } from 'react-native';
import { Text } from 'react-native-elements';
import styles from './Styles';

const BulletItems = (props) => (
  <>
    {props.list.map((item1, i) => (
      <View style={styles.row} key={i}>
        <View style={{ width: props.bulletWidth }}>
          <Text style={styles.bulletTextStyle}>{item1.bullet}</Text>
        </View>
        <View style={styles.bulletContent}>
          {item1.content}
          {/* Level 2 List  */}
          {typeof item1.subContent !== 'undefined' && (
            <>
              {item1.subContent.map((item2, ii) => (
                <View style={styles.row} key={ii}>
                  <View style={styles.bulletStyle}>
                    <Text style={styles.bulletTextStyle}>{item2.bullet}</Text>
                  </View>
                  <View style={styles.bulletContent}>
                    {item2.content}
                    {/* Level 3 List  */}
                    {typeof item2.subContent !== 'undefined' && (
                      <>
                        {item2.subContent.map((item3, iii) => (
                          <View style={styles.row} key={iii}>
                            <View style={styles.bulletStyle}>
                              <Text style={styles.bulletTextStyle}>{item3.bullet}</Text>
                            </View>
                            <View style={styles.bulletContent}>
                              {item3.content}
                              {/* Level 4 List  */}
                              {typeof item3.subContent !== 'undefined' && (
                                <>
                                  {item3.subContent.map((item4, iv) => (
                                    <View style={styles.row} key={iv}>
                                      <View style={styles.bulletStyle}>
                                        <Text style={styles.bulletTextStyle}>{item4.bullet}</Text>
                                      </View>
                                      <View style={styles.bulletContent}>
                                        {item4.content}
                                      </View>
                                    </View>
                                  ))}
                                </>
                              )}
                            </View>
                          </View>
                        ))}
                      </>
                    )}
                  </View>
                </View>
              ))}
            </>
          )}
        </View>
      </View>
    ))}
  </>
);

export default BulletItems;
