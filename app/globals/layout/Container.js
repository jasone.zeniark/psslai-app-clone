import React from 'react';
import {SafeAreaView} from 'react-native';

import styles from './Styles';

const Container = ({style, children}) => (
  <SafeAreaView style={[styles.container, style]}>{children}</SafeAreaView>
);

export default Container;
