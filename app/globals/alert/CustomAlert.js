import React, { createContext, useState, Component } from 'react';
import { Overlay } from 'react-native-elements';

const CustomAlert = ({ visible, toggle, styles, children }) => {
  return (
    <Overlay isVisible={visible} onBackdropPress={toggle} overlayStyle={styles}>
      {children}
    </Overlay>
  );
};

export default CustomAlert;
