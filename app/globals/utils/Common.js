/* eslint-disable import/prefer-default-export */

import { CARD_TYPE } from './Constant';

export const getCardTypeLabel = (type) => {
  switch (type) {
    case CARD_TYPE.SAVINGS:
      return 'SAVINGS';
    case CARD_TYPE.CAPCON:
      return 'CAPCON';
    case CARD_TYPE.CASA:
      return 'CASA';
    default:
      return type;
  }
};
