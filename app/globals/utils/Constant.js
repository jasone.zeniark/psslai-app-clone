/* eslint-disable import/prefer-default-export */
export const CARD_TYPE = {
  SAVINGS: 'SA',
  CASA: 'CS',
  CAPCON: 'CA',
};

export const cardStatus = {
  STATUS_NO_LINKED_CARD: 0,
  STATUS_ACTIVE: 1,
  STATUS_PENDING: 2,
  STATUS_FOR_ACTIVATION: 3,
  STATUS_DEACTIVATED: 4,
  STATUS_SUSPENDED: 10,
  STATUS_PERSO_CARD_ACTIVATION_RETRY: 11,
};

export const cardCode = {
  CARD_TYPE_CODE_BLUE: 1,
  CARD_TYPE_CODE_GOLD: 2,
  CARD_TYPE_CODE_PLATINUM: 3,
};

export const cardType = {
  CARD_TYPE_GENERIC: 'blue',
  CARD_TYPE_BLUE: 'blue',
  CARD_TYPE_GOLD: 'gold',
  CARD_TYPE_PLATINUM: 'platinum',
};

export const cardErrorMessage = {
  CARD_SUSPENDED:
    'Card is suspended due to 3 failed PIN entry attempts. Please contact EON at (632) 8981-3688 or send an email at heretohelp@eonbankph.com for assistance.',
  CARD_ADMIN_LOCKED:
    'Card is admin locked. Please go to PSSLAI Office to request the admin to unlock this card.',
  CARD_DEACTIVATED:
    'Card is deactivated. Please go to PSSLAI Office to request the admin to activate this card.',
};
