import React from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  ScrollView,
  StyleSheet,
  Image,
} from 'react-native';
import COLORS from '../colors';
import STYLES from '../styles';

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

const {width, height} = Dimensions.get('window');

const ModalPicker = props => {
  return (
    <View style={STYLES.iContainter}>
      <View style={[STYLES.modal, {width: WIDTH - 70, height: HEIGHT / 2}]}>
        <TouchableOpacity
          style={{
            width: 30,
            height: 30,
            alignSelf: 'flex-end',
            marginEnd: 20,
            marginTop: 20,
          }}
          onPress={() => props.changeModalVisibility2(false)}>
          <Image
            style={{
              width: 30,
              height: 30,
            }}
            source={require('../../images/assets/close.png')}
          />
        </TouchableOpacity>

        <View style={{marginHorizontal: 20}}>
          <View style={{marginTop: 20}}>
            <Text style={{fontSize: 20, fontWeight: 'bold'}}>
              Choose the Type of Card to Apply For
            </Text>
          </View>
          <View style={{marginTop: 20}}>
            <Text style={{fontSize: 16, fontWeight: '200'}}>
              Personalized card takes 3-5 working days while default card can be
              claimed immediately the same day.
            </Text>
          </View>

          <TouchableOpacity
            style={{
              backgroundColor: COLORS.primary,
              marginTop: 25,
              height: height / 17,
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: 5,
            }}>
            <View>
              <Text
                style={{
                  color: COLORS.white,
                  fontSize: 15,
                  fontWeight: 'bold',
                  textAlign: 'center',
                }}>
                Personallized
              </Text>
              <Text
                style={{
                  color: COLORS.white,
                  fontSize: 15,
                  fontWeight: 'bold',
                  textAlign: 'center',
                }}>
                (3-5 Working Days)
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              marginTop: 10,
              height: height / 17,
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: 5,
              borderWidth: 1,
              borderColor: COLORS.light,
            }}>
            <View>
              <Text
                style={{
                  fontSize: 15,
                  fontWeight: 'bold',
                  textAlign: 'center',
                }}>
                Default (Same Day)
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export {ModalPicker};
