const routes = {
  LANDING: 'Landing',
  SIGNUP: 'Sign Up',
  LOGIN: 'Login',
  MAIN_DASHBOARD: 'Main Dashboard',
  SUB_DASHBOARD: 'Sub Dashboard',
  SPLASH: 'Splash',
  DASHBOARD: 'Dashboard',
  TRANSFER_FUNDS: 'Transfer Funds',
  PAY_BILLS: 'Pay Bills',
  BUY_LOAD: 'Buy Load',
  MORE: 'More',
  WEB_VIEW: 'Web View',
  OTP: 'One Time Password',
  CARD_SETTINGS: 'Card Settings',
  TERMS_AND_CONDITIONS: 'Terms and Conditions',
  PRIVACY_POLICY: 'Privacy Policy',
  CHANGE_CVV: 'Change CVV',
  CHANGE_ATM_PIN: 'Change ATM PIN',
  JAIL_BROKEN_ROOTED: 'Jail Broken / Rooted',
};

export default routes;
