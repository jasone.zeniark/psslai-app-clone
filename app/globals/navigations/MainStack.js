import React, { useEffect, useState } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { getUniqueId } from 'react-native-device-info';
import { useDispatch, useSelector } from 'react-redux';
import NetInfo from '@react-native-community/netinfo';

import BottomTab from '../../components/dashboard/tab_navigator/BottomTabs';
import Landing from '../../components/auth/landing/AuthLanding';
import Login from '../../components/auth/login/AuthLogin';
import OneTimePassword from '../../components/auth/otp/OneTimePassword';
import SignUp from '../../components/auth/signup/AuthSignUp';
import Splash from '../../components/splash/UserSplash';
import JailBreakRooted from '../../components/jailbreak_rooted/JailBreakRooted';
import WebView from '../../components/webview/UserWebView';
import TermsAndConditions from '../../components/landing/terms_and_condition/TermsAndConditions';
import PrivacyPolicy from '../../components/landing/privacy_policy/PrivacyPolicy';

import { connected } from '../../store/actions/LoginActions';

import routes from './Routes';

const { Navigator, Screen } = createStackNavigator();

const {
  LANDING,
  SIGNUP,
  LOGIN,
  MAIN_DASHBOARD,
  SPLASH,
  JAIL_BROKEN_ROOTED,
  WEB_VIEW,
  OTP,
  CARD_SETTINGS,
  TERMS_AND_CONDITIONS,
  PRIVACY_POLICY,
  TRANSFER_FUNDS,
  CHANGE_CVV,
  CHANGE_ATM_PIN,
} = routes;

const MainStack = () => {
  const dispatch = useDispatch();
  const login = useSelector(state => state.login);

  const [isSplashLoading, setSplashLoading] = useState(true);

  useEffect(() => {
    console.log('Your Device ID is: ', getUniqueId());
  }, [login.result.token]);

  useEffect(() => {
    setTimeout(() => setSplashLoading(false), 2000);
  }, []);

  useEffect(() => {
    if (!isSplashLoading) {
      NetInfo.addEventListener(state => {
        // let connStatus = false;
        let connStatus = state.isConnected;

        dispatch(connected(connStatus));
      });
    }
  }, [isSplashLoading]);

  return (
    <Navigator
      initialRouteName={
        isSplashLoading ? SPLASH : login.result.token ? MAIN_DASHBOARD : LANDING
      }
      headerMode={'none'}>
      {isSplashLoading && <Screen name={SPLASH} component={Splash} />}
      {!login.result.token ? (
        <>
          <Screen name={LANDING} component={Landing} />
          <Screen name={SIGNUP} component={SignUp} />
          <Screen name={LOGIN} component={Login} />
        </>
      ) : (
        <>
          <Screen name={MAIN_DASHBOARD} component={BottomTab} />
        </>
      )}
      <Screen name={OTP} component={OneTimePassword} />
      <Screen name={WEB_VIEW} component={WebView} />
      <Screen name={TERMS_AND_CONDITIONS} component={TermsAndConditions} />
      <Screen name={PRIVACY_POLICY} component={PrivacyPolicy} />
      <Screen name={JAIL_BROKEN_ROOTED} component={JailBreakRooted} />
    </Navigator>
  );
};

export default MainStack;
