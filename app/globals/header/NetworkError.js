import React from 'react';
import { View } from 'react-native';
import { Text } from 'react-native-elements';

import { OfflineIcon } from '../../images/icons/UserIcons';

import styles from './Styles';

const NetworkErrorScreen = () => (
  <View style={styles.networkErrorScreenContainer}>
    <OfflineIcon size={styles.bigCloudSize.height} />
    <Text style={styles.networkErrorScreenTitle}>You're Offline</Text>
    <Text style={styles.networkErrorScreenDesc}>
      You are not connected to the internet. Make sure Wi-Fi or Mobile data is
      on and Airplane Mode is off.
    </Text>
  </View>
);

export default NetworkErrorScreen;
