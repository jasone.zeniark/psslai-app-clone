import React from 'react';
import { View } from 'react-native';
import { Text } from 'react-native-elements';

import { NetworkOffIcon } from '../../images/icons/UserIcons';

import styles from './Styles';

const NetworkBanner = ({ connected }) =>
  !connected && (
    <View style={styles.networkErrorContainer}>
      <NetworkOffIcon
        size={styles.cloudSize.height}
        color={styles.bannerCloudColor.color}
      />
      <Text style={styles.networkErrorTitle}>No Internet Connection</Text>
    </View>
  );

export default NetworkBanner;
