import React, { useEffect, useState } from 'react';
import { View } from 'react-native';
import { Header, Text } from 'react-native-elements';
import { useDispatch, useSelector } from 'react-redux';

import {
  ArrowLeftIcon,
  CloseIcon,
  NetworkOffIcon,
} from '../../images/icons/UserIcons';
import { WHITE } from '../Theme';

import NetworkBanner from './NetworkBanner';

import styles from './Styles';

const CommonHeader = ({ title, onBackPress, web, bannerEnabled = true }) => {
  const dispatch = useDispatch();
  const login = useSelector(state => state.login);

  const [isConnected, setConnected] = useState(true);

  useEffect(() => {
    setConnected(login.connected);
  }, [login.connected]);

  return (
    <>
      <Header backgroundColor={WHITE}>
        <View style={styles.wrapper}>
          {web ? (
            <CloseIcon
              size={40}
              style={styles.arrowLeft}
              onPress={onBackPress}
            />
          ) : (
            <ArrowLeftIcon
              size={30}
              style={styles.arrowLeft}
              onPress={onBackPress}
            />
          )}
          <Text h4 h4Style={styles.headerTitle}>
            {title}
          </Text>
        </View>
      </Header>
      {bannerEnabled && <NetworkBanner connected={isConnected} />}
    </>
  );
};

export default CommonHeader;
