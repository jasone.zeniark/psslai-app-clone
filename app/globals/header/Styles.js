import { StyleSheet, Dimensions } from 'react-native';

import { WHITE, GRAY, ERROR, BLACK, FONT, DARK_BLUE } from '../Theme';
import colors from '../colors';

const { width, height } = Dimensions.get('screen');

const Styles = StyleSheet.create({
  container: {
    backgroundColor: WHITE,
  },
  headerLeft: {
    flexDirection: 'row',
    marginLeft: 12,
  },
  wrapper: {
    flexDirection: 'row',
    width: 'auto',
  },
  headerTitle: {
    ...FONT,
    fontSize: 20,
    fontWeight: 'bold',
    width: 300,
  },
  arrowLeft: {
    marginLeft: 10,
    marginRight: 10,
  },
  networkErrorScreenContainer: {
    height: height * 0.75,
    alignItems: 'center',
    justifyContent: 'center',
  },
  networkErrorContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    backgroundColor: DARK_BLUE,
    paddingVertical: 5,
  },
  networkErrorTitle: {
    ...FONT,
    fontSize: height * 0.019,
    marginLeft: 7,
    color: WHITE,
  },
  networkErrorScreenTitle: {
    marginTop: '3%',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: height * 0.025,
    color: DARK_BLUE,
  },
  networkErrorScreenDesc: {
    ...FONT,
    textAlign: 'center',
    marginTop: '3%',
    fontSize: height * 0.018,
    width: '75%',
    color: BLACK,
  },
  bannerCloudColor: {
    color: WHITE,
  },
  cloudSize: {
    height: height * 0.025,
  },
  bigCloudSize: {
    height: height * 0.13,
  },
  loginButton: {
    marginTop: '5%',
    height: height * 0.055,
    borderRadius: 36,
    borderWidth: 2,
    borderColor: DARK_BLUE,
  },
  loginTitle: {
    fontFamily: 'Montserrat-Bold',
    color: DARK_BLUE,
    paddingHorizontal: '6%',
    fontSize: height * 0.0185,
  },
});

export default Styles;
