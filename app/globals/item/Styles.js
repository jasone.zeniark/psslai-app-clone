import { StyleSheet } from 'react-native';

const Styles = StyleSheet.create({
  margin: {
    marginHorizontal: 5,
  },

  container: {
    marginStart: -13,
    marginEnd: -13,
  },
  icon: {
    fontSize: 30,
    color: '#002171',
  },
});

export default Styles;
