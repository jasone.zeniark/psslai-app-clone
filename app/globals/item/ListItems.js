import React from 'react';
import { View } from 'react-native';
import { ListItem } from 'react-native-elements';
import styles from './Styles';

const ListItems = props => (
  <View style={styles.wrapper}>
    {props.list.map((item, i) => (
      <ListItem
        style={[styles.container]}
        key={i}
        bottomDivider
        topDivider
        onPress={item.handler}>
        <ListItem.Content style={[styles.margin]}>
          <ListItem.Title>{item.title}</ListItem.Title>
        </ListItem.Content>
        <ListItem.Chevron style={[styles.margin]} iconStyle={styles.icon}/>
      </ListItem>
    ))}
  </View>
);

export default ListItems;
