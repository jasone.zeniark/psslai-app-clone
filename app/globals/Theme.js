import React from 'react';

const DARK_BLUE = '#201751';
const GRAY = '#909196';
const BLACK = '#000000';
const WHITE = '#FFFFFF';
const SEMI_LIGHT = '#F9F7F7';
const ERROR = '#FF5C5C';
const FONT = {
  fontFamily: 'Montserrat-Medium',
};
const BLUE = '#2A3D99';

export {DARK_BLUE, GRAY, BLACK, WHITE, FONT, BLUE, SEMI_LIGHT, ERROR};
