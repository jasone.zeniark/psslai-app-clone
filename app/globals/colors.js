const COLORS = {
  black: '#000000',
  light: '#a5a5a5',
  semi_light: '#e0e0e0',
  medium: '#757575',
  white: '#ffffff',
  primary: '#201751',
  secondary: '#f9a825',
  transparent: '#ffffff00',
  primary_hard: '#1a237e',
  semi_trans: '#FFFFFF66',
  very_light: '#eeeeee',
  semi_gray: '#f1f1f1',
  medium_gray: '#8e8f92',
  blue: '#30429c',
  red: '#E02B27',
};

export default COLORS;
