import React from 'react';
import { View, ActivityIndicator } from 'react-native';

const UserLoader = ({ styles }) => (
  <View>
    <ActivityIndicator size="large" color="#0000ff" style={styles} />
  </View>
);

export default UserLoader;
