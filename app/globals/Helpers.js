import React from 'react';
import { Linking } from 'react-native';

import routes from './navigations/Routes';

const { WEB_VIEW } = routes;

// Linking from psslai app to email, map, phone app, and etc.
export const LinkingData = data => Linking.openURL(data);

// Navigate from psslai app to psslai's trusted web links.
export const NavigateWeb = (navigation, link) =>
  navigation.navigate(WEB_VIEW, { url: link });

// Format the value into a card number format using regex.
export const CardNumber = value => {
  let v = value.replace(/\s+/g, '').replace(/[^0-9]/gi, '');
  let matches = v.match(/\d{4,12}/g);
  let match = (matches && matches[0]) || '';
  let parts = [];

  for (let i = 0, len = match.length; i < len; i += 4) {
    parts.push(match.substring(i, i + 4));
  }

  if (parts.length) {
    return parts.join(' ');
  } else {
    return value;
  }
};

export const NumbersAndCommas = value =>
  value.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
