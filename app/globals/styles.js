import { StyleSheet, Dimensions, Platform } from 'react-native';
import COLORS from './colors';

const { width, height } = Dimensions.get('screen');
const os = Platform.OS === 'ios';

const STYLES = StyleSheet.create({
  /* GLOBAL */

  textBold: {
    fontFamily: 'Montserrat-Bold',
  },

  Input: {
    borderWidth: 1,
    borderColor: COLORS.light,
    height: 50,
    borderRadius: 5,
    padding: 10,
  },

  Icon: {
    width: 20,
    height: 20,
  },

  IconRotate: {
    width: 15,
    height: 15,
    transform: [{ rotate: '180deg' }],
  },

  textStart: {
    marginStart: 20,
  },

  scrollviewHeight: {
    height: '100%',
  },

  TextInputContainer: {
    marginHorizontal: 20,
    marginTop: 20,
    justifyContent: 'center',
  },

  MainView: {
    flexDirection: 'row',
    marginTop: 15,
  },

  HeaderNav: {
    flexDirection: 'row',
    width: width / 11,
    marginStart: 20,
  },

  NavHeaderImage: {
    width: 30,
    height: 18,
    justifyContent: 'center',
    alignSelf: 'center',
  },

  TextInputTitle: {
    color: COLORS.light,
    fontWeight: 'bold',
  },

  TextInputTitleContainer: {
    marginTop: 15,
  },

  CheckBoxText: {
    flexWrap: 'wrap',
    marginStart: 20,
    fontSize: 18,
  },

  /* HOME SCREEN */

  homeMainView: {
    flexDirection: 'row',
    marginTop: 25,
  },

  homeLogo: {
    marginStart: 25,
    width: 110,
    height: 30,
    justifyContent: 'center',
    alignSelf: 'center',
  },

  homeTextView: {
    fontSize: 30,
    color: COLORS.primary,
    marginStart: 25,
    marginTop: 110,
  },

  homeSignUpBtn: {
    backgroundColor: COLORS.primary,
    width: width / 1.2,
    height: 50,
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },

  homeLoginBtn: {
    width: 90,
    height: 40,
    borderWidth: 1,
    borderColor: COLORS.black,
    borderRadius: 4,
    justifyContent: 'center',
    alignItems: 'center',
    marginStart: 140,
  },

  homeTransparentBtnContainer: {
    flexDirection: 'row',
    borderRadius: 25,
    borderWidth: 3,
    borderColor: COLORS.very_light,
    height: 45,
    width: width / 1.2,
    alignItems: 'center',
    alignSelf: 'center',
  },

  homeBtnText: {
    marginStart: 10,
    color: COLORS.primary,
  },

  homeFooterText: {
    textAlign: 'center',
    color: COLORS.light,
  },

  homeFooter: {
    marginHorizontal: 25,
    justifyContent: 'center',
  },

  /* LOGIN SCREEN */

  loginMainView: {
    flexDirection: 'row',
    marginTop: 15,
  },

  HeaderNavText: {
    fontSize: 15,
    fontWeight: 'bold',
    marginStart: 5,
  },

  loginSecureEntry: {
    width: 20,
    justifyContent: 'center',
    alignContent: 'center',
    alignSelf: 'flex-end',
    alignItems: 'center',
    marginRight: 30,
    flexDirection: 'row',
    marginTop: -35,
  },

  loginIconContainer: {
    width: 20,
    height: 20,
    alignSelf: 'center',
    marginLeft: '36%',
  },

  loginTransparentBtn: {
    flexDirection: 'row',
    height: 50,
    borderWidth: 1,
    borderStartColor: COLORS.transparent,
    borderEndColor: COLORS.transparent,
    borderTopColor: COLORS.transparent,
    borderColor: COLORS.semi_light,
  },

  loginSecondaryBtnContainer: {
    justifyContent: 'center',
  },

  /* SIGN UP SCREEN */

  SignUpTextInputSV: { height: height / 1.8, marginTop: 10 },

  text: {
    color: COLORS.white,
    fontSize: 15,
    fontWeight: 'bold',
  },
  textStyle: {
    color: COLORS.primary,
  },

  checboxContainer: {
    borderWidth: 1,
    borderColor: COLORS.light,
    height: 20,
    width: 20,
    borderRadius: 0,
    backgroundColor: COLORS.light,
  },
  checkbox: {
    height: 20,
    width: 20,
    borderWidth: 1,
    borderColor: COLORS.medium,
  },

  signupBtn: {
    backgroundColor: COLORS.primary,
    marginTop: 20,
    height: 50,
    width: width / 1.2,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    borderRadius: 5,
  },

  loginBtn: {
    backgroundColor: COLORS.primary,
    marginTop: 60,
    height: 50,
    width: width / 1.1,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    borderRadius: 3,
  },

  secondaryBtn: {
    marginTop: 35,
    flexDirection: 'row',
    height: 50,
    borderWidth: 1,
    borderStartColor: COLORS.transparent,
    borderEndColor: COLORS.transparent,
    borderColor: COLORS.semi_light,
  },

  secondaryBtnInner: {
    justifyContent: 'center',
  },

  touchableBtn: {
    width: 20,
    height: 20,
    alignSelf: 'center',
    marginLeft: 150,
  },

  transparentBtn: {
    marginTop: 170,
    flexDirection: 'row',
    borderRadius: 25,
    borderWidth: 3,
    borderColor: COLORS.very_light,
    height: 45,
    width: width / 1.2,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },

  modalBackground: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.5)',
    justifyContent: 'center',
    alignItems: 'center',
  },

  modalContainer: {
    width: width / 1.4,
    height: height / 2,
    backgroundColor: COLORS.white,
    paddingHorizontal: 20,
    paddingVertical: 30,
    borderRadius: 5,
    elevation: 20,
  },

  modalHeader: {
    width: 30,
    height: 30,
    alignSelf: 'flex-end',
    justifyContent: 'center',
  },

  spacer: {
    height: height / 15,
    width: width / 1,
  },

  cardView: {
    resizeMode: 'cover',
    height: height / 3.5,
    width: width * 0.9,
  },

  modal: {
    backgroundColor: COLORS.white,
    borderRadius: 10,
  },

  mContainer: {
    flex: 1,
    backgroundColor: 'green',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
  },

  touchableOpacity: {
    backgroundColor: COLORS.secondary,
    alignSelf: 'center',
    paddingHorizontal: 20,
  },

  iContainter: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  iModal: {
    backgroundColor: COLORS.white,
  },

  scrollView: {
    height: '100%',
    backgroundColor: '#fafafa',
    // backgroundColor: 'red',
  },
  headerWrapper: {
    marginTop: os ? 20 : 40,
    width: 'auto',
  },
});

export default STYLES;
