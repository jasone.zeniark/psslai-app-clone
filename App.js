/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import 'react-native-gesture-handler';
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { Provider } from 'react-redux';
import { SafeAreaView, StatusBar } from 'react-native';
import { PersistGate } from 'redux-persist/es/integration/react';

import MainStack from './app/globals/navigations/MainStack';
import { store, persistor } from './app/store/ReduxStore';

const App = () => (
  <SafeAreaView style={{ flex: 1 }}>
    <StatusBar barStyle={'dark-content'} backgroundColor={'#fff'} />
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <NavigationContainer>
          <MainStack />
        </NavigationContainer>
      </PersistGate>
    </Provider>
  </SafeAreaView>
);

export default App;
