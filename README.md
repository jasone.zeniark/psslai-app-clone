# Welcome to PSSLAI Member Mobile App repository!

## clone the repository 
`git clone https://gitlab.com/ubx-zeniark/psslai-member-mobile.git`

## checkout dev branch
`git checkout dev`

## run npm install
`npm install`

## running react native app (iOS)  

- go to ios directory using the project terminal by running:

    `cd ios`

- run pod install

    `pod install`

- run the app using ios simulator 

    `npx react-native run-ios`

## running react native app (Android)  

- run the app using android emulator 

    `npx react-native run-android`
